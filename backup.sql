-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: localhost    Database: keycloak
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADMIN_EVENT_ENTITY`
--

DROP TABLE IF EXISTS `ADMIN_EVENT_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ADMIN_EVENT_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `ADMIN_EVENT_TIME` bigint DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `OPERATION_TYPE` varchar(255) DEFAULT NULL,
  `AUTH_REALM_ID` varchar(255) DEFAULT NULL,
  `AUTH_CLIENT_ID` varchar(255) DEFAULT NULL,
  `AUTH_USER_ID` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `RESOURCE_PATH` text,
  `REPRESENTATION` text,
  `ERROR` varchar(255) DEFAULT NULL,
  `RESOURCE_TYPE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADMIN_EVENT_ENTITY`
--

LOCK TABLES `ADMIN_EVENT_ENTITY` WRITE;
/*!40000 ALTER TABLE `ADMIN_EVENT_ENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADMIN_EVENT_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ASSOCIATED_POLICY`
--

DROP TABLE IF EXISTS `ASSOCIATED_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ASSOCIATED_POLICY` (
  `POLICY_ID` varchar(36) NOT NULL,
  `ASSOCIATED_POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`POLICY_ID`,`ASSOCIATED_POLICY_ID`),
  KEY `IDX_ASSOC_POL_ASSOC_POL_ID` (`ASSOCIATED_POLICY_ID`),
  CONSTRAINT `FK_FRSR5S213XCX4WNKOG82SSRFY` FOREIGN KEY (`ASSOCIATED_POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`),
  CONSTRAINT `FK_FRSRPAS14XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ASSOCIATED_POLICY`
--

LOCK TABLES `ASSOCIATED_POLICY` WRITE;
/*!40000 ALTER TABLE `ASSOCIATED_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ASSOCIATED_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATION_EXECUTION`
--

DROP TABLE IF EXISTS `AUTHENTICATION_EXECUTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATION_EXECUTION` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `AUTHENTICATOR` varchar(36) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `FLOW_ID` varchar(36) DEFAULT NULL,
  `REQUIREMENT` int DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  `AUTHENTICATOR_FLOW` bit(1) NOT NULL DEFAULT b'0',
  `AUTH_FLOW_ID` varchar(36) DEFAULT NULL,
  `AUTH_CONFIG` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_EXEC_REALM_FLOW` (`REALM_ID`,`FLOW_ID`),
  KEY `IDX_AUTH_EXEC_FLOW` (`FLOW_ID`),
  CONSTRAINT `FK_AUTH_EXEC_FLOW` FOREIGN KEY (`FLOW_ID`) REFERENCES `AUTHENTICATION_FLOW` (`ID`),
  CONSTRAINT `FK_AUTH_EXEC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATION_EXECUTION`
--

LOCK TABLES `AUTHENTICATION_EXECUTION` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATION_EXECUTION` DISABLE KEYS */;
INSERT INTO `AUTHENTICATION_EXECUTION` VALUES ('00b51b5c-5df3-4253-9942-fd94601245ff',NULL,NULL,'master','d51f8ea4-5687-49f7-811c-00887172a1b1',1,40,_binary '','1742483e-c797-4783-b60f-edb7308548e6',NULL),('02184c20-3d44-4720-a96c-419068a2b8f2',NULL,'direct-grant-validate-password','master','052a297e-6977-47ef-ac36-71502001a3b9',0,20,_binary '\0',NULL,NULL),('08d362fa-20c1-48f5-aa26-2866af77f3c7',NULL,'auth-cookie','Sc-project','e01b385b-7ba6-4bb0-a1b9-c001baa111b2',2,10,_binary '\0',NULL,NULL),('0aa6c079-caa4-44ae-8fae-e6b620563ce0',NULL,NULL,'master','87efb9fc-a39b-4713-98cf-eba1a8d30723',2,20,_binary '','e3ae3fdc-1c41-4095-a482-b396befe2c68',NULL),('0e2b90c2-5021-41ae-8fad-347c5915acb3',NULL,NULL,'master','b0cbca6d-372e-49dd-bdcf-14fce67d009f',2,30,_binary '','15b86501-8057-4a58-aa0b-bbda757da2b8',NULL),('0ef01908-fad5-4108-8260-c5eb69905988',NULL,'client-secret-jwt','master','879ccd32-2d80-41b6-931d-3a5754924f53',2,30,_binary '\0',NULL,NULL),('1811a19d-1f07-4cdf-915e-f5f550ed16f2',NULL,'conditional-user-configured','master','1742483e-c797-4783-b60f-edb7308548e6',0,10,_binary '\0',NULL,NULL),('18761d9e-60d3-46f9-9b98-d1df43eeb97a',NULL,'auth-otp-form','Sc-project','95d55aee-cf65-44a3-9795-c860c1bc6c8b',0,20,_binary '\0',NULL,NULL),('19ed40be-251a-4757-bf8f-dc48423fa28c',NULL,'identity-provider-redirector','Sc-project','e01b385b-7ba6-4bb0-a1b9-c001baa111b2',2,25,_binary '\0',NULL,NULL),('1a9f11f5-2992-4f9f-9671-705fbacbbf0c',NULL,'auth-otp-form','master','c464bef1-79d8-49e9-a785-7e6a05a2a04b',0,20,_binary '\0',NULL,NULL),('1b1e3af1-03e6-40dd-81e0-0bcc6db04a7b',NULL,'conditional-user-configured','master','eda21637-4071-44a9-9519-ba59178c90f2',0,10,_binary '\0',NULL,NULL),('1b6f590a-c4d5-4824-a2a4-627a66b2774d',NULL,NULL,'Sc-project','7f62c500-ff19-449e-bd4d-ed934b2714a8',0,20,_binary '','4606ab8e-71fc-4034-9a8d-57bb4f01f82b',NULL),('1ebe3405-b992-42f7-9f60-c6fb31428b36',NULL,'direct-grant-validate-password','Sc-project','2c6ccc59-5ec8-4356-b703-e72b9a3ab823',0,20,_binary '\0',NULL,NULL),('1ef3afee-f4e6-4d22-bf1d-2634524dadbe',NULL,'docker-http-basic-authenticator','master','7639537e-660e-486e-84c4-e203fccd71c3',0,10,_binary '\0',NULL,NULL),('1f60adee-a1f8-4b9c-95d0-1e755849a835',NULL,'client-jwt','Sc-project','aeb079fd-6ff5-4144-863c-968adad83dad',2,20,_binary '\0',NULL,NULL),('1f98daf6-a2d5-4a7b-beb5-01f7ff5b3034',NULL,'registration-password-action','Sc-project','cbab8acd-b00b-418f-8235-aae6dc5d5993',0,50,_binary '\0',NULL,NULL),('20d64189-c785-4d2f-acbc-4d009dee98ed',NULL,'reset-otp','master','1742483e-c797-4783-b60f-edb7308548e6',0,20,_binary '\0',NULL,NULL),('238986fb-99f2-458a-93d5-014e26455933',NULL,'client-jwt','master','879ccd32-2d80-41b6-931d-3a5754924f53',2,20,_binary '\0',NULL,NULL),('24e2f887-456e-4e63-851b-8bb6a8a91320',NULL,'docker-http-basic-authenticator','Sc-project','95ac08d4-1301-4743-9bff-60b1a0bbfac8',0,10,_binary '\0',NULL,NULL),('2b281f34-1978-4c07-8476-8fab837a288d',NULL,NULL,'Sc-project','2c6ccc59-5ec8-4356-b703-e72b9a3ab823',1,30,_binary '','bf75c868-d962-4aed-bf91-56fc0ff2f661',NULL),('2cf0db95-9120-4001-9e3b-cb033c380eec',NULL,NULL,'Sc-project','2d365691-f30e-47b6-bde7-930bb15a7d17',1,40,_binary '','2d7bdffc-f138-4e8c-bd22-2d2f9bc8edbe',NULL),('2dc0e87c-cef3-401b-828e-0f58aff3ef41',NULL,'idp-create-user-if-unique','Sc-project','4606ab8e-71fc-4034-9a8d-57bb4f01f82b',2,10,_binary '\0',NULL,'03ee9d90-244a-4f13-a83b-74a6ded0105e'),('2e32ac6f-ca30-49c4-af24-ddfc96467099',NULL,'conditional-user-configured','master','c464bef1-79d8-49e9-a785-7e6a05a2a04b',0,10,_binary '\0',NULL,NULL),('2f7d83ac-5b1e-4440-b920-d5d80fbe538f',NULL,'basic-auth-otp','master','c3979f6a-a17c-47ff-a2fb-f1d58a1d609a',3,20,_binary '\0',NULL,NULL),('32428493-3ca0-4c35-b659-8bb8607646e2',NULL,'registration-recaptcha-action','master','e3b172cc-ef91-4583-af91-f277de07674b',3,60,_binary '\0',NULL,NULL),('32b68d09-95c8-4a0a-bf7b-b2551752ef19',NULL,'client-x509','master','879ccd32-2d80-41b6-931d-3a5754924f53',2,40,_binary '\0',NULL,NULL),('35a7db67-7548-40ce-a0bf-da4b16090325',NULL,'idp-email-verification','Sc-project','c899b3bd-79af-4410-b6db-220aae439bb2',2,10,_binary '\0',NULL,NULL),('36d4d899-4f02-4aef-ab22-287387123135',NULL,NULL,'master','dc72162f-567f-40b9-8412-af41eab99e1c',2,20,_binary '','7bc5b37d-ee44-407c-8ab3-d5cfc6d2d905',NULL),('387b9653-a3b7-4e6c-80b4-d34a399061f5',NULL,'client-secret','master','879ccd32-2d80-41b6-931d-3a5754924f53',2,10,_binary '\0',NULL,NULL),('39194525-d8e7-4de8-9c77-1493c421d80e',NULL,'conditional-user-configured','Sc-project','2d7bdffc-f138-4e8c-bd22-2d2f9bc8edbe',0,10,_binary '\0',NULL,NULL),('41179ef3-9eac-47fa-a750-c6a8dd1825af',NULL,'reset-credentials-choose-user','Sc-project','2d365691-f30e-47b6-bde7-930bb15a7d17',0,10,_binary '\0',NULL,NULL),('4636ca34-402e-413c-b889-49731da50809',NULL,'direct-grant-validate-username','master','052a297e-6977-47ef-ac36-71502001a3b9',0,10,_binary '\0',NULL,NULL),('4657244b-2740-4ad2-ab8a-c2e0c8c17784',NULL,'registration-user-creation','Sc-project','cbab8acd-b00b-418f-8235-aae6dc5d5993',0,20,_binary '\0',NULL,NULL),('47ae923f-3a82-4787-bb00-f024045e20ca',NULL,'reset-credential-email','Sc-project','2d365691-f30e-47b6-bde7-930bb15a7d17',0,20,_binary '\0',NULL,NULL),('48484ca4-308b-4b9c-9cc0-9978610c2b37',NULL,'basic-auth','Sc-project','9070c678-6e31-4dc8-bd82-af375807f17a',0,10,_binary '\0',NULL,NULL),('48c230cb-3318-4566-814f-5681490098bd',NULL,'auth-spnego','Sc-project','e01b385b-7ba6-4bb0-a1b9-c001baa111b2',3,20,_binary '\0',NULL,NULL),('49016fff-da15-4dd7-abee-b34c2c9e4069',NULL,NULL,'master','7bc5b37d-ee44-407c-8ab3-d5cfc6d2d905',1,20,_binary '','eda21637-4071-44a9-9519-ba59178c90f2',NULL),('4a602ab3-0092-4036-8b96-84baa276d3bb',NULL,'idp-username-password-form','Sc-project','e593fd42-dc91-420f-bdb9-6bef6f9780ca',0,10,_binary '\0',NULL,NULL),('4c5a12fa-7b90-4f77-a646-04e51862a899',NULL,'no-cookie-redirect','Sc-project','07b9c3d9-42b8-40c9-99fe-fb143d278409',0,10,_binary '\0',NULL,NULL),('4d720ad8-3b6f-436e-b52f-85ae1b1db9a2',NULL,'idp-confirm-link','Sc-project','bd59319c-643f-4649-8fc8-cb268ec93768',0,10,_binary '\0',NULL,NULL),('4fa3b6ae-6550-46e5-99d0-302c4e9001ea',NULL,'reset-credential-email','master','d51f8ea4-5687-49f7-811c-00887172a1b1',0,20,_binary '\0',NULL,NULL),('550e51ce-f69e-4ca8-a081-16621b9c2a04',NULL,'reset-password','master','d51f8ea4-5687-49f7-811c-00887172a1b1',0,30,_binary '\0',NULL,NULL),('57d02167-1671-4b41-ac5b-cda6a14f4009',NULL,NULL,'Sc-project','dbbe3bef-b1af-4cab-8428-bddbfcb20f9e',1,20,_binary '','2010451c-183d-4902-acdb-c614452ba25d',NULL),('57ff68d4-bc0b-480a-94e3-dae7e12d1b43',NULL,'conditional-user-configured','Sc-project','2010451c-183d-4902-acdb-c614452ba25d',0,10,_binary '\0',NULL,NULL),('595ea1b3-4785-43d1-88ec-79014651d083',NULL,'client-secret-jwt','Sc-project','aeb079fd-6ff5-4144-863c-968adad83dad',2,30,_binary '\0',NULL,NULL),('5c8b8c0d-89b6-4121-89f4-2a1145844124',NULL,'auth-otp-form','master','eda21637-4071-44a9-9519-ba59178c90f2',0,20,_binary '\0',NULL,NULL),('60107997-9434-485b-9698-178a0dbab45e',NULL,'basic-auth','master','c3979f6a-a17c-47ff-a2fb-f1d58a1d609a',0,10,_binary '\0',NULL,NULL),('6570b044-e865-4e4c-9517-b0518bea75b3',NULL,NULL,'master','f7a037ca-cba6-436c-b37a-0151280bcc91',0,20,_binary '','c3979f6a-a17c-47ff-a2fb-f1d58a1d609a',NULL),('662d9cb7-ca24-4e03-b87c-283e2292b0c4',NULL,'conditional-user-configured','master','3fa6f911-71db-4fe2-a62d-a119ecbd48ce',0,10,_binary '\0',NULL,NULL),('677dea89-2902-4f36-a7e5-2f4c1ddad8e9',NULL,'client-secret','Sc-project','aeb079fd-6ff5-4144-863c-968adad83dad',2,10,_binary '\0',NULL,NULL),('68e27779-d72d-4046-a5b2-91f34653997e',NULL,'registration-profile-action','master','e3b172cc-ef91-4583-af91-f277de07674b',0,40,_binary '\0',NULL,NULL),('6ff69b3f-b84c-483e-8752-740e3e3cf788',NULL,'auth-spnego','master','b0cbca6d-372e-49dd-bdcf-14fce67d009f',3,20,_binary '\0',NULL,NULL),('770f3872-6f33-4dd9-ab9d-a2a2edb051d5',NULL,'reset-credentials-choose-user','master','d51f8ea4-5687-49f7-811c-00887172a1b1',0,10,_binary '\0',NULL,NULL),('7cd9029a-a1ca-43e9-8f93-c9a75c8b28e9',NULL,'conditional-user-configured','Sc-project','bf75c868-d962-4aed-bf91-56fc0ff2f661',0,10,_binary '\0',NULL,NULL),('80f55cfd-6652-47e2-a680-2a530db59e56',NULL,NULL,'Sc-project','e01b385b-7ba6-4bb0-a1b9-c001baa111b2',2,30,_binary '','dbbe3bef-b1af-4cab-8428-bddbfcb20f9e',NULL),('8119f321-f4a0-462b-b909-743c67ac35af',NULL,'auth-otp-form','Sc-project','2010451c-183d-4902-acdb-c614452ba25d',0,20,_binary '\0',NULL,NULL),('8258663e-260f-4dbc-a6ce-3a290f3b0ee1',NULL,NULL,'Sc-project','c899b3bd-79af-4410-b6db-220aae439bb2',2,20,_binary '','e593fd42-dc91-420f-bdb9-6bef6f9780ca',NULL),('93293075-3ab8-41c2-8e6b-4abfb3564fd7',NULL,'http-basic-authenticator','master','afa78e92-2df3-4457-9d1d-d104f4ccdf5d',0,10,_binary '\0',NULL,NULL),('9510bc8f-c012-4022-aaaf-1bc56ad8604a',NULL,'direct-grant-validate-otp','master','3fa6f911-71db-4fe2-a62d-a119ecbd48ce',0,20,_binary '\0',NULL,NULL),('9a858619-da60-428b-8e1d-071888368326',NULL,'identity-provider-redirector','master','b0cbca6d-372e-49dd-bdcf-14fce67d009f',2,25,_binary '\0',NULL,NULL),('9b36a434-4ee2-412f-ab74-a91756498068',NULL,'auth-cookie','master','b0cbca6d-372e-49dd-bdcf-14fce67d009f',2,10,_binary '\0',NULL,NULL),('9c92dce4-7dd3-47a1-9e8e-e35e28420482',NULL,'no-cookie-redirect','master','f7a037ca-cba6-436c-b37a-0151280bcc91',0,10,_binary '\0',NULL,NULL),('9d3daafa-94e1-4188-a890-91316bb90213',NULL,'basic-auth-otp','Sc-project','9070c678-6e31-4dc8-bd82-af375807f17a',3,20,_binary '\0',NULL,NULL),('a1b7ab0d-3b31-472f-b6d3-1e9e4eee0883',NULL,'direct-grant-validate-username','Sc-project','2c6ccc59-5ec8-4356-b703-e72b9a3ab823',0,10,_binary '\0',NULL,NULL),('a1f4a1ec-d8a6-474b-a725-b4ad2d1b4640',NULL,'auth-spnego','master','c3979f6a-a17c-47ff-a2fb-f1d58a1d609a',3,30,_binary '\0',NULL,NULL),('a35d6ff5-c6f1-4ca6-9fce-e0e2be280ba7',NULL,NULL,'master','e3ae3fdc-1c41-4095-a482-b396befe2c68',0,20,_binary '','dc72162f-567f-40b9-8412-af41eab99e1c',NULL),('a6db6e56-460a-4083-9342-cacad4c1c33e',NULL,NULL,'Sc-project','bd59319c-643f-4649-8fc8-cb268ec93768',0,20,_binary '','c899b3bd-79af-4410-b6db-220aae439bb2',NULL),('a76791fd-c627-4a80-8a7e-c940dfb3dbd0',NULL,NULL,'Sc-project','07b9c3d9-42b8-40c9-99fe-fb143d278409',0,20,_binary '','9070c678-6e31-4dc8-bd82-af375807f17a',NULL),('ae4ef3b8-8abf-4fc6-ba3a-192610d5fcab',NULL,'registration-page-form','Sc-project','e9e32b41-6d9f-4a76-ba7c-cf366b58d62f',0,10,_binary '','cbab8acd-b00b-418f-8235-aae6dc5d5993',NULL),('b2e001a3-3465-4cde-8357-222bd0e25a1d',NULL,'idp-username-password-form','master','7bc5b37d-ee44-407c-8ab3-d5cfc6d2d905',0,10,_binary '\0',NULL,NULL),('b3573f01-a6d2-4ff5-9b3a-3c94e6ef8e2f',NULL,NULL,'master','052a297e-6977-47ef-ac36-71502001a3b9',1,30,_binary '','3fa6f911-71db-4fe2-a62d-a119ecbd48ce',NULL),('b44aa002-5c23-44da-8d4b-b7b1377a4beb',NULL,'auth-username-password-form','master','15b86501-8057-4a58-aa0b-bbda757da2b8',0,10,_binary '\0',NULL,NULL),('b47aea57-8217-4f10-b2bc-061d94655e09',NULL,'reset-otp','Sc-project','2d7bdffc-f138-4e8c-bd22-2d2f9bc8edbe',0,20,_binary '\0',NULL,NULL),('b52f73f3-2a91-444d-be16-3fe4a24674d8',NULL,'conditional-user-configured','Sc-project','95d55aee-cf65-44a3-9795-c860c1bc6c8b',0,10,_binary '\0',NULL,NULL),('c1af0a72-dc68-4d6e-ab89-ee8477ff57b5',NULL,NULL,'Sc-project','4606ab8e-71fc-4034-9a8d-57bb4f01f82b',2,20,_binary '','bd59319c-643f-4649-8fc8-cb268ec93768',NULL),('c7329857-259c-4bc1-877a-cf2e0e3e64d2',NULL,'idp-review-profile','Sc-project','7f62c500-ff19-449e-bd4d-ed934b2714a8',0,10,_binary '\0',NULL,'3a7c3d3a-d0b9-4dc6-a61b-d64147a2d48c'),('ce5119e8-9ca6-4848-af68-616b7097a405',NULL,'registration-profile-action','Sc-project','cbab8acd-b00b-418f-8235-aae6dc5d5993',0,40,_binary '\0',NULL,NULL),('d95f6ca4-ab00-4691-a892-712c2fa09ab6',NULL,NULL,'master','26f176d3-7d70-433f-a89c-fe19f2509144',0,20,_binary '','87efb9fc-a39b-4713-98cf-eba1a8d30723',NULL),('dfee9840-2625-41b3-ae86-8fa6d5b10ea2',NULL,'auth-username-password-form','Sc-project','dbbe3bef-b1af-4cab-8428-bddbfcb20f9e',0,10,_binary '\0',NULL,NULL),('e390a881-ba36-46d5-a55f-bfd29911ef0f',NULL,'registration-page-form','master','fcd3f9ff-77ca-4e2d-9c0e-71d32053d75f',0,10,_binary '','e3b172cc-ef91-4583-af91-f277de07674b',NULL),('e3a89d6f-9290-4531-8a34-ee1a727d1bd1',NULL,NULL,'Sc-project','e593fd42-dc91-420f-bdb9-6bef6f9780ca',1,20,_binary '','95d55aee-cf65-44a3-9795-c860c1bc6c8b',NULL),('e42d733f-7b13-4210-a08f-9e116b23bb57',NULL,'idp-create-user-if-unique','master','87efb9fc-a39b-4713-98cf-eba1a8d30723',2,10,_binary '\0',NULL,'c7da533a-3278-4464-8153-9741cc26f397'),('e99dcdd4-e751-46db-a3ca-b1f730172014',NULL,'registration-user-creation','master','e3b172cc-ef91-4583-af91-f277de07674b',0,20,_binary '\0',NULL,NULL),('ec3a3a69-caee-4620-acc2-f681ce875d6a',NULL,'direct-grant-validate-otp','Sc-project','bf75c868-d962-4aed-bf91-56fc0ff2f661',0,20,_binary '\0',NULL,NULL),('eccc04fa-d1b2-4377-a87d-a3d2fa9a2fa1',NULL,'idp-confirm-link','master','e3ae3fdc-1c41-4095-a482-b396befe2c68',0,10,_binary '\0',NULL,NULL),('edb4a7f7-93ed-4bc2-ab19-f4cbb4c1da4a',NULL,'auth-spnego','Sc-project','9070c678-6e31-4dc8-bd82-af375807f17a',3,30,_binary '\0',NULL,NULL),('ee608a01-9642-4108-895d-f8a9cd93c953',NULL,'idp-review-profile','master','26f176d3-7d70-433f-a89c-fe19f2509144',0,10,_binary '\0',NULL,'b4efc01c-9950-4291-b13c-c9cdeaac6775'),('eeebd29a-ba10-42fd-b755-504a8f2ec1c8',NULL,'registration-recaptcha-action','Sc-project','cbab8acd-b00b-418f-8235-aae6dc5d5993',3,60,_binary '\0',NULL,NULL),('f292e9d1-e3cb-4a82-9be4-b8d74ddf2b80',NULL,'http-basic-authenticator','Sc-project','e685808a-be71-435d-8761-52e1afd138d2',0,10,_binary '\0',NULL,NULL),('f4392d80-3d7c-43cf-8db4-0a136f5709cd',NULL,NULL,'master','15b86501-8057-4a58-aa0b-bbda757da2b8',1,20,_binary '','c464bef1-79d8-49e9-a785-7e6a05a2a04b',NULL),('f5250374-00b7-42b7-a0e5-680b84f8e592',NULL,'idp-email-verification','master','dc72162f-567f-40b9-8412-af41eab99e1c',2,10,_binary '\0',NULL,NULL),('f6a6e160-0d39-4bd4-9ab1-a3ddf0731b93',NULL,'registration-password-action','master','e3b172cc-ef91-4583-af91-f277de07674b',0,50,_binary '\0',NULL,NULL),('fc3eee2b-0603-4c90-bc47-48ca7ac2c082',NULL,'client-x509','Sc-project','aeb079fd-6ff5-4144-863c-968adad83dad',2,40,_binary '\0',NULL,NULL),('fc8b3a12-a8e9-49f8-a470-9106359d573f',NULL,'reset-password','Sc-project','2d365691-f30e-47b6-bde7-930bb15a7d17',0,30,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `AUTHENTICATION_EXECUTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATION_FLOW`
--

DROP TABLE IF EXISTS `AUTHENTICATION_FLOW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATION_FLOW` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_ID` varchar(36) NOT NULL DEFAULT 'basic-flow',
  `TOP_LEVEL` bit(1) NOT NULL DEFAULT b'0',
  `BUILT_IN` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_FLOW_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_FLOW_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATION_FLOW`
--

LOCK TABLES `AUTHENTICATION_FLOW` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATION_FLOW` DISABLE KEYS */;
INSERT INTO `AUTHENTICATION_FLOW` VALUES ('052a297e-6977-47ef-ac36-71502001a3b9','direct grant','OpenID Connect Resource Owner Grant','master','basic-flow',_binary '',_binary ''),('07b9c3d9-42b8-40c9-99fe-fb143d278409','http challenge','An authentication flow based on challenge-response HTTP Authentication Schemes','Sc-project','basic-flow',_binary '',_binary ''),('15b86501-8057-4a58-aa0b-bbda757da2b8','forms','Username, password, otp and other auth forms.','master','basic-flow',_binary '\0',_binary ''),('1742483e-c797-4783-b60f-edb7308548e6','Reset - Conditional OTP','Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.','master','basic-flow',_binary '\0',_binary ''),('2010451c-183d-4902-acdb-c614452ba25d','Browser - Conditional OTP','Flow to determine if the OTP is required for the authentication','Sc-project','basic-flow',_binary '\0',_binary ''),('26f176d3-7d70-433f-a89c-fe19f2509144','first broker login','Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account','master','basic-flow',_binary '',_binary ''),('2c6ccc59-5ec8-4356-b703-e72b9a3ab823','direct grant','OpenID Connect Resource Owner Grant','Sc-project','basic-flow',_binary '',_binary ''),('2d365691-f30e-47b6-bde7-930bb15a7d17','reset credentials','Reset credentials for a user if they forgot their password or something','Sc-project','basic-flow',_binary '',_binary ''),('2d7bdffc-f138-4e8c-bd22-2d2f9bc8edbe','Reset - Conditional OTP','Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.','Sc-project','basic-flow',_binary '\0',_binary ''),('3fa6f911-71db-4fe2-a62d-a119ecbd48ce','Direct Grant - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('4606ab8e-71fc-4034-9a8d-57bb4f01f82b','User creation or linking','Flow for the existing/non-existing user alternatives','Sc-project','basic-flow',_binary '\0',_binary ''),('7639537e-660e-486e-84c4-e203fccd71c3','docker auth','Used by Docker clients to authenticate against the IDP','master','basic-flow',_binary '',_binary ''),('7bc5b37d-ee44-407c-8ab3-d5cfc6d2d905','Verify Existing Account by Re-authentication','Reauthentication of existing account','master','basic-flow',_binary '\0',_binary ''),('7f62c500-ff19-449e-bd4d-ed934b2714a8','first broker login','Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account','Sc-project','basic-flow',_binary '',_binary ''),('879ccd32-2d80-41b6-931d-3a5754924f53','clients','Base authentication for clients','master','client-flow',_binary '',_binary ''),('87efb9fc-a39b-4713-98cf-eba1a8d30723','User creation or linking','Flow for the existing/non-existing user alternatives','master','basic-flow',_binary '\0',_binary ''),('9070c678-6e31-4dc8-bd82-af375807f17a','Authentication Options','Authentication options.','Sc-project','basic-flow',_binary '\0',_binary ''),('95ac08d4-1301-4743-9bff-60b1a0bbfac8','docker auth','Used by Docker clients to authenticate against the IDP','Sc-project','basic-flow',_binary '',_binary ''),('95d55aee-cf65-44a3-9795-c860c1bc6c8b','First broker login - Conditional OTP','Flow to determine if the OTP is required for the authentication','Sc-project','basic-flow',_binary '\0',_binary ''),('aeb079fd-6ff5-4144-863c-968adad83dad','clients','Base authentication for clients','Sc-project','client-flow',_binary '',_binary ''),('afa78e92-2df3-4457-9d1d-d104f4ccdf5d','saml ecp','SAML ECP Profile Authentication Flow','master','basic-flow',_binary '',_binary ''),('b0cbca6d-372e-49dd-bdcf-14fce67d009f','browser','browser based authentication','master','basic-flow',_binary '',_binary ''),('bd59319c-643f-4649-8fc8-cb268ec93768','Handle Existing Account','Handle what to do if there is existing account with same email/username like authenticated identity provider','Sc-project','basic-flow',_binary '\0',_binary ''),('bf75c868-d962-4aed-bf91-56fc0ff2f661','Direct Grant - Conditional OTP','Flow to determine if the OTP is required for the authentication','Sc-project','basic-flow',_binary '\0',_binary ''),('c3979f6a-a17c-47ff-a2fb-f1d58a1d609a','Authentication Options','Authentication options.','master','basic-flow',_binary '\0',_binary ''),('c464bef1-79d8-49e9-a785-7e6a05a2a04b','Browser - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('c899b3bd-79af-4410-b6db-220aae439bb2','Account verification options','Method with which to verity the existing account','Sc-project','basic-flow',_binary '\0',_binary ''),('cbab8acd-b00b-418f-8235-aae6dc5d5993','registration form','registration form','Sc-project','form-flow',_binary '\0',_binary ''),('d51f8ea4-5687-49f7-811c-00887172a1b1','reset credentials','Reset credentials for a user if they forgot their password or something','master','basic-flow',_binary '',_binary ''),('dbbe3bef-b1af-4cab-8428-bddbfcb20f9e','forms','Username, password, otp and other auth forms.','Sc-project','basic-flow',_binary '\0',_binary ''),('dc72162f-567f-40b9-8412-af41eab99e1c','Account verification options','Method with which to verity the existing account','master','basic-flow',_binary '\0',_binary ''),('e01b385b-7ba6-4bb0-a1b9-c001baa111b2','browser','browser based authentication','Sc-project','basic-flow',_binary '',_binary ''),('e3ae3fdc-1c41-4095-a482-b396befe2c68','Handle Existing Account','Handle what to do if there is existing account with same email/username like authenticated identity provider','master','basic-flow',_binary '\0',_binary ''),('e3b172cc-ef91-4583-af91-f277de07674b','registration form','registration form','master','form-flow',_binary '\0',_binary ''),('e593fd42-dc91-420f-bdb9-6bef6f9780ca','Verify Existing Account by Re-authentication','Reauthentication of existing account','Sc-project','basic-flow',_binary '\0',_binary ''),('e685808a-be71-435d-8761-52e1afd138d2','saml ecp','SAML ECP Profile Authentication Flow','Sc-project','basic-flow',_binary '',_binary ''),('e9e32b41-6d9f-4a76-ba7c-cf366b58d62f','registration','registration flow','Sc-project','basic-flow',_binary '',_binary ''),('eda21637-4071-44a9-9519-ba59178c90f2','First broker login - Conditional OTP','Flow to determine if the OTP is required for the authentication','master','basic-flow',_binary '\0',_binary ''),('f7a037ca-cba6-436c-b37a-0151280bcc91','http challenge','An authentication flow based on challenge-response HTTP Authentication Schemes','master','basic-flow',_binary '',_binary ''),('fcd3f9ff-77ca-4e2d-9c0e-71d32053d75f','registration','registration flow','master','basic-flow',_binary '',_binary '');
/*!40000 ALTER TABLE `AUTHENTICATION_FLOW` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATOR_CONFIG`
--

DROP TABLE IF EXISTS `AUTHENTICATOR_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATOR_CONFIG` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_AUTH_CONFIG_REALM` (`REALM_ID`),
  CONSTRAINT `FK_AUTH_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATOR_CONFIG`
--

LOCK TABLES `AUTHENTICATOR_CONFIG` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG` DISABLE KEYS */;
INSERT INTO `AUTHENTICATOR_CONFIG` VALUES ('03ee9d90-244a-4f13-a83b-74a6ded0105e','create unique user config','Sc-project'),('3a7c3d3a-d0b9-4dc6-a61b-d64147a2d48c','review profile config','Sc-project'),('b4efc01c-9950-4291-b13c-c9cdeaac6775','review profile config','master'),('c7da533a-3278-4464-8153-9741cc26f397','create unique user config','master');
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUTHENTICATOR_CONFIG_ENTRY`
--

DROP TABLE IF EXISTS `AUTHENTICATOR_CONFIG_ENTRY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AUTHENTICATOR_CONFIG_ENTRY` (
  `AUTHENTICATOR_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`AUTHENTICATOR_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTHENTICATOR_CONFIG_ENTRY`
--

LOCK TABLES `AUTHENTICATOR_CONFIG_ENTRY` WRITE;
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG_ENTRY` DISABLE KEYS */;
INSERT INTO `AUTHENTICATOR_CONFIG_ENTRY` VALUES ('03ee9d90-244a-4f13-a83b-74a6ded0105e','false','require.password.update.after.registration'),('3a7c3d3a-d0b9-4dc6-a61b-d64147a2d48c','missing','update.profile.on.first.login'),('b4efc01c-9950-4291-b13c-c9cdeaac6775','missing','update.profile.on.first.login'),('c7da533a-3278-4464-8153-9741cc26f397','false','require.password.update.after.registration');
/*!40000 ALTER TABLE `AUTHENTICATOR_CONFIG_ENTRY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BROKER_LINK`
--

DROP TABLE IF EXISTS `BROKER_LINK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `BROKER_LINK` (
  `IDENTITY_PROVIDER` varchar(255) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `BROKER_USER_ID` varchar(255) DEFAULT NULL,
  `BROKER_USERNAME` varchar(255) DEFAULT NULL,
  `TOKEN` text,
  `USER_ID` varchar(255) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BROKER_LINK`
--

LOCK TABLES `BROKER_LINK` WRITE;
/*!40000 ALTER TABLE `BROKER_LINK` DISABLE KEYS */;
/*!40000 ALTER TABLE `BROKER_LINK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT`
--

DROP TABLE IF EXISTS `CLIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT` (
  `ID` varchar(36) NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FULL_SCOPE_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int DEFAULT NULL,
  `PUBLIC_CLIENT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` varchar(255) DEFAULT NULL,
  `BASE_URL` varchar(255) DEFAULT NULL,
  `BEARER_ONLY` bit(1) NOT NULL DEFAULT b'0',
  `MANAGEMENT_URL` varchar(255) DEFAULT NULL,
  `SURROGATE_AUTH_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) DEFAULT NULL,
  `PROTOCOL` varchar(255) DEFAULT NULL,
  `NODE_REREG_TIMEOUT` int DEFAULT '0',
  `FRONTCHANNEL_LOGOUT` bit(1) NOT NULL DEFAULT b'0',
  `CONSENT_REQUIRED` bit(1) NOT NULL DEFAULT b'0',
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `SERVICE_ACCOUNTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `CLIENT_AUTHENTICATOR_TYPE` varchar(255) DEFAULT NULL,
  `ROOT_URL` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REGISTRATION_TOKEN` varchar(255) DEFAULT NULL,
  `STANDARD_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'1',
  `IMPLICIT_FLOW_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DIRECT_ACCESS_GRANTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ALWAYS_DISPLAY_IN_CONSOLE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_B71CJLBENV945RB6GCON438AT` (`REALM_ID`,`CLIENT_ID`),
  KEY `IDX_CLIENT_ID` (`CLIENT_ID`),
  CONSTRAINT `FK_P56CTINXXB9GSK57FO49F9TAC` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT`
--

LOCK TABLES `CLIENT` WRITE;
/*!40000 ALTER TABLE `CLIENT` DISABLE KEYS */;
INSERT INTO `CLIENT` VALUES ('1b9f755b-72b9-4ee4-8fc6-132ab0556852',_binary '',_binary '','sc-user',0,_binary '','cabf54bb-5d19-4bb1-b941-8d1ff565fb51',NULL,_binary '\0','http://localhost:8081',_binary '\0','Sc-project','openid-connect',-1,_binary '\0',_binary '\0',NULL,_binary '\0','client-secret','http://localhost:8081',NULL,NULL,_binary '',_binary '\0',_binary '',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '',_binary '\0','account',0,_binary '\0','b1647491-7f73-49eb-a1e3-38ca8015388c','/realms/master/account/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_account}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('3510d930-aa13-46d9-bfd7-730a324d04da',_binary '',_binary '','Sc-project-realm',0,_binary '\0','2d84d6e5-4c17-4a14-b4a6-788f35eb40ef',NULL,_binary '',NULL,_binary '\0','master',NULL,0,_binary '\0',_binary '\0','Sc-project Realm',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '',_binary '\0','realm-management',0,_binary '\0','d5017687-a236-4bdb-9c63-d055c2c43bc3',NULL,_binary '',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_realm-management}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549',_binary '',_binary '','master-realm',0,_binary '\0','8ed5fea3-209e-4b80-ae60-6139bc16a3ee',NULL,_binary '',NULL,_binary '\0','master',NULL,0,_binary '\0',_binary '\0','master Realm',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107',_binary '',_binary '\0','admin-cli',0,_binary '','196b8794-cbb2-4d12-b502-7e3ac72711b7',NULL,_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_admin-cli}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '\0',_binary '\0',_binary '',_binary '\0'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e',_binary '',_binary '\0','security-admin-console',0,_binary '','549553d1-47f6-4412-95be-7e6e14529303','/admin/master/console/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_security-admin-console}',_binary '\0','client-secret','${authAdminUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('7884858a-7fa3-48ac-bfbd-01d20dbaf736',_binary '',_binary '\0','broker',0,_binary '\0','43838500-fc58-43b2-a086-695decfd689b',NULL,_binary '\0',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_broker}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('8763ceef-d83f-4487-bc9f-e42470c5cb10',_binary '',_binary '\0','admin-cli',0,_binary '','0cb02733-1b1c-44e2-9849-9d99a513c0b8',NULL,_binary '\0',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_admin-cli}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '\0',_binary '\0',_binary '',_binary '\0'),('9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '',_binary '\0','account',0,_binary '\0','d38f883c-53f9-4202-a991-e24b054e9910','/realms/Sc-project/account/',_binary '\0',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_account}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563',_binary '',_binary '\0','broker',0,_binary '\0','10b1f393-8d05-4358-857a-30d90f5b45ce',NULL,_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_broker}',_binary '\0','client-secret',NULL,NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('c16a2068-7765-47d5-b17e-c9079f96cbbd',_binary '',_binary '\0','account-console',0,_binary '','f530f97a-a791-4b95-ab39-44fcf3a8458c','/realms/Sc-project/account/',_binary '\0',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_account-console}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5',_binary '',_binary '\0','account-console',0,_binary '','eb6bf5b6-af02-4882-8cb1-732be9df5c31','/realms/master/account/',_binary '\0',NULL,_binary '\0','master','openid-connect',0,_binary '\0',_binary '\0','${client_account-console}',_binary '\0','client-secret','${authBaseUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9',_binary '',_binary '\0','security-admin-console',0,_binary '','8180a93a-2d16-49d5-99ce-c5789476bcce','/admin/Sc-project/console/',_binary '\0',NULL,_binary '\0','Sc-project','openid-connect',0,_binary '\0',_binary '\0','${client_security-admin-console}',_binary '\0','client-secret','${authAdminUrl}',NULL,NULL,_binary '',_binary '\0',_binary '\0',_binary '\0'),('f092c270-84ce-44f5-8649-e1fd9714d24b',_binary '',_binary '','sc-ui',0,_binary '','630d583f-efc4-4c29-bc1a-3d42efba2895',NULL,_binary '\0','http://localhost:4200',_binary '\0','Sc-project','openid-connect',-1,_binary '\0',_binary '\0',NULL,_binary '\0','client-secret','http://localhost:4200',NULL,NULL,_binary '',_binary '\0',_binary '',_binary '\0');
/*!40000 ALTER TABLE `CLIENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_ATTRIBUTES`
--

DROP TABLE IF EXISTS `CLIENT_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_ATTRIBUTES` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` text,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  CONSTRAINT `FK3C47C64BEACCA966` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_ATTRIBUTES`
--

LOCK TABLES `CLIENT_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `CLIENT_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `CLIENT_ATTRIBUTES` VALUES ('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','display.on.consent.screen'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','exclude.session.state.from.auth.response'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml_force_name_id_format'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.assertion.signature'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.authnstatement'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.client.signature'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.encrypt'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.force.post.binding'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.multivalued.roles'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.onetimeuse.condition'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.server.signature'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','saml.server.signature.keyinfo.ext'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','false','tls.client.certificate.bound.access.tokens'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','S256','pkce.code.challenge.method'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','S256','pkce.code.challenge.method'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','S256','pkce.code.challenge.method'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','S256','pkce.code.challenge.method'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','display.on.consent.screen'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','exclude.session.state.from.auth.response'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml_force_name_id_format'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.assertion.signature'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.authnstatement'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.client.signature'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.encrypt'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.force.post.binding'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.multivalued.roles'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.onetimeuse.condition'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.server.signature'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','saml.server.signature.keyinfo.ext'),('f092c270-84ce-44f5-8649-e1fd9714d24b','false','tls.client.certificate.bound.access.tokens');
/*!40000 ALTER TABLE `CLIENT_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_AUTH_FLOW_BINDINGS`
--

DROP TABLE IF EXISTS `CLIENT_AUTH_FLOW_BINDINGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_AUTH_FLOW_BINDINGS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `FLOW_ID` varchar(36) DEFAULT NULL,
  `BINDING_NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`BINDING_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_AUTH_FLOW_BINDINGS`
--

LOCK TABLES `CLIENT_AUTH_FLOW_BINDINGS` WRITE;
/*!40000 ALTER TABLE `CLIENT_AUTH_FLOW_BINDINGS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_AUTH_FLOW_BINDINGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_DEFAULT_ROLES`
--

DROP TABLE IF EXISTS `CLIENT_DEFAULT_ROLES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_DEFAULT_ROLES` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`ROLE_ID`),
  UNIQUE KEY `UK_8AELWNIBJI49AVXSRTUF6XJOW` (`ROLE_ID`),
  KEY `IDX_CLIENT_DEF_ROLES_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_8AELWNIBJI49AVXSRTUF6XJOW` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`),
  CONSTRAINT `FK_NUILTS7KLWQW2H8M2B5JOYTKY` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_DEFAULT_ROLES`
--

LOCK TABLES `CLIENT_DEFAULT_ROLES` WRITE;
/*!40000 ALTER TABLE `CLIENT_DEFAULT_ROLES` DISABLE KEYS */;
INSERT INTO `CLIENT_DEFAULT_ROLES` VALUES ('9de038f3-8dcd-4169-8514-c3ae3561ea90','0b7a6b42-19fd-49b0-a3bd-b8e300f0b209'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','704996b1-eb75-4c54-84c2-035e0fed2c5a'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','a3620b79-c24b-4aed-86ed-016ce53e1190'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','e5e27e00-90d6-464b-9560-d8af8b88272c');
/*!40000 ALTER TABLE `CLIENT_DEFAULT_ROLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_INITIAL_ACCESS`
--

DROP TABLE IF EXISTS `CLIENT_INITIAL_ACCESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_INITIAL_ACCESS` (
  `ID` varchar(36) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `EXPIRATION` int DEFAULT NULL,
  `COUNT` int DEFAULT NULL,
  `REMAINING_COUNT` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_INIT_ACC_REALM` (`REALM_ID`),
  CONSTRAINT `FK_CLIENT_INIT_ACC_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_INITIAL_ACCESS`
--

LOCK TABLES `CLIENT_INITIAL_ACCESS` WRITE;
/*!40000 ALTER TABLE `CLIENT_INITIAL_ACCESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_INITIAL_ACCESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_NODE_REGISTRATIONS`
--

DROP TABLE IF EXISTS `CLIENT_NODE_REGISTRATIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_NODE_REGISTRATIONS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` int DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`NAME`),
  CONSTRAINT `FK4129723BA992F594` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_NODE_REGISTRATIONS`
--

LOCK TABLES `CLIENT_NODE_REGISTRATIONS` WRITE;
/*!40000 ALTER TABLE `CLIENT_NODE_REGISTRATIONS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_NODE_REGISTRATIONS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `PROTOCOL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CLI_SCOPE` (`REALM_ID`,`NAME`),
  KEY `IDX_REALM_CLSCOPE` (`REALM_ID`),
  CONSTRAINT `FK_REALM_CLI_SCOPE` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE`
--

LOCK TABLES `CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE` VALUES ('0a2ff77e-0d2e-43ea-b777-5b32344e688e','address','master','OpenID Connect built-in scope: address','openid-connect'),('0e72584e-7e30-484f-91c7-f7166f7770a8','email','Sc-project','OpenID Connect built-in scope: email','openid-connect'),('1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5','web-origins','Sc-project','OpenID Connect scope for add allowed web origins to the access token','openid-connect'),('26df8b91-6c5e-436e-8f71-834ae2cffc80','web-origins','master','OpenID Connect scope for add allowed web origins to the access token','openid-connect'),('3adad139-0c20-4b06-9440-f00fca6e97b4','profile','master','OpenID Connect built-in scope: profile','openid-connect'),('3fda5dcc-0d7a-493b-9450-e177852a507d','profile','Sc-project','OpenID Connect built-in scope: profile','openid-connect'),('64820772-1d17-457f-a114-8a6ffa9a3a3d','email','master','OpenID Connect built-in scope: email','openid-connect'),('66999a59-b339-4571-95dd-d845da05f339','address','Sc-project','OpenID Connect built-in scope: address','openid-connect'),('75cf7c60-5290-4cbc-9bd7-82043f072751','role_list','Sc-project','SAML role list','saml'),('76a0fe56-946c-448d-aa3f-f9c44b405229','offline_access','master','OpenID Connect built-in scope: offline_access','openid-connect'),('78f4d7da-e8f1-43fb-8e6b-e43a14b16628','phone','Sc-project','OpenID Connect built-in scope: phone','openid-connect'),('7d130904-f3a2-432b-b54a-7eb1d8048611','roles','master','OpenID Connect scope for add user roles to the access token','openid-connect'),('a8db9c91-8759-4cf8-83dd-683470d8f0cb','microprofile-jwt','Sc-project','Microprofile - JWT built-in scope','openid-connect'),('aec8c585-83ed-4419-a433-9532b35b3dc7','microprofile-jwt','master','Microprofile - JWT built-in scope','openid-connect'),('cbabf283-6a06-4914-8f29-44edc0b3e469','roles','Sc-project','OpenID Connect scope for add user roles to the access token','openid-connect'),('df8bc044-2e83-45ee-a483-97db61775541','offline_access','Sc-project','OpenID Connect built-in scope: offline_access','openid-connect'),('e43593c4-d629-41bd-a69e-a8f6bb8a9a8c','phone','master','OpenID Connect built-in scope: phone','openid-connect'),('f4029215-37ac-461b-bd03-e03f4018bd5a','role_list','master','SAML role list','saml');
/*!40000 ALTER TABLE `CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_ATTRIBUTES`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_ATTRIBUTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_ATTRIBUTES` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `VALUE` text,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`NAME`),
  KEY `IDX_CLSCOPE_ATTRS` (`SCOPE_ID`),
  CONSTRAINT `FK_CL_SCOPE_ATTR_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_ATTRIBUTES`
--

LOCK TABLES `CLIENT_SCOPE_ATTRIBUTES` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_ATTRIBUTES` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_ATTRIBUTES` VALUES ('0a2ff77e-0d2e-43ea-b777-5b32344e688e','${addressScopeConsentText}','consent.screen.text'),('0a2ff77e-0d2e-43ea-b777-5b32344e688e','true','display.on.consent.screen'),('0a2ff77e-0d2e-43ea-b777-5b32344e688e','true','include.in.token.scope'),('0e72584e-7e30-484f-91c7-f7166f7770a8','${emailScopeConsentText}','consent.screen.text'),('0e72584e-7e30-484f-91c7-f7166f7770a8','true','display.on.consent.screen'),('0e72584e-7e30-484f-91c7-f7166f7770a8','true','include.in.token.scope'),('1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5','','consent.screen.text'),('1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5','false','display.on.consent.screen'),('1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5','false','include.in.token.scope'),('26df8b91-6c5e-436e-8f71-834ae2cffc80','','consent.screen.text'),('26df8b91-6c5e-436e-8f71-834ae2cffc80','false','display.on.consent.screen'),('26df8b91-6c5e-436e-8f71-834ae2cffc80','false','include.in.token.scope'),('3adad139-0c20-4b06-9440-f00fca6e97b4','${profileScopeConsentText}','consent.screen.text'),('3adad139-0c20-4b06-9440-f00fca6e97b4','true','display.on.consent.screen'),('3adad139-0c20-4b06-9440-f00fca6e97b4','true','include.in.token.scope'),('3fda5dcc-0d7a-493b-9450-e177852a507d','${profileScopeConsentText}','consent.screen.text'),('3fda5dcc-0d7a-493b-9450-e177852a507d','true','display.on.consent.screen'),('3fda5dcc-0d7a-493b-9450-e177852a507d','true','include.in.token.scope'),('64820772-1d17-457f-a114-8a6ffa9a3a3d','${emailScopeConsentText}','consent.screen.text'),('64820772-1d17-457f-a114-8a6ffa9a3a3d','true','display.on.consent.screen'),('64820772-1d17-457f-a114-8a6ffa9a3a3d','true','include.in.token.scope'),('66999a59-b339-4571-95dd-d845da05f339','${addressScopeConsentText}','consent.screen.text'),('66999a59-b339-4571-95dd-d845da05f339','true','display.on.consent.screen'),('66999a59-b339-4571-95dd-d845da05f339','true','include.in.token.scope'),('75cf7c60-5290-4cbc-9bd7-82043f072751','${samlRoleListScopeConsentText}','consent.screen.text'),('75cf7c60-5290-4cbc-9bd7-82043f072751','true','display.on.consent.screen'),('76a0fe56-946c-448d-aa3f-f9c44b405229','${offlineAccessScopeConsentText}','consent.screen.text'),('76a0fe56-946c-448d-aa3f-f9c44b405229','true','display.on.consent.screen'),('78f4d7da-e8f1-43fb-8e6b-e43a14b16628','${phoneScopeConsentText}','consent.screen.text'),('78f4d7da-e8f1-43fb-8e6b-e43a14b16628','true','display.on.consent.screen'),('78f4d7da-e8f1-43fb-8e6b-e43a14b16628','true','include.in.token.scope'),('7d130904-f3a2-432b-b54a-7eb1d8048611','${rolesScopeConsentText}','consent.screen.text'),('7d130904-f3a2-432b-b54a-7eb1d8048611','true','display.on.consent.screen'),('7d130904-f3a2-432b-b54a-7eb1d8048611','false','include.in.token.scope'),('a8db9c91-8759-4cf8-83dd-683470d8f0cb','false','display.on.consent.screen'),('a8db9c91-8759-4cf8-83dd-683470d8f0cb','true','include.in.token.scope'),('aec8c585-83ed-4419-a433-9532b35b3dc7','false','display.on.consent.screen'),('aec8c585-83ed-4419-a433-9532b35b3dc7','true','include.in.token.scope'),('cbabf283-6a06-4914-8f29-44edc0b3e469','${rolesScopeConsentText}','consent.screen.text'),('cbabf283-6a06-4914-8f29-44edc0b3e469','true','display.on.consent.screen'),('cbabf283-6a06-4914-8f29-44edc0b3e469','false','include.in.token.scope'),('df8bc044-2e83-45ee-a483-97db61775541','${offlineAccessScopeConsentText}','consent.screen.text'),('df8bc044-2e83-45ee-a483-97db61775541','true','display.on.consent.screen'),('e43593c4-d629-41bd-a69e-a8f6bb8a9a8c','${phoneScopeConsentText}','consent.screen.text'),('e43593c4-d629-41bd-a69e-a8f6bb8a9a8c','true','display.on.consent.screen'),('e43593c4-d629-41bd-a69e-a8f6bb8a9a8c','true','include.in.token.scope'),('f4029215-37ac-461b-bd03-e03f4018bd5a','${samlRoleListScopeConsentText}','consent.screen.text'),('f4029215-37ac-461b-bd03-e03f4018bd5a','true','display.on.consent.screen');
/*!40000 ALTER TABLE `CLIENT_SCOPE_ATTRIBUTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_CLIENT`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_CLIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_CLIENT` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`CLIENT_ID`,`SCOPE_ID`),
  KEY `IDX_CLSCOPE_CL` (`CLIENT_ID`),
  KEY `IDX_CL_CLSCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_C_CLI_SCOPE_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`),
  CONSTRAINT `FK_C_CLI_SCOPE_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_CLIENT`
--

LOCK TABLES `CLIENT_SCOPE_CLIENT` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_CLIENT` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_CLIENT` VALUES ('1b9f755b-72b9-4ee4-8fc6-132ab0556852','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('3510d930-aa13-46d9-bfd7-730a324d04da','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('3510d930-aa13-46d9-bfd7-730a324d04da','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('3510d930-aa13-46d9-bfd7-730a324d04da','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('3510d930-aa13-46d9-bfd7-730a324d04da','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('3510d930-aa13-46d9-bfd7-730a324d04da','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('3510d930-aa13-46d9-bfd7-730a324d04da','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('3510d930-aa13-46d9-bfd7-730a324d04da','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('3510d930-aa13-46d9-bfd7-730a324d04da','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('3510d930-aa13-46d9-bfd7-730a324d04da','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('3659c4fb-dab0-460a-a07b-77bea31bb4c1','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('522586e3-ecfe-44af-8053-8859d3870549','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('522586e3-ecfe-44af-8053-8859d3870549','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('522586e3-ecfe-44af-8053-8859d3870549','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('522586e3-ecfe-44af-8053-8859d3870549','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('522586e3-ecfe-44af-8053-8859d3870549','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('522ee0c5-2b7a-4418-8a2a-f07f41d8e107','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('7884858a-7fa3-48ac-bfbd-01d20dbaf736','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('8763ceef-d83f-4487-bc9f-e42470c5cb10','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('8763ceef-d83f-4487-bc9f-e42470c5cb10','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('8763ceef-d83f-4487-bc9f-e42470c5cb10','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('8763ceef-d83f-4487-bc9f-e42470c5cb10','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('8763ceef-d83f-4487-bc9f-e42470c5cb10','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('8763ceef-d83f-4487-bc9f-e42470c5cb10','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('8763ceef-d83f-4487-bc9f-e42470c5cb10','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('8763ceef-d83f-4487-bc9f-e42470c5cb10','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('8763ceef-d83f-4487-bc9f-e42470c5cb10','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('9de038f3-8dcd-4169-8514-c3ae3561ea90','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('9de038f3-8dcd-4169-8514-c3ae3561ea90','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('9de038f3-8dcd-4169-8514-c3ae3561ea90','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('9de038f3-8dcd-4169-8514-c3ae3561ea90','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('9de038f3-8dcd-4169-8514-c3ae3561ea90','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('b87832b0-7d60-4836-ba48-d340ef312563','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('b87832b0-7d60-4836-ba48-d340ef312563','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('b87832b0-7d60-4836-ba48-d340ef312563','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('b87832b0-7d60-4836-ba48-d340ef312563','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('b87832b0-7d60-4836-ba48-d340ef312563','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('c16a2068-7765-47d5-b17e-c9079f96cbbd','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0'),('f092c270-84ce-44f5-8649-e1fd9714d24b','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('f092c270-84ce-44f5-8649-e1fd9714d24b','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('f092c270-84ce-44f5-8649-e1fd9714d24b','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('f092c270-84ce-44f5-8649-e1fd9714d24b','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('f092c270-84ce-44f5-8649-e1fd9714d24b','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('f092c270-84ce-44f5-8649-e1fd9714d24b','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('f092c270-84ce-44f5-8649-e1fd9714d24b','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('f092c270-84ce-44f5-8649-e1fd9714d24b','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('f092c270-84ce-44f5-8649-e1fd9714d24b','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0');
/*!40000 ALTER TABLE `CLIENT_SCOPE_CLIENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SCOPE_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `CLIENT_SCOPE_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SCOPE_ROLE_MAPPING` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`ROLE_ID`),
  KEY `IDX_CLSCOPE_ROLE` (`SCOPE_ID`),
  KEY `IDX_ROLE_CLSCOPE` (`ROLE_ID`),
  CONSTRAINT `FK_CL_SCOPE_RM_ROLE` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`),
  CONSTRAINT `FK_CL_SCOPE_RM_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SCOPE_ROLE_MAPPING`
--

LOCK TABLES `CLIENT_SCOPE_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `CLIENT_SCOPE_ROLE_MAPPING` DISABLE KEYS */;
INSERT INTO `CLIENT_SCOPE_ROLE_MAPPING` VALUES ('76a0fe56-946c-448d-aa3f-f9c44b405229','20c62e21-0357-4254-990d-fd6dba5c33e9'),('df8bc044-2e83-45ee-a483-97db61775541','031730c1-5ec1-4cde-845c-2d9001f1858d');
/*!40000 ALTER TABLE `CLIENT_SCOPE_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION`
--

DROP TABLE IF EXISTS `CLIENT_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(36) DEFAULT NULL,
  `REDIRECT_URI` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `SESSION_ID` varchar(36) DEFAULT NULL,
  `AUTH_METHOD` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `AUTH_USER_ID` varchar(36) DEFAULT NULL,
  `CURRENT_ACTION` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_CLIENT_SESSION_SESSION` (`SESSION_ID`),
  CONSTRAINT `FK_B4AO2VCVAT6UKAU74WBWTFQO1` FOREIGN KEY (`SESSION_ID`) REFERENCES `USER_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION`
--

LOCK TABLES `CLIENT_SESSION` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_AUTH_STATUS`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_AUTH_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_AUTH_STATUS` (
  `AUTHENTICATOR` varchar(36) NOT NULL,
  `STATUS` int DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`AUTHENTICATOR`),
  CONSTRAINT `AUTH_STATUS_CONSTRAINT` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_AUTH_STATUS`
--

LOCK TABLES `CLIENT_SESSION_AUTH_STATUS` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_AUTH_STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_AUTH_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_NOTE`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_NOTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51C2736` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_NOTE`
--

LOCK TABLES `CLIENT_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_PROT_MAPPER`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_PROT_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_PROT_MAPPER` (
  `PROTOCOL_MAPPER_ID` varchar(36) NOT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`PROTOCOL_MAPPER_ID`),
  CONSTRAINT `FK_33A8SGQW18I532811V7O2DK89` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_PROT_MAPPER`
--

LOCK TABLES `CLIENT_SESSION_PROT_MAPPER` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_PROT_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_PROT_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_SESSION_ROLE`
--

DROP TABLE IF EXISTS `CLIENT_SESSION_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_SESSION_ROLE` (
  `ROLE_ID` varchar(255) NOT NULL,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`ROLE_ID`),
  CONSTRAINT `FK_11B7SGQW18I532811V7O2DV76` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_SESSION_ROLE`
--

LOCK TABLES `CLIENT_SESSION_ROLE` WRITE;
/*!40000 ALTER TABLE `CLIENT_SESSION_ROLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_SESSION_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CLIENT_USER_SESSION_NOTE`
--

DROP TABLE IF EXISTS `CLIENT_USER_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CLIENT_USER_SESSION_NOTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` text,
  `CLIENT_SESSION` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_SESSION`,`NAME`),
  CONSTRAINT `FK_CL_USR_SES_NOTE` FOREIGN KEY (`CLIENT_SESSION`) REFERENCES `CLIENT_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENT_USER_SESSION_NOTE`
--

LOCK TABLES `CLIENT_USER_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `CLIENT_USER_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CLIENT_USER_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPONENT`
--

DROP TABLE IF EXISTS `COMPONENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPONENT` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PARENT_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_TYPE` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `SUB_TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPONENT_REALM` (`REALM_ID`),
  KEY `IDX_COMPONENT_PROVIDER_TYPE` (`PROVIDER_TYPE`),
  CONSTRAINT `FK_COMPONENT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPONENT`
--

LOCK TABLES `COMPONENT` WRITE;
/*!40000 ALTER TABLE `COMPONENT` DISABLE KEYS */;
INSERT INTO `COMPONENT` VALUES ('01ad3662-5f3e-4788-a307-8e7be98ae70a','hmac-generated','Sc-project','hmac-generated','org.keycloak.keys.KeyProvider','Sc-project',NULL),('044dbaa5-1989-4ac6-8f59-8d34aa32abe5','aes-generated','master','aes-generated','org.keycloak.keys.KeyProvider','master',NULL),('1066605d-5197-4d48-82a2-8e66b5d4d785','Trusted Hosts','master','trusted-hosts','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('1d405328-7bd2-4656-a7f5-410d2666d1bc','Allowed Protocol Mapper Types','Sc-project','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('29a8cb3b-7d18-4326-9afb-fbe317655972','Allowed Protocol Mapper Types','Sc-project','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','authenticated'),('2baeb55c-ed0d-4f4a-96dc-af769c90c59f','Allowed Protocol Mapper Types','master','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','authenticated'),('3a3be003-70b2-4a73-a5d8-0400a698094d','Allowed Client Scopes','master','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('412e9a8e-19aa-4905-8feb-84b0ee4e1831','Full Scope Disabled','Sc-project','scope','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('4407bfe1-5345-4933-9217-0bb8b3194231','Max Clients Limit','Sc-project','max-clients','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('4b569367-d17c-441f-9815-5d8201fa1db2','Allowed Client Scopes','Sc-project','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('5b82797a-4a3d-419a-8f97-f807127dd53e','Trusted Hosts','Sc-project','trusted-hosts','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('6bfb85ca-d977-4a53-957a-fe04b76de3b0','Full Scope Disabled','master','scope','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('8abcc772-8450-4904-bbd7-03417d42af01','hmac-generated','master','hmac-generated','org.keycloak.keys.KeyProvider','master',NULL),('93a98a6f-549e-4d2f-b040-2a783bfd6c39','Allowed Protocol Mapper Types','master','allowed-protocol-mappers','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('a32978bb-fa47-406d-8ba7-3f12daf39880','rsa-generated','Sc-project','rsa-generated','org.keycloak.keys.KeyProvider','Sc-project',NULL),('b585b7ee-83e9-41d1-afd9-ea56a35b63e9','Consent Required','master','consent-required','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('c02e8d07-1872-453c-a1be-57db4b46dc03','Consent Required','Sc-project','consent-required','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','anonymous'),('d3b9969e-ebac-4d8a-8819-de54e9d462c5','Allowed Client Scopes','Sc-project','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','Sc-project','authenticated'),('dda698ae-5f73-4d7c-aa46-a1d181ab877e','Max Clients Limit','master','max-clients','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','anonymous'),('e2f6ca25-8dce-4895-a74c-fc2d7e23a7d9','aes-generated','Sc-project','aes-generated','org.keycloak.keys.KeyProvider','Sc-project',NULL),('f20ab6f5-5431-48ce-90d9-172762d3d2a2','Allowed Client Scopes','master','allowed-client-templates','org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy','master','authenticated'),('f70a4e30-fe52-494a-87e7-293de891d2f1','rsa-generated','master','rsa-generated','org.keycloak.keys.KeyProvider','master',NULL);
/*!40000 ALTER TABLE `COMPONENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPONENT_CONFIG`
--

DROP TABLE IF EXISTS `COMPONENT_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPONENT_CONFIG` (
  `ID` varchar(36) NOT NULL,
  `COMPONENT_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_COMPO_CONFIG_COMPO` (`COMPONENT_ID`),
  CONSTRAINT `FK_COMPONENT_CONFIG` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `COMPONENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPONENT_CONFIG`
--

LOCK TABLES `COMPONENT_CONFIG` WRITE;
/*!40000 ALTER TABLE `COMPONENT_CONFIG` DISABLE KEYS */;
INSERT INTO `COMPONENT_CONFIG` VALUES ('010f1820-c978-4675-aeb3-86afb30bca69','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('0254d504-e357-49f2-bbc6-83aea1656bde','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('0e1c7dda-a98a-453f-93ca-8ed05a66e310','8abcc772-8450-4904-bbd7-03417d42af01','kid','2ce2fe80-76b4-4e68-a236-6e406d3e53ea'),('11335776-67bc-4ca4-9801-34003b8ccff4','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','saml-role-list-mapper'),('127dde17-49ff-4ed1-8e8a-58d04c57312f','5b82797a-4a3d-419a-8f97-f807127dd53e','client-uris-must-match','true'),('1759cb70-446f-4c32-b810-b794910bf702','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','saml-role-list-mapper'),('1773e52a-180a-4f36-a3f1-f3297586d9c3','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','oidc-address-mapper'),('18e943b0-c9b3-4aa0-8d23-12075cd91c7f','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('1939799d-8aa3-4fd2-b356-4c1f87a2612f','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('1ae12301-e829-49ed-8580-79b3cb6f57d7','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','saml-user-property-mapper'),('23ef7ee1-6bae-4866-82b1-815279fbbbfb','1066605d-5197-4d48-82a2-8e66b5d4d785','client-uris-must-match','true'),('298c77ff-15db-4f72-9c4a-0526d5d557c8','d3b9969e-ebac-4d8a-8819-de54e9d462c5','allow-default-scopes','true'),('31c5e607-b421-418b-aa58-44d9f451e2dd','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','oidc-address-mapper'),('3606d7f0-8bca-4611-8fdf-558ad69a63fc','3a3be003-70b2-4a73-a5d8-0400a698094d','allow-default-scopes','true'),('3efdda38-ef0a-499a-b44c-4866490f1d42','01ad3662-5f3e-4788-a307-8e7be98ae70a','priority','100'),('3fd4d8b0-cb61-4c54-82ac-ebd31b72683d','8abcc772-8450-4904-bbd7-03417d42af01','secret','PyIqfP22rSq-m0q5EGi6Z_o3bRaVTUT-u-SnVlEw-DfTeqsIu6Jiqz4uXbLMKAQoLFU--kZTbqvZUmNB_uup7g'),('440c00bb-6ab8-4626-bedc-7b08b94f6e7b','e2f6ca25-8dce-4895-a74c-fc2d7e23a7d9','kid','3d08adab-8704-40dd-b212-23fbac4c980a'),('46d68d58-8297-4404-b48c-0dbaef6cada2','f70a4e30-fe52-494a-87e7-293de891d2f1','priority','100'),('47573f2d-a50f-4a2f-8499-0f529dbf88b9','044dbaa5-1989-4ac6-8f59-8d34aa32abe5','kid','f5720473-d6a8-44ed-acdf-0803826013e0'),('4934e950-588a-4ede-a88c-e69be1f51910','4407bfe1-5345-4933-9217-0bb8b3194231','max-clients','200'),('4c9bd913-813a-4a16-9fae-971c35099ab1','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('5078191f-a8f9-4880-be17-f687d718bce4','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','saml-user-property-mapper'),('50b87ec4-a60f-4dfd-a489-fc09de734819','5b82797a-4a3d-419a-8f97-f807127dd53e','host-sending-registration-request-must-match','true'),('50d4ddba-0340-465b-a0a3-bbe884c80567','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('5db3cbba-e197-4e00-97c8-0fa1b15181f1','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','oidc-full-name-mapper'),('630f099c-415d-4095-8a7f-8a4716b154c9','e2f6ca25-8dce-4895-a74c-fc2d7e23a7d9','secret','-R6lkjhIVa1qVegyhp7hLg'),('66ebc91f-9bb7-4cb3-bbd0-8941bb723cc0','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','oidc-full-name-mapper'),('6c03ac5f-8841-4c1a-980a-557073bfae5c','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('6ee04243-4bd7-435e-b976-c10320aeeffb','a32978bb-fa47-406d-8ba7-3f12daf39880','certificate','MIICozCCAYsCBgFxTKMAYjANBgkqhkiG9w0BAQsFADAVMRMwEQYDVQQDDApTYy1wcm9qZWN0MB4XDTIwMDQwNTIzMTYyM1oXDTMwMDQwNTIzMTgwM1owFTETMBEGA1UEAwwKU2MtcHJvamVjdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKVhsJ0BP/yjMxfKlUn1A/zB8WOLtoZ+uw8eQNWxy/lEF/CdgHKnbzgXdTtmIRtPcGKI7QaBIP4gQ+NkU2geU9W4sAFiVBMr0ZvfLwfI1sqDWotzaRa83XBxqYZ442J2I8J0G9yEorCCQDMlHXaeSpcZi7zdFu8FDbPVTHkkdz4SbeWXmwqHlYt+SAn+NmjCp2p9EJWqXlfaOa33OKJcFJWgv0rE04Qr+aKCcwNweeYyoGEj97+ms8qcPHzNF1xFJ7JDee80RUe+4l1dqEVErqM+/qxoF2e6k7wwP2BDS9iPTBailnyX2ongdT/bUgrTalk82yhq38SAZFWym56sbq8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAZynuMnrW+JTV9VJe6irVObwWVnAe98gCjCAczLHcsjMUNZo4ZjgEGW6ZnVMIGu86RxdX4ZTUunCAG/mR6dZiOD+jskT3AXGE0TNK5uePOuvThUpPa0uAM8ZUDV+Dq4Otq01A5N2H6/Ik7mrOsixvnC3qxIWYA0XibJhg087NUFomK5vcCDaeI4uX67XPzGlGIvj8MaPFHK40J/Asyb5YiFYuMpMCXxsdxki5EDK47SgPe57POh56rEg0b0O0DZQZLLq6zH4P5b1LzGcJrRDm/40MhYPynq2J1ZGdA074h4rsYqj1rXmHBe5WIa1CRWtc8+/o/NooJoKQ2M+TLqCxUg=='),('756f7236-bcce-464f-ac79-ae00412b5a47','8abcc772-8450-4904-bbd7-03417d42af01','priority','100'),('76e8eb77-1621-48f7-8c49-3cf26cb7ea40','1066605d-5197-4d48-82a2-8e66b5d4d785','host-sending-registration-request-must-match','true'),('7f0bd828-d531-47f3-a90f-fe9ff1027e97','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('81dc5fb6-de04-4248-9e6b-869db66cc6c0','f70a4e30-fe52-494a-87e7-293de891d2f1','certificate','MIICmzCCAYMCBgFxTJ/iAzANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjAwNDA1MjMxMjU4WhcNMzAwNDA1MjMxNDM4WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCFeTXsaGgwxO/BU5LjPU5YW2xlzY4rGzEieEqPWlaNEsLjwKLcapqpXTWMZFTv7WKtqzcALqY3631f19UJDhg8ZPr4gjt46bMNEf2zDK7Dbgy3fxitHfKppDKs/gwJEIXNnj3f+8oGtHaqLWqP8KjNHSeT6Qgza8i6Qu2ZV9+eMHtyevATipI4STWKGXxM1B7TrqBaOZtNSQ7lLEuIQ36/IocHPMZIld+zilR6f951hAp2/0huLPmYQ2X0CHF785wFbWDMs197/fiheph+nUwadToXRzKFTW7vC8Hoh8HB5Jb0OfhU8UIjqDglpRXvVdMQz4b4t+BU6h1/MDs/frL3AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAECC6UA/bDVPMnayezuIWOgC3L5ExHFdRnECtS1h0ALjQdnnCgPEzJ9wQvA2Q7VnsXWr4D3JQ/Rc+G4RMJoXNara4LyUzrr/7PsPEPenUdE7U8IO8E+u/HHtHG28M4cSWjbpfrvZ4S33WATAD27JQS4Av0g0jsJ/dCDUULGrcmc4GG92Ap5NA65B0X+EKzDbu98eBIUaPTZjIyyslQgNfaxSfqm4hCdkPHxBcZG8QPdQKkhdr5iQn7QqCI2GanAOxjVxPjRP3lp4rmRUymt4wc+9WV5TnyeSPUqjY+QIWSl/htYMzsONeFxfB2OkkPJ6wb0rqEZ0LxQh/je4wHJjfTU='),('81e34d18-f564-4608-aabc-2a90bf36ba1f','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','saml-user-property-mapper'),('81e92f79-9faf-4ce1-b0c5-672fb0392a5c','044dbaa5-1989-4ac6-8f59-8d34aa32abe5','secret','KmQNOE53BgAa8Vaq-HyvEQ'),('8b5505a7-8333-4740-841e-a0e15f3669a4','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','saml-role-list-mapper'),('8f48721b-937c-4427-902e-7dca914fef2f','01ad3662-5f3e-4788-a307-8e7be98ae70a','kid','f8d6d2a2-b88f-4dc3-8d39-ebca185b9dba'),('91e232b1-a2d5-4b5e-a4e7-69e93306a2b1','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('9a3a1b3f-4158-49fc-b2d5-5c3b565ee312','4b569367-d17c-441f-9815-5d8201fa1db2','allow-default-scopes','true'),('9a5788a1-1c99-46c2-bede-ab11ae91945b','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','oidc-usermodel-property-mapper'),('ac59ee81-2927-42d6-baf1-d6a60be8c4c5','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','oidc-full-name-mapper'),('ae0fc0c3-cd96-44f3-afb6-227ef153171e','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('b086928b-f757-46f9-8d2e-95a061c92e82','a32978bb-fa47-406d-8ba7-3f12daf39880','privateKey','MIIEogIBAAKCAQEApWGwnQE//KMzF8qVSfUD/MHxY4u2hn67Dx5A1bHL+UQX8J2AcqdvOBd1O2YhG09wYojtBoEg/iBD42RTaB5T1biwAWJUEyvRm98vB8jWyoNai3NpFrzdcHGphnjjYnYjwnQb3ISisIJAMyUddp5KlxmLvN0W7wUNs9VMeSR3PhJt5ZebCoeVi35ICf42aMKnan0QlapeV9o5rfc4olwUlaC/SsTThCv5ooJzA3B55jKgYSP3v6azypw8fM0XXEUnskN57zRFR77iXV2oRUSuoz7+rGgXZ7qTvDA/YENL2I9MFqKWfJfaieB1P9tSCtNqWTzbKGrfxIBkVbKbnqxurwIDAQABAoIBACUitoHK0g9spgBloPgmaGrNMeLkcGvq8R4H8Q8LCX5DaAzDkSAtsK+rtMtNt4lmYRn/HRokzL/Dhvr8gC4VAUYwXnUxGyfi4wJZLzpUQsbej+pYU/SjHCYvn51ub8CgPo9fexIdRlrUJI5XezIzgRREzDfwfUHWpKkI8E8TfSMWq132ONoGYLdHdk4lkW07u7Oeuo705Yf+RzHqzQU6uYK+ljPTdQ5b6YqElPXHyWRMC+c2Djz/MGuxiN+bKCHaPNUdCt6xRtWxy/FgD4FCGNTxZf3FB++k230at5/XW3BY5BBzaZxBEWU6tspCn0LHjIcAPjzcsrHzuF1QvLrffeECgYEA5RgZ3IHnDMyTlRLmEHI+T8BMnLkCFQnWqZM8yMpGrLWvma2HsUYCt2ZCLXWHGAfb5yDEsJ5WArHSB/X+7EPdKKF1ySfD00JFzzbaXxyDhcQhVLhlMbAh7vebnZQeAY+qre5biF2Ry5/ehfkSn/fKkWEl5pRAh4M8PU/oUbhQHqMCgYEAuM4FLPWb9qjs1CD8dhM8Kt0m84T9qFdXbQfWFIyW2qm799sAKage9kbcSdPUqR8G6DvQrL1MSIgFV4HvFhA5QjnBg3wB61MMWx1lsnyTVkHRI8835AkQwstKuJOXa7C08g1tXDNLrurpBfGoc6IM9w3+ycPrK4ksl7sbdTAdrIUCgYAxO5CzNqd0tLIyozEV77c8ZFMSix/LbaXYI3A4pl+x1gm6uPQkQKtsxmkKY+t4WpgDf6NFYYGSaqx8UX1LQPpSNQCfDhCjYeamtV8QeYw6Pv/6uPXmS7e/Jy8mGiKoqzs+za6WC6W9YMnteK1r+1BpFbuTZeDIPoe/lWCTz0xIiQKBgBTi/ATD0ZfEKdgjzkTOEbvKeO+G9WBEVtY0z4ZNCXO3QU5XCyKqJK2y/oy9i0h+ADrZH9GWJ3v/G5NzVQFKXh5VBsEEj/b30OqvXOIaYFCcabXBYtC8EY/iiBRqYe/C7gB5cgW8qm4fkKyiTmabRxnZFBXbtcWa4FZZyFWrVX89AoGAWeYvnOVP+75ZAqa27/5OS/uV+egcTBYMw1JEW/JAUDRPMQu9czLLDqLo1a/k7JetAPKwdKC+kdAZD6DgSAM2crM+P+8Msluqx9fxCrp05GzzAqe0vW59xHURwoHE1c+vlqAi4pmEyYwJ3OYxA27cXUCy8t45xL/A7BhoQ4pFRZQ='),('b8f0c4b1-dc37-4b85-9f48-0ab6f3596e3f','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','saml-user-property-mapper'),('c2efcc35-2af7-405d-b25d-27ed1dae45d8','8abcc772-8450-4904-bbd7-03417d42af01','algorithm','HS256'),('c362f19a-437e-4581-b975-6decc656c079','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','oidc-address-mapper'),('c5877b0f-afff-465d-b79a-ee29ac3fd944','a32978bb-fa47-406d-8ba7-3f12daf39880','priority','100'),('c6dc0317-25fd-44d1-96b9-8646fe4e55b7','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','saml-role-list-mapper'),('cd38a2f0-2704-49b2-be35-f9282d245071','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('d5adf69b-6d3f-4b57-865a-bd7ff56962a8','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','oidc-address-mapper'),('d95e64fb-bdea-4652-a836-c36d759cd19a','01ad3662-5f3e-4788-a307-8e7be98ae70a','algorithm','HS256'),('db0273e0-fa8c-4eb3-9927-941a70763d6e','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','oidc-full-name-mapper'),('dba24cb8-cbac-4e71-8509-611bc7e3850e','2baeb55c-ed0d-4f4a-96dc-af769c90c59f','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('dc44b292-99d8-4fe5-b0d5-06aaff0e3588','29a8cb3b-7d18-4326-9afb-fbe317655972','allowed-protocol-mapper-types','oidc-sha256-pairwise-sub-mapper'),('de0220f3-0639-4730-ab7a-3d9755b02c57','1d405328-7bd2-4656-a7f5-410d2666d1bc','allowed-protocol-mapper-types','oidc-usermodel-attribute-mapper'),('e1a83611-c6fe-4e73-b599-ea99252d05dc','01ad3662-5f3e-4788-a307-8e7be98ae70a','secret','sfo7JUAF1j1LXOPJr4uPhGiGmYkSonI38e04gOWt-BKSi9YKiGEgNGBCBdZRA9zn6R7OC5pNh9kh_ts9wIo7Cg'),('e1fb29dc-93b0-47d6-baa6-8baaba1f7a32','e2f6ca25-8dce-4895-a74c-fc2d7e23a7d9','priority','100'),('e82c3b69-13d8-4ea5-9ab2-86835a286897','93a98a6f-549e-4d2f-b040-2a783bfd6c39','allowed-protocol-mapper-types','saml-user-attribute-mapper'),('ea40fe04-318f-41af-8c16-56da8180a033','dda698ae-5f73-4d7c-aa46-a1d181ab877e','max-clients','200'),('f3819a7b-ce31-4a74-8595-e94e9a1ca610','044dbaa5-1989-4ac6-8f59-8d34aa32abe5','priority','100'),('f4d3e0b0-7195-4b5c-9f31-2a16d399ed04','f70a4e30-fe52-494a-87e7-293de891d2f1','privateKey','MIIEogIBAAKCAQEAhXk17GhoMMTvwVOS4z1OWFtsZc2OKxsxInhKj1pWjRLC48Ci3GqaqV01jGRU7+1iras3AC6mN+t9X9fVCQ4YPGT6+II7eOmzDRH9swyuw24Mt38YrR3yqaQyrP4MCRCFzZ493/vKBrR2qi1qj/CozR0nk+kIM2vIukLtmVffnjB7cnrwE4qSOEk1ihl8TNQe066gWjmbTUkO5SxLiEN+vyKHBzzGSJXfs4pUen/edYQKdv9Ibiz5mENl9Ahxe/OcBW1gzLNfe/34oXqYfp1MGnU6F0cyhU1u7wvB6IfBweSW9Dn4VPFCI6g4JaUV71XTEM+G+LfgVOodfzA7P36y9wIDAQABAoIBADn8AXAVn4XoWFv9aaesv3dlKD9FwIh7hKn75jBGx6j7yIzW6Cmnak8/ff7iq3+b+i2GXxuGKJHMjSfxBMytQdwi5mDFev65UB7SfQum69lc2stwvJCgnQYTnixHJYVelzJPl4PRx9+H0LF3j36+xyZsjwAT0Ys9PPOmFcBzS2NV/PnTskkEzq1ychCob/HkimJv1tEuQQmNGQNaZHeqZeTocSwuRtmL2Vk+rauXhEhjbLWU/i250dZrtANToOLqm8uGkiFAiTWhmTZrxDsyslTR0M44y2FX37gojuEQ2ZhNBInqj/DNJeshPnGXxAXCvDDdTDSkljbYDH0a7LaOmaECgYEAzubQn/4C/3Jgux+ixTwAMRjT5s3WaucUAY/8dqI8PV2k6ImPD+FTEr7clCjT8byrMXTVFx8cYN+he2+SL8klOn2uRDNaM7bvwzoLsH09fbhFiNX+IOyfObb+FmG3Dc6f7wUMOx/X7aL8LoJ+YjZxNdp+A3Kqoa8GDmJDRtMqeJECgYEApSWq3oppHxpPq5S/Kf5T51ZKTu9LYuXZ0XQfkUNfFJKpqxLWFaTfxumeaY59NuUI0SiiMl0HYAbolhFomLJDIW2KrP8uZxqV23zg3ncxIEGnQdSpU9PMSxF6FyjWYTL8dr3nXPLyaG88X+5kG/K8+sOyscSbszPLRCTjcXuqdwcCgYBQ8V226eH0W6PaMQM11EZi7sqfpfF3exWJkvH623djrooAayhr1wPv9nlHAT4BeYUj4QgodoRcpcCAEOMJDNEis96EUe69ApmEL+Y40pZG49tYMBeue8PDCYMX8Hxzy10oXjotu0R3dGbiLy0QcSoTHnpkEO0OOfHNmMWnJJ+d8QKBgFiinNCP7NeaoYPvc6IwNAwHTh6LZCSSRJOcBY1LYW6DQLjmhSxcrxXtMO5qYV3jExZe/p6VO24YhyBz+3Vox4rbZgBvfRItPJzuqyn7QsyTMBeu8apiFFV97c8cReMpIEMCrXg/lEbk3LyIEAbIAxF7kx7B7OcDN2c9fSzdOKJlAoGARt7iPAI8cQjJWZ3CayO01nd617k6PTl6d7Cldo+yW/FH8ZcbTb756kilQxMf1IzFvNCfIerRDwJsDI+OXF5MW0a4K66k1wDs63prdzdiHZ8NpWX5rqABzv26nRjuk2QCTiBcG2bjLxC/sl89+CE4XOpIc1xqYwurCe3fSQgBC2w='),('fc9e642d-c25f-4d60-a5ba-0f25e22599da','f20ab6f5-5431-48ce-90d9-172762d3d2a2','allow-default-scopes','true');
/*!40000 ALTER TABLE `COMPONENT_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPOSITE_ROLE`
--

DROP TABLE IF EXISTS `COMPOSITE_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COMPOSITE_ROLE` (
  `COMPOSITE` varchar(36) NOT NULL,
  `CHILD_ROLE` varchar(36) NOT NULL,
  PRIMARY KEY (`COMPOSITE`,`CHILD_ROLE`),
  KEY `IDX_COMPOSITE` (`COMPOSITE`),
  KEY `IDX_COMPOSITE_CHILD` (`CHILD_ROLE`),
  CONSTRAINT `FK_A63WVEKFTU8JO1PNJ81E7MCE2` FOREIGN KEY (`COMPOSITE`) REFERENCES `KEYCLOAK_ROLE` (`ID`),
  CONSTRAINT `FK_GR7THLLB9LU8Q4VQA4524JJY8` FOREIGN KEY (`CHILD_ROLE`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPOSITE_ROLE`
--

LOCK TABLES `COMPOSITE_ROLE` WRITE;
/*!40000 ALTER TABLE `COMPOSITE_ROLE` DISABLE KEYS */;
INSERT INTO `COMPOSITE_ROLE` VALUES ('2930bab7-ff20-4032-bfc2-f8377ccec89b','5495ab85-9f42-4594-a85e-94f41836b828'),('2930bab7-ff20-4032-bfc2-f8377ccec89b','59ab301e-4e7d-4cc5-87d8-5ba8ef330e64'),('49e96f8d-cabd-4601-9f9b-1d6b96b74257','125b3134-4b2f-4ae2-a379-3e8b918d675f'),('4d30d933-8c1c-44d3-82b3-74c38dc42ae3','5403fe89-090c-4ca5-886a-f102107c93d0'),('4d30d933-8c1c-44d3-82b3-74c38dc42ae3','e0df025a-d993-421e-a76c-a88dbf63388b'),('704996b1-eb75-4c54-84c2-035e0fed2c5a','3cdd1a30-c2e6-427b-8152-9302dfd799c0'),('8273524c-e028-4938-ba84-7a9f72850743','0c0e50b8-eddf-4179-89e9-6ac9ce02f1ce'),('8273524c-e028-4938-ba84-7a9f72850743','3f99e8f6-a03d-4e97-882d-d23c6f6bb2be'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','0c0e50b8-eddf-4179-89e9-6ac9ce02f1ce'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','125b3134-4b2f-4ae2-a379-3e8b918d675f'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','3f99e8f6-a03d-4e97-882d-d23c6f6bb2be'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','49e96f8d-cabd-4601-9f9b-1d6b96b74257'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','506702ed-8475-423a-93fa-c9bd81d679a1'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','5c7f9d01-d972-4e67-ac72-28570d658114'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','6496f525-f14b-4af1-9898-bc531cafb742'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','68b9623a-7e9b-4871-8b06-d6792bb7cd75'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','6f2620ed-05ce-484f-ae6e-4fb3250808f9'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','8273524c-e028-4938-ba84-7a9f72850743'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','8346b9a7-602a-4108-b4e5-ef9b98e435d2'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','c301ee88-d5de-4a16-a50e-d8ed4c20f5dd'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','d04b794d-4e7d-4e72-9cf2-a29a003da973'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','d3e043f1-24bd-4ea8-a839-91127690b166'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','dbeb4187-7b0e-4a3e-b0c9-1d8d199c5a13'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','ece0e553-fa10-405e-9b46-e6bea1ff2858'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','f762e32a-d8de-4f92-a21b-0003b32ff421'),('881b8bec-f570-44e6-8b71-f3b5b3b67041','fd695420-353a-4c77-8dde-f623a80aa390'),('89ba3d52-e8f7-49e6-ba67-512897ba91ea','022161f1-25a7-4b58-96ae-2b55c66b7291'),('9d9a491f-f0a6-4b87-b5bf-a964a8f9ba4a','3ee750fd-6512-4566-8a45-81a2059198fa'),('a3620b79-c24b-4aed-86ed-016ce53e1190','3790a5b4-efcc-457f-b134-fbd53ea93663'),('a90f1cd1-926b-4e9f-8400-504e15f00e66','1c88b02d-4ecd-404f-9e71-d465921d2242'),('aa75e4c0-43b6-4f3a-bd76-148e964f0190','137da2db-c968-4561-8a90-f9b2107efd62'),('b462d407-a386-4006-aec7-5b5c37223690','022161f1-25a7-4b58-96ae-2b55c66b7291'),('b462d407-a386-4006-aec7-5b5c37223690','08121c92-f09e-4668-a2d7-fdcc00abff8d'),('b462d407-a386-4006-aec7-5b5c37223690','0fa46056-1cf7-4c21-b1e3-80ebd31300b9'),('b462d407-a386-4006-aec7-5b5c37223690','0fe4fdaa-b05a-4a51-a4b8-c8956da45e47'),('b462d407-a386-4006-aec7-5b5c37223690','16c7f069-2527-46e1-9d34-deed6672675b'),('b462d407-a386-4006-aec7-5b5c37223690','1aadc70c-d72e-4d14-88ba-187d61010a08'),('b462d407-a386-4006-aec7-5b5c37223690','2930bab7-ff20-4032-bfc2-f8377ccec89b'),('b462d407-a386-4006-aec7-5b5c37223690','33c03778-c73f-4f37-a7e8-fd755d33faaf'),('b462d407-a386-4006-aec7-5b5c37223690','3ee750fd-6512-4566-8a45-81a2059198fa'),('b462d407-a386-4006-aec7-5b5c37223690','4446a8c0-4592-4d27-bcad-2aac6952a844'),('b462d407-a386-4006-aec7-5b5c37223690','477fe2dc-3cde-46bf-9051-4a74a25c07cc'),('b462d407-a386-4006-aec7-5b5c37223690','47ae124c-f13e-4d78-b33f-e3664fe40f52'),('b462d407-a386-4006-aec7-5b5c37223690','4a472544-8031-4fb9-8bc3-eb671936e041'),('b462d407-a386-4006-aec7-5b5c37223690','4d30d933-8c1c-44d3-82b3-74c38dc42ae3'),('b462d407-a386-4006-aec7-5b5c37223690','4ef4d7c7-5d1a-4cc2-92e4-f3dc02dfb450'),('b462d407-a386-4006-aec7-5b5c37223690','5403fe89-090c-4ca5-886a-f102107c93d0'),('b462d407-a386-4006-aec7-5b5c37223690','5495ab85-9f42-4594-a85e-94f41836b828'),('b462d407-a386-4006-aec7-5b5c37223690','582b5625-0473-4deb-a759-1dafa77c5bc3'),('b462d407-a386-4006-aec7-5b5c37223690','59ab301e-4e7d-4cc5-87d8-5ba8ef330e64'),('b462d407-a386-4006-aec7-5b5c37223690','60b4ea8a-8eeb-412a-b221-42757bd2a9aa'),('b462d407-a386-4006-aec7-5b5c37223690','61c2b816-fbf8-40bf-a61c-98be8746aa27'),('b462d407-a386-4006-aec7-5b5c37223690','65dc6ddc-245a-40d8-91ec-0dc6950eb5d6'),('b462d407-a386-4006-aec7-5b5c37223690','89ba3d52-e8f7-49e6-ba67-512897ba91ea'),('b462d407-a386-4006-aec7-5b5c37223690','9d9a491f-f0a6-4b87-b5bf-a964a8f9ba4a'),('b462d407-a386-4006-aec7-5b5c37223690','b29c9e22-ceb6-4142-83f4-c3904ca23cf2'),('b462d407-a386-4006-aec7-5b5c37223690','b8bd3ef2-3f5a-4681-9c32-186d453f2255'),('b462d407-a386-4006-aec7-5b5c37223690','c89c13b8-3119-49b5-974a-f2818d5f7ea4'),('b462d407-a386-4006-aec7-5b5c37223690','ca7d602a-949c-4774-acc7-0fa43a44205b'),('b462d407-a386-4006-aec7-5b5c37223690','cb93f475-d3d7-4919-9764-becaa77100d2'),('b462d407-a386-4006-aec7-5b5c37223690','d555e901-3a51-4aba-9456-cc1cb07fa59d'),('b462d407-a386-4006-aec7-5b5c37223690','e0df025a-d993-421e-a76c-a88dbf63388b'),('b462d407-a386-4006-aec7-5b5c37223690','e27295e8-029e-48f7-a5a3-e0938725eb0a'),('b462d407-a386-4006-aec7-5b5c37223690','e68bc928-06b7-4ac8-8431-42b276afcd14'),('b462d407-a386-4006-aec7-5b5c37223690','ec76a6fa-154a-4413-8a6f-7ae5cc1f8b4e'),('b462d407-a386-4006-aec7-5b5c37223690','f35938bc-b4e9-4684-ad60-d5bd911a86d0'),('b462d407-a386-4006-aec7-5b5c37223690','f8e23751-2b1c-454d-a3b4-8af5104e9022'),('b462d407-a386-4006-aec7-5b5c37223690','fa01cf60-a67d-4979-af23-d24d7ce9461b');
/*!40000 ALTER TABLE `COMPOSITE_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CREDENTIAL`
--

DROP TABLE IF EXISTS `CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CREDENTIAL` (
  `ID` varchar(36) NOT NULL,
  `SALT` tinyblob,
  `TYPE` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `USER_LABEL` varchar(255) DEFAULT NULL,
  `SECRET_DATA` longtext,
  `CREDENTIAL_DATA` longtext,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_CREDENTIAL` (`USER_ID`),
  CONSTRAINT `FK_PFYR0GLASQYL0DEI3KL69R6V0` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CREDENTIAL`
--

LOCK TABLES `CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `CREDENTIAL` DISABLE KEYS */;
INSERT INTO `CREDENTIAL` VALUES ('29d9b339-c15e-44b6-8594-12e47cd98fda',NULL,'password','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90',1586129111801,NULL,'{\"value\":\"J54MQ0M7slBLw4cZq0TEakzmC6B5gctuLPtL4XuIeOhQ5z8H+Co6+shFQTjvgBpSSPsuS2oZWXtdUf852QoX8g==\",\"salt\":\"RvO7cOiVbngHtZ9z1klzYQ==\"}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\"}',10),('66bc063b-25b6-4d4e-bfd8-16b30cc5d4bb',NULL,'password','78bf47b3-9d32-412d-8942-24e1def64a0b',1586128479533,NULL,'{\"value\":\"cwi3hB00jXiurqz6n9ue+ERgdMewOoSbucDafF+w19IX+o0y2qqA02K81wNBN5dyZOFM5cJfEJzSAj3fUTerzw==\",\"salt\":\"/R3lRWlhjmGcP204AUAgeA==\"}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\"}',10),('b2a346fe-f622-4e88-89cd-ee68a78fe752',NULL,'password','36044e7d-81de-4250-acee-f8962d9f857f',1586129098954,NULL,'{\"value\":\"RYMOjHf6ppKsrYp7O5WnqhhJJFBmvz02gDynGnudRHO/tT4rCJ9Q1T3i82HLhO6tPlCcrvhl18nbyL0/+MueTQ==\",\"salt\":\"K9Mz3cBBgnOJAjhdj3LSWg==\"}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\"}',10),('cca4670d-f83c-4c38-a4bb-e91c5f4a71fb',NULL,'password','87b0f71d-ee50-4d00-b433-0da7255245bb',1586129058206,NULL,'{\"value\":\"yEIOkq+48iMsK05Qmbbj7JtzPwxvjCdvLBOD6bUi128WjSX+rNNRaCyuhbMUheOYW30uwLYO4pjJOJk+XINKoQ==\",\"salt\":\"UhY8L8z+Ww50BxWlhwxkOg==\"}','{\"hashIterations\":27500,\"algorithm\":\"pbkdf2-sha256\"}',10);
/*!40000 ALTER TABLE `CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOG`
--

LOCK TABLES `DATABASECHANGELOG` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOG` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOG` VALUES ('1.0.0.Final-KEYCLOAK-5461','sthorger@redhat.com','META-INF/jpa-changelog-1.0.0.Final.xml','2020-04-05 23:13:08',1,'EXECUTED','7:4e70412f24a3f382c82183742ec79317','createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.0.0.Final-KEYCLOAK-5461','sthorger@redhat.com','META-INF/db2-jpa-changelog-1.0.0.Final.xml','2020-04-05 23:13:08',2,'MARK_RAN','7:cb16724583e9675711801c6875114f28','createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.1.0.Beta1','sthorger@redhat.com','META-INF/jpa-changelog-1.1.0.Beta1.xml','2020-04-05 23:13:09',3,'EXECUTED','7:0310eb8ba07cec616460794d42ade0fa','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.1.0.Final','sthorger@redhat.com','META-INF/jpa-changelog-1.1.0.Final.xml','2020-04-05 23:13:10',4,'EXECUTED','7:5d25857e708c3233ef4439df1f93f012','renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.2.0.Beta1','psilva@redhat.com','META-INF/jpa-changelog-1.2.0.Beta1.xml','2020-04-05 23:13:15',5,'EXECUTED','7:c7a54a1041d58eb3817a4a883b4d4e84','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.2.0.Beta1','psilva@redhat.com','META-INF/db2-jpa-changelog-1.2.0.Beta1.xml','2020-04-05 23:13:15',6,'MARK_RAN','7:2e01012df20974c1c2a605ef8afe25b7','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.2.0.RC1','bburke@redhat.com','META-INF/jpa-changelog-1.2.0.CR1.xml','2020-04-05 23:13:21',7,'EXECUTED','7:0f08df48468428e0f30ee59a8ec01a41','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.2.0.RC1','bburke@redhat.com','META-INF/db2-jpa-changelog-1.2.0.CR1.xml','2020-04-05 23:13:21',8,'MARK_RAN','7:a77ea2ad226b345e7d689d366f185c8c','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.2.0.Final','keycloak','META-INF/jpa-changelog-1.2.0.Final.xml','2020-04-05 23:13:21',9,'EXECUTED','7:a3377a2059aefbf3b90ebb4c4cc8e2ab','update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.3.0','bburke@redhat.com','META-INF/jpa-changelog-1.3.0.xml','2020-04-05 23:13:27',10,'EXECUTED','7:04c1dbedc2aa3e9756d1a1668e003451','delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.4.0','bburke@redhat.com','META-INF/jpa-changelog-1.4.0.xml','2020-04-05 23:13:30',11,'EXECUTED','7:36ef39ed560ad07062d956db861042ba','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.4.0','bburke@redhat.com','META-INF/db2-jpa-changelog-1.4.0.xml','2020-04-05 23:13:30',12,'MARK_RAN','7:d909180b2530479a716d3f9c9eaea3d7','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.5.0','bburke@redhat.com','META-INF/jpa-changelog-1.5.0.xml','2020-04-05 23:13:30',13,'EXECUTED','7:cf12b04b79bea5152f165eb41f3955f6','delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.6.1_from15','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2020-04-05 23:13:31',14,'EXECUTED','7:7e32c8f05c755e8675764e7d5f514509','addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.6.1_from16-pre','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2020-04-05 23:13:31',15,'MARK_RAN','7:980ba23cc0ec39cab731ce903dd01291','delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.6.1_from16','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2020-04-05 23:13:31',16,'MARK_RAN','7:2fa220758991285312eb84f3b4ff5336','dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.6.1','mposolda@redhat.com','META-INF/jpa-changelog-1.6.1.xml','2020-04-05 23:13:31',17,'EXECUTED','7:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.7.0','bburke@redhat.com','META-INF/jpa-changelog-1.7.0.xml','2020-04-05 23:13:35',18,'EXECUTED','7:91ace540896df890cc00a0490ee52bbc','createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.8.0','mposolda@redhat.com','META-INF/jpa-changelog-1.8.0.xml','2020-04-05 23:13:38',19,'EXECUTED','7:c31d1646dfa2618a9335c00e07f89f24','addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.8.0-2','keycloak','META-INF/jpa-changelog-1.8.0.xml','2020-04-05 23:13:38',20,'EXECUTED','7:df8bc21027a4f7cbbb01f6344e89ce07','dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.8.0','mposolda@redhat.com','META-INF/db2-jpa-changelog-1.8.0.xml','2020-04-05 23:13:38',21,'MARK_RAN','7:f987971fe6b37d963bc95fee2b27f8df','addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.8.0-2','keycloak','META-INF/db2-jpa-changelog-1.8.0.xml','2020-04-05 23:13:38',22,'MARK_RAN','7:df8bc21027a4f7cbbb01f6344e89ce07','dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.9.0','mposolda@redhat.com','META-INF/jpa-changelog-1.9.0.xml','2020-04-05 23:13:39',23,'EXECUTED','7:ed2dc7f799d19ac452cbcda56c929e47','update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.9.1','keycloak','META-INF/jpa-changelog-1.9.1.xml','2020-04-05 23:13:39',24,'EXECUTED','7:80b5db88a5dda36ece5f235be8757615','modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.9.1','keycloak','META-INF/db2-jpa-changelog-1.9.1.xml','2020-04-05 23:13:39',25,'MARK_RAN','7:1437310ed1305a9b93f8848f301726ce','modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('1.9.2','keycloak','META-INF/jpa-changelog-1.9.2.xml','2020-04-05 23:13:40',26,'EXECUTED','7:b82ffb34850fa0836be16deefc6a87c4','createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-2.0.0','psilva@redhat.com','META-INF/jpa-changelog-authz-2.0.0.xml','2020-04-05 23:13:45',27,'EXECUTED','7:9cc98082921330d8d9266decdd4bd658','createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-2.5.1','psilva@redhat.com','META-INF/jpa-changelog-authz-2.5.1.xml','2020-04-05 23:13:45',28,'EXECUTED','7:03d64aeed9cb52b969bd30a7ac0db57e','update tableName=RESOURCE_SERVER_POLICY','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.1.0-KEYCLOAK-5461','bburke@redhat.com','META-INF/jpa-changelog-2.1.0.xml','2020-04-05 23:13:49',29,'EXECUTED','7:f1f9fd8710399d725b780f463c6b21cd','createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.2.0','bburke@redhat.com','META-INF/jpa-changelog-2.2.0.xml','2020-04-05 23:13:49',30,'EXECUTED','7:53188c3eb1107546e6f765835705b6c1','addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.3.0','bburke@redhat.com','META-INF/jpa-changelog-2.3.0.xml','2020-04-05 23:13:51',31,'EXECUTED','7:d6e6f3bc57a0c5586737d1351725d4d4','createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.4.0','bburke@redhat.com','META-INF/jpa-changelog-2.4.0.xml','2020-04-05 23:13:51',32,'EXECUTED','7:454d604fbd755d9df3fd9c6329043aa5','customChange','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.0','bburke@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2020-04-05 23:13:51',33,'EXECUTED','7:57e98a3077e29caf562f7dbf80c72600','customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.0-unicode-oracle','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2020-04-05 23:13:51',34,'MARK_RAN','7:e4c7e8f2256210aee71ddc42f538b57a','modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.0-unicode-other-dbs','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2020-04-05 23:13:56',35,'EXECUTED','7:09a43c97e49bc626460480aa1379b522','modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.0-duplicate-email-support','slawomir@dabek.name','META-INF/jpa-changelog-2.5.0.xml','2020-04-05 23:13:56',36,'EXECUTED','7:26bfc7c74fefa9126f2ce702fb775553','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.0-unique-group-names','hmlnarik@redhat.com','META-INF/jpa-changelog-2.5.0.xml','2020-04-05 23:13:56',37,'EXECUTED','7:a161e2ae671a9020fff61e996a207377','addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'6128374759'),('2.5.1','bburke@redhat.com','META-INF/jpa-changelog-2.5.1.xml','2020-04-05 23:13:56',38,'EXECUTED','7:37fc1781855ac5388c494f1442b3f717','addColumn tableName=FED_USER_CONSENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.0.0','bburke@redhat.com','META-INF/jpa-changelog-3.0.0.xml','2020-04-05 23:13:57',39,'EXECUTED','7:13a27db0dae6049541136adad7261d27','addColumn tableName=IDENTITY_PROVIDER','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.2.0-fix','keycloak','META-INF/jpa-changelog-3.2.0.xml','2020-04-05 23:13:57',40,'MARK_RAN','7:550300617e3b59e8af3a6294df8248a3','addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.2.0-fix-with-keycloak-5416','keycloak','META-INF/jpa-changelog-3.2.0.xml','2020-04-05 23:13:57',41,'MARK_RAN','7:e3a9482b8931481dc2772a5c07c44f17','dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.2.0-fix-offline-sessions','hmlnarik','META-INF/jpa-changelog-3.2.0.xml','2020-04-05 23:13:57',42,'EXECUTED','7:72b07d85a2677cb257edb02b408f332d','customChange','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.2.0-fixed','keycloak','META-INF/jpa-changelog-3.2.0.xml','2020-04-05 23:14:02',43,'EXECUTED','7:a72a7858967bd414835d19e04d880312','addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.3.0','keycloak','META-INF/jpa-changelog-3.3.0.xml','2020-04-05 23:14:02',44,'EXECUTED','7:94edff7cf9ce179e7e85f0cd78a3cf2c','addColumn tableName=USER_ENTITY','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-3.4.0.CR1-resource-server-pk-change-part1','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2020-04-05 23:14:02',45,'EXECUTED','7:6a48ce645a3525488a90fbf76adf3bb3','addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095','hmlnarik@redhat.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2020-04-05 23:14:02',46,'EXECUTED','7:e64b5dcea7db06077c6e57d3b9e5ca14','customChange','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2020-04-05 23:14:02',47,'MARK_RAN','7:fd8cf02498f8b1e72496a20afc75178c','dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2020-04-05 23:14:06',48,'EXECUTED','7:542794f25aa2b1fbabb7e577d6646319','addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authn-3.4.0.CR1-refresh-token-max-reuse','glavoie@gmail.com','META-INF/jpa-changelog-authz-3.4.0.CR1.xml','2020-04-05 23:14:06',49,'EXECUTED','7:edad604c882df12f74941dac3cc6d650','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.4.0','keycloak','META-INF/jpa-changelog-3.4.0.xml','2020-04-05 23:14:10',50,'EXECUTED','7:0f88b78b7b46480eb92690cbf5e44900','addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.4.0-KEYCLOAK-5230','hmlnarik@redhat.com','META-INF/jpa-changelog-3.4.0.xml','2020-04-05 23:14:11',51,'EXECUTED','7:d560e43982611d936457c327f872dd59','createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.4.1','psilva@redhat.com','META-INF/jpa-changelog-3.4.1.xml','2020-04-05 23:14:11',52,'EXECUTED','7:c155566c42b4d14ef07059ec3b3bbd8e','modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.4.2','keycloak','META-INF/jpa-changelog-3.4.2.xml','2020-04-05 23:14:11',53,'EXECUTED','7:b40376581f12d70f3c89ba8ddf5b7dea','update tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('3.4.2-KEYCLOAK-5172','mkanis@redhat.com','META-INF/jpa-changelog-3.4.2.xml','2020-04-05 23:14:11',54,'EXECUTED','7:a1132cc395f7b95b3646146c2e38f168','update tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.0.0-KEYCLOAK-6335','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2020-04-05 23:14:11',55,'EXECUTED','7:d8dc5d89c789105cfa7ca0e82cba60af','createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.0.0-CLEANUP-UNUSED-TABLE','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2020-04-05 23:14:11',56,'EXECUTED','7:7822e0165097182e8f653c35517656a3','dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.0.0-KEYCLOAK-6228','bburke@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2020-04-05 23:14:12',57,'EXECUTED','7:c6538c29b9c9a08f9e9ea2de5c2b6375','dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.0.0-KEYCLOAK-5579-fixed','mposolda@redhat.com','META-INF/jpa-changelog-4.0.0.xml','2020-04-05 23:14:21',58,'EXECUTED','7:6d4893e36de22369cf73bcb051ded875','dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-4.0.0.CR1','psilva@redhat.com','META-INF/jpa-changelog-authz-4.0.0.CR1.xml','2020-04-05 23:14:23',59,'EXECUTED','7:57960fc0b0f0dd0563ea6f8b2e4a1707','createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-4.0.0.Beta3','psilva@redhat.com','META-INF/jpa-changelog-authz-4.0.0.Beta3.xml','2020-04-05 23:14:23',60,'EXECUTED','7:2b4b8bff39944c7097977cc18dbceb3b','addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-4.2.0.Final','mhajas@redhat.com','META-INF/jpa-changelog-authz-4.2.0.Final.xml','2020-04-05 23:14:24',61,'EXECUTED','7:2aa42a964c59cd5b8ca9822340ba33a8','createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-4.2.0.Final-KEYCLOAK-9944','hmlnarik@redhat.com','META-INF/jpa-changelog-authz-4.2.0.Final.xml','2020-04-05 23:14:24',62,'EXECUTED','7:9ac9e58545479929ba23f4a3087a0346','addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.2.0-KEYCLOAK-6313','wadahiro@gmail.com','META-INF/jpa-changelog-4.2.0.xml','2020-04-05 23:14:24',63,'EXECUTED','7:14d407c35bc4fe1976867756bcea0c36','addColumn tableName=REQUIRED_ACTION_PROVIDER','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.3.0-KEYCLOAK-7984','wadahiro@gmail.com','META-INF/jpa-changelog-4.3.0.xml','2020-04-05 23:14:24',64,'EXECUTED','7:241a8030c748c8548e346adee548fa93','update tableName=REQUIRED_ACTION_PROVIDER','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.6.0-KEYCLOAK-7950','psilva@redhat.com','META-INF/jpa-changelog-4.6.0.xml','2020-04-05 23:14:24',65,'EXECUTED','7:7d3182f65a34fcc61e8d23def037dc3f','update tableName=RESOURCE_SERVER_RESOURCE','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.6.0-KEYCLOAK-8377','keycloak','META-INF/jpa-changelog-4.6.0.xml','2020-04-05 23:14:24',66,'EXECUTED','7:b30039e00a0b9715d430d1b0636728fa','createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.6.0-KEYCLOAK-8555','gideonray@gmail.com','META-INF/jpa-changelog-4.6.0.xml','2020-04-05 23:14:24',67,'EXECUTED','7:3797315ca61d531780f8e6f82f258159','createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.7.0-KEYCLOAK-1267','sguilhen@redhat.com','META-INF/jpa-changelog-4.7.0.xml','2020-04-05 23:14:25',68,'EXECUTED','7:c7aa4c8d9573500c2d347c1941ff0301','addColumn tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.7.0-KEYCLOAK-7275','keycloak','META-INF/jpa-changelog-4.7.0.xml','2020-04-05 23:14:25',69,'EXECUTED','7:b207faee394fc074a442ecd42185a5dd','renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('4.8.0-KEYCLOAK-8835','sguilhen@redhat.com','META-INF/jpa-changelog-4.8.0.xml','2020-04-05 23:14:25',70,'EXECUTED','7:ab9a9762faaba4ddfa35514b212c4922','addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM','',NULL,'3.5.4',NULL,NULL,'6128374759'),('authz-7.0.0-KEYCLOAK-10443','psilva@redhat.com','META-INF/jpa-changelog-authz-7.0.0.xml','2020-04-05 23:14:26',71,'EXECUTED','7:b9710f74515a6ccb51b72dc0d19df8c4','addColumn tableName=RESOURCE_SERVER','',NULL,'3.5.4',NULL,NULL,'6128374759'),('8.0.0-adding-credential-columns','keycloak','META-INF/jpa-changelog-8.0.0.xml','2020-04-05 23:14:26',72,'EXECUTED','7:ec9707ae4d4f0b7452fee20128083879','addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('8.0.0-updating-credential-data-not-oracle','keycloak','META-INF/jpa-changelog-8.0.0.xml','2020-04-05 23:14:26',73,'EXECUTED','7:03b3f4b264c3c68ba082250a80b74216','update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('8.0.0-updating-credential-data-oracle','keycloak','META-INF/jpa-changelog-8.0.0.xml','2020-04-05 23:14:26',74,'MARK_RAN','7:64c5728f5ca1f5aa4392217701c4fe23','update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('8.0.0-credential-cleanup-fixed','keycloak','META-INF/jpa-changelog-8.0.0.xml','2020-04-05 23:14:29',75,'EXECUTED','7:b48da8c11a3d83ddd6b7d0c8c2219345','dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('8.0.0-resource-tag-support','keycloak','META-INF/jpa-changelog-8.0.0.xml','2020-04-05 23:14:29',76,'EXECUTED','7:a73379915c23bfad3e8f5c6d5c0aa4bd','addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.0-always-display-client','keycloak','META-INF/jpa-changelog-9.0.0.xml','2020-04-05 23:14:29',77,'EXECUTED','7:39e0073779aba192646291aa2332493d','addColumn tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.0-drop-constraints-for-column-increase','keycloak','META-INF/jpa-changelog-9.0.0.xml','2020-04-05 23:14:29',78,'MARK_RAN','7:81f87368f00450799b4bf42ea0b3ec34','dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.0-increase-column-size-federated-fk','keycloak','META-INF/jpa-changelog-9.0.0.xml','2020-04-05 23:14:32',79,'EXECUTED','7:20b37422abb9fb6571c618148f013a15','modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.0-recreate-constraints-after-column-increase','keycloak','META-INF/jpa-changelog-9.0.0.xml','2020-04-05 23:14:32',80,'MARK_RAN','7:1970bb6cfb5ee800736b95ad3fb3c78a','addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.1-add-index-to-client.client_id','keycloak','META-INF/jpa-changelog-9.0.1.xml','2020-04-05 23:14:32',81,'EXECUTED','7:45d9b25fc3b455d522d8dcc10a0f4c80','createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.1-KEYCLOAK-12579-drop-constraints','keycloak','META-INF/jpa-changelog-9.0.1.xml','2020-04-05 23:14:32',82,'MARK_RAN','7:890ae73712bc187a66c2813a724d037f','dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.1-KEYCLOAK-12579-add-not-null-constraint','keycloak','META-INF/jpa-changelog-9.0.1.xml','2020-04-05 23:14:32',83,'EXECUTED','7:0a211980d27fafe3ff50d19a3a29b538','addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.1-KEYCLOAK-12579-recreate-constraints','keycloak','META-INF/jpa-changelog-9.0.1.xml','2020-04-05 23:14:32',84,'MARK_RAN','7:a161e2ae671a9020fff61e996a207377','addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP','',NULL,'3.5.4',NULL,NULL,'6128374759'),('9.0.1-add-index-to-events','keycloak','META-INF/jpa-changelog-9.0.1.xml','2020-04-05 23:14:32',85,'EXECUTED','7:01c49302201bdf815b0a18d1f98a55dc','createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY','',NULL,'3.5.4',NULL,NULL,'6128374759');
/*!40000 ALTER TABLE `DATABASECHANGELOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATABASECHANGELOGLOCK`
--

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOGLOCK`
--

LOCK TABLES `DATABASECHANGELOGLOCK` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOGLOCK` VALUES (1,_binary '\0',NULL,NULL),(1000,_binary '\0',NULL,NULL),(1001,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `DATABASECHANGELOGLOCK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DEFAULT_CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `DEFAULT_CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DEFAULT_CLIENT_SCOPE` (
  `REALM_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  `DEFAULT_SCOPE` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`REALM_ID`,`SCOPE_ID`),
  KEY `IDX_DEFCLS_REALM` (`REALM_ID`),
  KEY `IDX_DEFCLS_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_R_DEF_CLI_SCOPE_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`),
  CONSTRAINT `FK_R_DEF_CLI_SCOPE_SCOPE` FOREIGN KEY (`SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DEFAULT_CLIENT_SCOPE`
--

LOCK TABLES `DEFAULT_CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `DEFAULT_CLIENT_SCOPE` DISABLE KEYS */;
INSERT INTO `DEFAULT_CLIENT_SCOPE` VALUES ('master','0a2ff77e-0d2e-43ea-b777-5b32344e688e',_binary '\0'),('master','26df8b91-6c5e-436e-8f71-834ae2cffc80',_binary ''),('master','3adad139-0c20-4b06-9440-f00fca6e97b4',_binary ''),('master','64820772-1d17-457f-a114-8a6ffa9a3a3d',_binary ''),('master','76a0fe56-946c-448d-aa3f-f9c44b405229',_binary '\0'),('master','7d130904-f3a2-432b-b54a-7eb1d8048611',_binary ''),('master','aec8c585-83ed-4419-a433-9532b35b3dc7',_binary '\0'),('master','e43593c4-d629-41bd-a69e-a8f6bb8a9a8c',_binary '\0'),('master','f4029215-37ac-461b-bd03-e03f4018bd5a',_binary ''),('Sc-project','0e72584e-7e30-484f-91c7-f7166f7770a8',_binary ''),('Sc-project','1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5',_binary ''),('Sc-project','3fda5dcc-0d7a-493b-9450-e177852a507d',_binary ''),('Sc-project','66999a59-b339-4571-95dd-d845da05f339',_binary '\0'),('Sc-project','75cf7c60-5290-4cbc-9bd7-82043f072751',_binary ''),('Sc-project','78f4d7da-e8f1-43fb-8e6b-e43a14b16628',_binary '\0'),('Sc-project','a8db9c91-8759-4cf8-83dd-683470d8f0cb',_binary '\0'),('Sc-project','cbabf283-6a06-4914-8f29-44edc0b3e469',_binary ''),('Sc-project','df8bc044-2e83-45ee-a483-97db61775541',_binary '\0');
/*!40000 ALTER TABLE `DEFAULT_CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENT_ENTITY`
--

DROP TABLE IF EXISTS `EVENT_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `EVENT_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `DETAILS_JSON` text,
  `ERROR` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `SESSION_ID` varchar(255) DEFAULT NULL,
  `EVENT_TIME` bigint DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_EVENT_TIME` (`REALM_ID`,`EVENT_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENT_ENTITY`
--

LOCK TABLES `EVENT_ENTITY` WRITE;
/*!40000 ALTER TABLE `EVENT_ENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `EVENT_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FEDERATED_IDENTITY`
--

DROP TABLE IF EXISTS `FEDERATED_IDENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FEDERATED_IDENTITY` (
  `IDENTITY_PROVIDER` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `FEDERATED_USER_ID` varchar(255) DEFAULT NULL,
  `FEDERATED_USERNAME` varchar(255) DEFAULT NULL,
  `TOKEN` text,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER`,`USER_ID`),
  KEY `IDX_FEDIDENTITY_USER` (`USER_ID`),
  KEY `IDX_FEDIDENTITY_FEDUSER` (`FEDERATED_USER_ID`),
  CONSTRAINT `FK404288B92EF007A6` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FEDERATED_IDENTITY`
--

LOCK TABLES `FEDERATED_IDENTITY` WRITE;
/*!40000 ALTER TABLE `FEDERATED_IDENTITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `FEDERATED_IDENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FEDERATED_USER`
--

DROP TABLE IF EXISTS `FEDERATED_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FEDERATED_USER` (
  `ID` varchar(255) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FEDERATED_USER`
--

LOCK TABLES `FEDERATED_USER` WRITE;
/*!40000 ALTER TABLE `FEDERATED_USER` DISABLE KEYS */;
/*!40000 ALTER TABLE `FEDERATED_USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_ATTRIBUTE`
--

DROP TABLE IF EXISTS `FED_USER_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `VALUE` text,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_ATTRIBUTE` (`USER_ID`,`REALM_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_ATTRIBUTE`
--

LOCK TABLES `FED_USER_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `FED_USER_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CONSENT`
--

DROP TABLE IF EXISTS `FED_USER_CONSENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CONSENT` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CONSENT` (`USER_ID`,`CLIENT_ID`),
  KEY `IDX_FU_CONSENT_RU` (`REALM_ID`,`USER_ID`),
  KEY `IDX_FU_CNSNT_EXT` (`USER_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CONSENT`
--

LOCK TABLES `FED_USER_CONSENT` WRITE;
/*!40000 ALTER TABLE `FED_USER_CONSENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CONSENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CONSENT_CL_SCOPE`
--

DROP TABLE IF EXISTS `FED_USER_CONSENT_CL_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CONSENT_CL_SCOPE` (
  `USER_CONSENT_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CONSENT_CL_SCOPE`
--

LOCK TABLES `FED_USER_CONSENT_CL_SCOPE` WRITE;
/*!40000 ALTER TABLE `FED_USER_CONSENT_CL_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CONSENT_CL_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_CREDENTIAL`
--

DROP TABLE IF EXISTS `FED_USER_CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_CREDENTIAL` (
  `ID` varchar(36) NOT NULL,
  `SALT` tinyblob,
  `TYPE` varchar(255) DEFAULT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  `USER_LABEL` varchar(255) DEFAULT NULL,
  `SECRET_DATA` longtext,
  `CREDENTIAL_DATA` longtext,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_FU_CREDENTIAL` (`USER_ID`,`TYPE`),
  KEY `IDX_FU_CREDENTIAL_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_CREDENTIAL`
--

LOCK TABLES `FED_USER_CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `FED_USER_CREDENTIAL` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_GROUP_MEMBERSHIP`
--

DROP TABLE IF EXISTS `FED_USER_GROUP_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_GROUP_MEMBERSHIP` (
  `GROUP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP` (`USER_ID`,`GROUP_ID`),
  KEY `IDX_FU_GROUP_MEMBERSHIP_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_GROUP_MEMBERSHIP`
--

LOCK TABLES `FED_USER_GROUP_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `FED_USER_GROUP_MEMBERSHIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_GROUP_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_REQUIRED_ACTION`
--

DROP TABLE IF EXISTS `FED_USER_REQUIRED_ACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_REQUIRED_ACTION` (
  `REQUIRED_ACTION` varchar(255) NOT NULL DEFAULT ' ',
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_FU_REQUIRED_ACTION` (`USER_ID`,`REQUIRED_ACTION`),
  KEY `IDX_FU_REQUIRED_ACTION_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_REQUIRED_ACTION`
--

LOCK TABLES `FED_USER_REQUIRED_ACTION` WRITE;
/*!40000 ALTER TABLE `FED_USER_REQUIRED_ACTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_REQUIRED_ACTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FED_USER_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `FED_USER_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FED_USER_ROLE_MAPPING` (
  `ROLE_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `STORAGE_PROVIDER_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_FU_ROLE_MAPPING` (`USER_ID`,`ROLE_ID`),
  KEY `IDX_FU_ROLE_MAPPING_RU` (`REALM_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FED_USER_ROLE_MAPPING`
--

LOCK TABLES `FED_USER_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `FED_USER_ROLE_MAPPING` DISABLE KEYS */;
/*!40000 ALTER TABLE `FED_USER_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GROUP_ATTRIBUTE`
--

DROP TABLE IF EXISTS `GROUP_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GROUP_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_GROUP_ATTR_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_GROUP_ATTRIBUTE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `KEYCLOAK_GROUP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GROUP_ATTRIBUTE`
--

LOCK TABLES `GROUP_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `GROUP_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUP_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GROUP_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `GROUP_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GROUP_ROLE_MAPPING` (
  `ROLE_ID` varchar(36) NOT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`GROUP_ID`),
  KEY `IDX_GROUP_ROLE_MAPP_GROUP` (`GROUP_ID`),
  CONSTRAINT `FK_GROUP_ROLE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `KEYCLOAK_GROUP` (`ID`),
  CONSTRAINT `FK_GROUP_ROLE_ROLE` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GROUP_ROLE_MAPPING`
--

LOCK TABLES `GROUP_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `GROUP_ROLE_MAPPING` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUP_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER` (
  `INTERNAL_ID` varchar(36) NOT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ALIAS` varchar(255) DEFAULT NULL,
  `PROVIDER_ID` varchar(255) DEFAULT NULL,
  `STORE_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `AUTHENTICATE_BY_DEFAULT` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) DEFAULT NULL,
  `ADD_TOKEN_ROLE` bit(1) NOT NULL DEFAULT b'1',
  `TRUST_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `FIRST_BROKER_LOGIN_FLOW_ID` varchar(36) DEFAULT NULL,
  `POST_BROKER_LOGIN_FLOW_ID` varchar(36) DEFAULT NULL,
  `PROVIDER_DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `LINK_ONLY` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`INTERNAL_ID`),
  UNIQUE KEY `UK_2DAELWNIBJI49AVXSRTUF6XJ33` (`PROVIDER_ALIAS`,`REALM_ID`),
  KEY `IDX_IDENT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK2B4EBC52AE5C3B34` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER`
--

LOCK TABLES `IDENTITY_PROVIDER` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER_CONFIG`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER_CONFIG` (
  `IDENTITY_PROVIDER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`IDENTITY_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FKDC4897CF864C4E43` FOREIGN KEY (`IDENTITY_PROVIDER_ID`) REFERENCES `IDENTITY_PROVIDER` (`INTERNAL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER_CONFIG`
--

LOCK TABLES `IDENTITY_PROVIDER_CONFIG` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDENTITY_PROVIDER_MAPPER`
--

DROP TABLE IF EXISTS `IDENTITY_PROVIDER_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDENTITY_PROVIDER_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `IDP_ALIAS` varchar(255) NOT NULL,
  `IDP_MAPPER_NAME` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ID_PROV_MAPP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_IDPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDENTITY_PROVIDER_MAPPER`
--

LOCK TABLES `IDENTITY_PROVIDER_MAPPER` WRITE;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDENTITY_PROVIDER_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IDP_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `IDP_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `IDP_MAPPER_CONFIG` (
  `IDP_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`IDP_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_IDPMCONFIG` FOREIGN KEY (`IDP_MAPPER_ID`) REFERENCES `IDENTITY_PROVIDER_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IDP_MAPPER_CONFIG`
--

LOCK TABLES `IDP_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `IDP_MAPPER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `IDP_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KEYCLOAK_GROUP`
--

DROP TABLE IF EXISTS `KEYCLOAK_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `KEYCLOAK_GROUP` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `PARENT_GROUP` varchar(36) NOT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SIBLING_NAMES` (`REALM_ID`,`PARENT_GROUP`,`NAME`),
  CONSTRAINT `FK_GROUP_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KEYCLOAK_GROUP`
--

LOCK TABLES `KEYCLOAK_GROUP` WRITE;
/*!40000 ALTER TABLE `KEYCLOAK_GROUP` DISABLE KEYS */;
/*!40000 ALTER TABLE `KEYCLOAK_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KEYCLOAK_ROLE`
--

DROP TABLE IF EXISTS `KEYCLOAK_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `KEYCLOAK_ROLE` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_REALM_CONSTRAINT` varchar(255) DEFAULT NULL,
  `CLIENT_ROLE` bit(1) DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `CLIENT` varchar(36) DEFAULT NULL,
  `REALM` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_J3RWUVD56ONTGSUHOGM184WW2-2` (`NAME`,`CLIENT_REALM_CONSTRAINT`),
  KEY `IDX_KEYCLOAK_ROLE_CLIENT` (`CLIENT`),
  KEY `IDX_KEYCLOAK_ROLE_REALM` (`REALM`),
  CONSTRAINT `FK_6VYQFE4CN4WLQ8R6KT5VDSJ5C` FOREIGN KEY (`REALM`) REFERENCES `REALM` (`ID`),
  CONSTRAINT `FK_KJHO5LE2C0RAL09FL8CM9WFW9` FOREIGN KEY (`CLIENT`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KEYCLOAK_ROLE`
--

LOCK TABLES `KEYCLOAK_ROLE` WRITE;
/*!40000 ALTER TABLE `KEYCLOAK_ROLE` DISABLE KEYS */;
INSERT INTO `KEYCLOAK_ROLE` VALUES ('022161f1-25a7-4b58-96ae-2b55c66b7291','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_query-clients}','query-clients','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('031730c1-5ec1-4cde-845c-2d9001f1858d','Sc-project',_binary '\0','${role_offline-access}','offline_access','Sc-project',NULL,'Sc-project'),('08121c92-f09e-4668-a2d7-fdcc00abff8d','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-identity-providers}','view-identity-providers','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('0b7a6b42-19fd-49b0-a3bd-b8e300f0b209','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_view-profile}','view-profile','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('0c0e50b8-eddf-4179-89e9-6ac9ce02f1ce','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_query-groups}','query-groups','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('0fa46056-1cf7-4c21-b1e3-80ebd31300b9','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_create-client}','create-client','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('0fe4fdaa-b05a-4a51-a4b8-c8956da45e47','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-realm}','manage-realm','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('125b3134-4b2f-4ae2-a379-3e8b918d675f','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_query-clients}','query-clients','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('137da2db-c968-4561-8a90-f9b2107efd62','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_view-consent}','view-consent','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('16c7f069-2527-46e1-9d34-deed6672675b','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_impersonation}','impersonation','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('1aadc70c-d72e-4d14-88ba-187d61010a08','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-clients}','manage-clients','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('1c88b02d-4ecd-404f-9e71-d465921d2242','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_view-consent}','view-consent','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('20c62e21-0357-4254-990d-fd6dba5c33e9','master',_binary '\0','${role_offline-access}','offline_access','master',NULL,'master'),('226e8be6-2a0e-43af-a6e4-20f3cedf2d1d','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_view-applications}','view-applications','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('265728e9-d9de-4edf-83e5-560c14ace534','Sc-project',_binary '\0','${role_uma_authorization}','uma_authorization','Sc-project',NULL,'Sc-project'),('29256e83-7313-4cc7-85d8-b02a5263b3a6','Sc-project',_binary '\0',NULL,'user','Sc-project',NULL,'Sc-project'),('2930bab7-ff20-4032-bfc2-f8377ccec89b','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-users}','view-users','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('33c03778-c73f-4f37-a7e8-fd755d33faaf','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-realm}','view-realm','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('3790a5b4-efcc-457f-b134-fbd53ea93663','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_manage-account-links}','manage-account-links','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('3a2b0ebb-462a-45a0-8a95-b48f58148162','master',_binary '\0','${role_uma_authorization}','uma_authorization','master',NULL,'master'),('3cdd1a30-c2e6-427b-8152-9302dfd799c0','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_manage-account-links}','manage-account-links','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('3ee750fd-6512-4566-8a45-81a2059198fa','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_query-clients}','query-clients','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('3f99e8f6-a03d-4e97-882d-d23c6f6bb2be','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_query-users}','query-users','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('4446a8c0-4592-4d27-bcad-2aac6952a844','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-authorization}','manage-authorization','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('477fe2dc-3cde-46bf-9051-4a74a25c07cc','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-realm}','view-realm','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('47ae124c-f13e-4d78-b33f-e3664fe40f52','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-authorization}','view-authorization','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('49e96f8d-cabd-4601-9f9b-1d6b96b74257','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-clients}','view-clients','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('4a472544-8031-4fb9-8bc3-eb671936e041','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_query-realms}','query-realms','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('4d30d933-8c1c-44d3-82b3-74c38dc42ae3','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-users}','view-users','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('4ef4d7c7-5d1a-4cc2-92e4-f3dc02dfb450','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-clients}','manage-clients','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('506702ed-8475-423a-93fa-c9bd81d679a1','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-identity-providers}','manage-identity-providers','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('51b7530b-928b-45e3-9770-f5a632303eb4','Sc-project',_binary '\0',NULL,'manager','Sc-project',NULL,'Sc-project'),('5403fe89-090c-4ca5-886a-f102107c93d0','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_query-users}','query-users','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('5495ab85-9f42-4594-a85e-94f41836b828','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_query-groups}','query-groups','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('582b5625-0473-4deb-a759-1dafa77c5bc3','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-events}','manage-events','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('59ab301e-4e7d-4cc5-87d8-5ba8ef330e64','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_query-users}','query-users','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('5c7f9d01-d972-4e67-ac72-28570d658114','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-realm}','manage-realm','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('60b4ea8a-8eeb-412a-b221-42757bd2a9aa','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_create-client}','create-client','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('61c2b816-fbf8-40bf-a61c-98be8746aa27','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-events}','view-events','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('6496f525-f14b-4af1-9898-bc531cafb742','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-identity-providers}','view-identity-providers','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('65dc6ddc-245a-40d8-91ec-0dc6950eb5d6','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-events}','manage-events','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('68b9623a-7e9b-4871-8b06-d6792bb7cd75','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-authorization}','manage-authorization','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('6f2620ed-05ce-484f-ae6e-4fb3250808f9','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-users}','manage-users','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('704996b1-eb75-4c54-84c2-035e0fed2c5a','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_manage-account}','manage-account','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('8273524c-e028-4938-ba84-7a9f72850743','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-users}','view-users','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('8346b9a7-602a-4108-b4e5-ef9b98e435d2','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-events}','view-events','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('881b8bec-f570-44e6-8b71-f3b5b3b67041','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_realm-admin}','realm-admin','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('89ba3d52-e8f7-49e6-ba67-512897ba91ea','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-clients}','view-clients','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('8cfc61b8-7254-4fe3-bcd4-2063066740ff','7884858a-7fa3-48ac-bfbd-01d20dbaf736',_binary '','${role_read-token}','read-token','Sc-project','7884858a-7fa3-48ac-bfbd-01d20dbaf736',NULL),('98560981-8901-4702-b117-b0cabee4aa76','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_view-applications}','view-applications','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('9d9a491f-f0a6-4b87-b5bf-a964a8f9ba4a','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-clients}','view-clients','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('a3620b79-c24b-4aed-86ed-016ce53e1190','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_manage-account}','manage-account','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('a90f1cd1-926b-4e9f-8400-504e15f00e66','9de038f3-8dcd-4169-8514-c3ae3561ea90',_binary '','${role_manage-consent}','manage-consent','Sc-project','9de038f3-8dcd-4169-8514-c3ae3561ea90',NULL),('aa75e4c0-43b6-4f3a-bd76-148e964f0190','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_manage-consent}','manage-consent','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('b29c9e22-ceb6-4142-83f4-c3904ca23cf2','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-users}','manage-users','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('b462d407-a386-4006-aec7-5b5c37223690','master',_binary '\0','${role_admin}','admin','master',NULL,'master'),('b8bd3ef2-3f5a-4681-9c32-186d453f2255','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_impersonation}','impersonation','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('c301ee88-d5de-4a16-a50e-d8ed4c20f5dd','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_query-realms}','query-realms','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('c89c13b8-3119-49b5-974a-f2818d5f7ea4','master',_binary '\0','${role_create-realm}','create-realm','master',NULL,'master'),('ca7d602a-949c-4774-acc7-0fa43a44205b','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-users}','manage-users','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('cb93f475-d3d7-4919-9764-becaa77100d2','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-realm}','manage-realm','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('cc5f4868-d5d7-4759-a88e-5d8e5e72fe7f','b87832b0-7d60-4836-ba48-d340ef312563',_binary '','${role_read-token}','read-token','master','b87832b0-7d60-4836-ba48-d340ef312563',NULL),('d04b794d-4e7d-4e72-9cf2-a29a003da973','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-realm}','view-realm','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('d3e043f1-24bd-4ea8-a839-91127690b166','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_impersonation}','impersonation','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('d555e901-3a51-4aba-9456-cc1cb07fa59d','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-authorization}','view-authorization','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('dbeb4187-7b0e-4a3e-b0c9-1d8d199c5a13','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_view-authorization}','view-authorization','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('e0df025a-d993-421e-a76c-a88dbf63388b','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_query-groups}','query-groups','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('e27295e8-029e-48f7-a5a3-e0938725eb0a','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-identity-providers}','manage-identity-providers','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('e5e27e00-90d6-464b-9560-d8af8b88272c','207e7c98-d443-4ef9-8a8d-5bf4438b2656',_binary '','${role_view-profile}','view-profile','master','207e7c98-d443-4ef9-8a8d-5bf4438b2656',NULL),('e68bc928-06b7-4ac8-8431-42b276afcd14','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_manage-identity-providers}','manage-identity-providers','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('ec76a6fa-154a-4413-8a6f-7ae5cc1f8b4e','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_manage-authorization}','manage-authorization','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('ece0e553-fa10-405e-9b46-e6bea1ff2858','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_create-client}','create-client','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('f35938bc-b4e9-4684-ad60-d5bd911a86d0','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_query-realms}','query-realms','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('f762e32a-d8de-4f92-a21b-0003b32ff421','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-clients}','manage-clients','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL),('f8e23751-2b1c-454d-a3b4-8af5104e9022','3510d930-aa13-46d9-bfd7-730a324d04da',_binary '','${role_view-events}','view-events','master','3510d930-aa13-46d9-bfd7-730a324d04da',NULL),('fa01cf60-a67d-4979-af23-d24d7ce9461b','522586e3-ecfe-44af-8053-8859d3870549',_binary '','${role_view-identity-providers}','view-identity-providers','master','522586e3-ecfe-44af-8053-8859d3870549',NULL),('fd695420-353a-4c77-8dde-f623a80aa390','3659c4fb-dab0-460a-a07b-77bea31bb4c1',_binary '','${role_manage-events}','manage-events','Sc-project','3659c4fb-dab0-460a-a07b-77bea31bb4c1',NULL);
/*!40000 ALTER TABLE `KEYCLOAK_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MIGRATION_MODEL`
--

DROP TABLE IF EXISTS `MIGRATION_MODEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MIGRATION_MODEL` (
  `ID` varchar(36) NOT NULL,
  `VERSION` varchar(36) DEFAULT NULL,
  `UPDATE_TIME` bigint NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IDX_UPDATE_TIME` (`UPDATE_TIME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MIGRATION_MODEL`
--

LOCK TABLES `MIGRATION_MODEL` WRITE;
/*!40000 ALTER TABLE `MIGRATION_MODEL` DISABLE KEYS */;
INSERT INTO `MIGRATION_MODEL` VALUES ('6gc9y','9.0.2',1586128476);
/*!40000 ALTER TABLE `MIGRATION_MODEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OFFLINE_CLIENT_SESSION`
--

DROP TABLE IF EXISTS `OFFLINE_CLIENT_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OFFLINE_CLIENT_SESSION` (
  `USER_SESSION_ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) NOT NULL,
  `OFFLINE_FLAG` varchar(4) NOT NULL,
  `TIMESTAMP` int DEFAULT NULL,
  `DATA` longtext,
  `CLIENT_STORAGE_PROVIDER` varchar(36) NOT NULL DEFAULT 'local',
  `EXTERNAL_CLIENT_ID` varchar(255) NOT NULL DEFAULT 'local',
  PRIMARY KEY (`USER_SESSION_ID`,`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`OFFLINE_FLAG`),
  KEY `IDX_US_SESS_ID_ON_CL_SESS` (`USER_SESSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OFFLINE_CLIENT_SESSION`
--

LOCK TABLES `OFFLINE_CLIENT_SESSION` WRITE;
/*!40000 ALTER TABLE `OFFLINE_CLIENT_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `OFFLINE_CLIENT_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OFFLINE_USER_SESSION`
--

DROP TABLE IF EXISTS `OFFLINE_USER_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `OFFLINE_USER_SESSION` (
  `USER_SESSION_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  `CREATED_ON` int NOT NULL,
  `OFFLINE_FLAG` varchar(4) NOT NULL,
  `DATA` longtext,
  `LAST_SESSION_REFRESH` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`USER_SESSION_ID`,`OFFLINE_FLAG`),
  KEY `IDX_OFFLINE_USS_CREATEDON` (`CREATED_ON`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OFFLINE_USER_SESSION`
--

LOCK TABLES `OFFLINE_USER_SESSION` WRITE;
/*!40000 ALTER TABLE `OFFLINE_USER_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `OFFLINE_USER_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `POLICY_CONFIG`
--

DROP TABLE IF EXISTS `POLICY_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `POLICY_CONFIG` (
  `POLICY_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` longtext,
  PRIMARY KEY (`POLICY_ID`,`NAME`),
  CONSTRAINT `FKDC34197CF864C4E43` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `POLICY_CONFIG`
--

LOCK TABLES `POLICY_CONFIG` WRITE;
/*!40000 ALTER TABLE `POLICY_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `POLICY_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROTOCOL_MAPPER`
--

DROP TABLE IF EXISTS `PROTOCOL_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PROTOCOL_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `PROTOCOL` varchar(255) NOT NULL,
  `PROTOCOL_MAPPER_NAME` varchar(255) NOT NULL,
  `CLIENT_ID` varchar(36) DEFAULT NULL,
  `CLIENT_SCOPE_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_PROTOCOL_MAPPER_CLIENT` (`CLIENT_ID`),
  KEY `IDX_CLSCOPE_PROTMAP` (`CLIENT_SCOPE_ID`),
  CONSTRAINT `FK_CLI_SCOPE_MAPPER` FOREIGN KEY (`CLIENT_SCOPE_ID`) REFERENCES `CLIENT_SCOPE` (`ID`),
  CONSTRAINT `FK_PCM_REALM` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROTOCOL_MAPPER`
--

LOCK TABLES `PROTOCOL_MAPPER` WRITE;
/*!40000 ALTER TABLE `PROTOCOL_MAPPER` DISABLE KEYS */;
INSERT INTO `PROTOCOL_MAPPER` VALUES ('03e1cd62-1b45-4fc0-962d-572be9350336','address','openid-connect','oidc-address-mapper',NULL,'66999a59-b339-4571-95dd-d845da05f339'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','zoneinfo','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','phone number verified','openid-connect','oidc-usermodel-attribute-mapper',NULL,'78f4d7da-e8f1-43fb-8e6b-e43a14b16628'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','birthdate','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('21744f05-23ca-46c8-88b9-15c4ebddd839','updated at','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','updated at','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('2dd3a990-8c6e-4098-a847-41592bc84096','family name','openid-connect','oidc-usermodel-property-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('3b9e387f-108f-4dc3-960d-efc6987157a5','groups','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'a8db9c91-8759-4cf8-83dd-683470d8f0cb'),('3f99f922-94a9-49fd-a362-4b3820127403','given name','openid-connect','oidc-usermodel-property-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('476e806c-da5c-4286-8ccb-a654491c57a7','client roles','openid-connect','oidc-usermodel-client-role-mapper',NULL,'7d130904-f3a2-432b-b54a-7eb1d8048611'),('49a3dfb0-cfff-47eb-8293-23e2d9722203','audience resolve','openid-connect','oidc-audience-resolve-mapper',NULL,'7d130904-f3a2-432b-b54a-7eb1d8048611'),('4b6957d8-c349-43ac-a163-41613f407475','locale','openid-connect','oidc-usermodel-attribute-mapper','6d408964-4ba2-4ce2-8c9e-d328bdb51c0e',NULL),('51019db7-295c-48d1-b77e-68a836252f4a','full name','openid-connect','oidc-full-name-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('52870437-98f4-4be2-9901-dfb360da9ad6','phone number verified','openid-connect','oidc-usermodel-attribute-mapper',NULL,'e43593c4-d629-41bd-a69e-a8f6bb8a9a8c'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','realm roles','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'cbabf283-6a06-4914-8f29-44edc0b3e469'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','locale','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('61d24ccb-6fde-4128-bd88-555ed34542a6','groups','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'aec8c585-83ed-4419-a433-9532b35b3dc7'),('632ddc16-fa19-4d6f-a02e-bc5e28f9c869','audience resolve','openid-connect','oidc-audience-resolve-mapper','c16a2068-7765-47d5-b17e-c9079f96cbbd',NULL),('68deeee3-6445-495c-bf1e-7e2aed6ba112','picture','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','family name','openid-connect','oidc-usermodel-property-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('6e02c16f-046a-4a6c-af57-dafcb4057135','zoneinfo','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('6f254500-4113-45d8-90f1-b93527d946e6','phone number','openid-connect','oidc-usermodel-attribute-mapper',NULL,'78f4d7da-e8f1-43fb-8e6b-e43a14b16628'),('770bb0e9-cb6a-490c-9182-9264fdad2776','website','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('7774afdd-cfce-4f10-ba0e-70103b1cdba9','full name','openid-connect','oidc-full-name-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','phone number','openid-connect','oidc-usermodel-attribute-mapper',NULL,'e43593c4-d629-41bd-a69e-a8f6bb8a9a8c'),('8a24cd18-82f7-4328-9c54-cdb84680b088','realm roles','openid-connect','oidc-usermodel-realm-role-mapper',NULL,'7d130904-f3a2-432b-b54a-7eb1d8048611'),('8d76a385-d210-4674-97e0-6b4bc0543339','email verified','openid-connect','oidc-usermodel-property-mapper',NULL,'64820772-1d17-457f-a114-8a6ffa9a3a3d'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','birthdate','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','username','openid-connect','oidc-usermodel-property-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','address','openid-connect','oidc-address-mapper',NULL,'0a2ff77e-0d2e-43ea-b777-5b32344e688e'),('ad7e0577-e842-46f6-ba37-063c7326abf4','gender','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('b1484c99-c19e-452d-9528-17c5b9342f9a','username','openid-connect','oidc-usermodel-property-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','picture','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','nickname','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('bcddf94a-9cac-4897-b659-bb93417bb030','given name','openid-connect','oidc-usermodel-property-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('bce0af2b-76c3-4f26-9983-00fac40306cf','middle name','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('be367858-7d9c-40c7-a145-25b20ddefd2f','role list','saml','saml-role-list-mapper',NULL,'75cf7c60-5290-4cbc-9bd7-82043f072751'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','gender','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','profile','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('c5ca3565-3b66-4e47-8b2f-862174513ead','email verified','openid-connect','oidc-usermodel-property-mapper',NULL,'0e72584e-7e30-484f-91c7-f7166f7770a8'),('c6ad7019-ccaf-4278-8fc0-ae6ec8ae1a17','audience resolve','openid-connect','oidc-audience-resolve-mapper','cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5',NULL),('ca5aaee7-8d7a-4c55-a9d2-533838918e68','role list','saml','saml-role-list-mapper',NULL,'f4029215-37ac-461b-bd03-e03f4018bd5a'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','locale','openid-connect','oidc-usermodel-attribute-mapper','e8c9f90c-1683-4037-8bb4-79c52969e5b9',NULL),('cc08a3b3-6a3d-4932-b9c6-1617899da67c','allowed web origins','openid-connect','oidc-allowed-origins-mapper',NULL,'26df8b91-6c5e-436e-8f71-834ae2cffc80'),('d3986df1-b765-4c2e-9808-06353556936a','client roles','openid-connect','oidc-usermodel-client-role-mapper',NULL,'cbabf283-6a06-4914-8f29-44edc0b3e469'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','upn','openid-connect','oidc-usermodel-property-mapper',NULL,'aec8c585-83ed-4419-a433-9532b35b3dc7'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','website','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','nickname','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','profile','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('e4652a24-5160-4413-bca3-13d108095e35','middle name','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3fda5dcc-0d7a-493b-9450-e177852a507d'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','email','openid-connect','oidc-usermodel-property-mapper',NULL,'64820772-1d17-457f-a114-8a6ffa9a3a3d'),('f22ad32d-dcb5-4a95-a484-c6c5b106af0c','audience resolve','openid-connect','oidc-audience-resolve-mapper',NULL,'cbabf283-6a06-4914-8f29-44edc0b3e469'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','email','openid-connect','oidc-usermodel-property-mapper',NULL,'0e72584e-7e30-484f-91c7-f7166f7770a8'),('f55ffa3e-e174-4bb5-bd23-c70db4581ef3','allowed web origins','openid-connect','oidc-allowed-origins-mapper',NULL,'1bc1045d-0d0b-4068-a3d1-6d9bb0a1d5c5'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','locale','openid-connect','oidc-usermodel-attribute-mapper',NULL,'3adad139-0c20-4b06-9440-f00fca6e97b4'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','upn','openid-connect','oidc-usermodel-property-mapper',NULL,'a8db9c91-8759-4cf8-83dd-683470d8f0cb');
/*!40000 ALTER TABLE `PROTOCOL_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROTOCOL_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `PROTOCOL_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PROTOCOL_MAPPER_CONFIG` (
  `PROTOCOL_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`PROTOCOL_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_PMCONFIG` FOREIGN KEY (`PROTOCOL_MAPPER_ID`) REFERENCES `PROTOCOL_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROTOCOL_MAPPER_CONFIG`
--

LOCK TABLES `PROTOCOL_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `PROTOCOL_MAPPER_CONFIG` DISABLE KEYS */;
INSERT INTO `PROTOCOL_MAPPER_CONFIG` VALUES ('03e1cd62-1b45-4fc0-962d-572be9350336','true','access.token.claim'),('03e1cd62-1b45-4fc0-962d-572be9350336','true','id.token.claim'),('03e1cd62-1b45-4fc0-962d-572be9350336','country','user.attribute.country'),('03e1cd62-1b45-4fc0-962d-572be9350336','formatted','user.attribute.formatted'),('03e1cd62-1b45-4fc0-962d-572be9350336','locality','user.attribute.locality'),('03e1cd62-1b45-4fc0-962d-572be9350336','postal_code','user.attribute.postal_code'),('03e1cd62-1b45-4fc0-962d-572be9350336','region','user.attribute.region'),('03e1cd62-1b45-4fc0-962d-572be9350336','street','user.attribute.street'),('03e1cd62-1b45-4fc0-962d-572be9350336','true','userinfo.token.claim'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','true','access.token.claim'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','zoneinfo','claim.name'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','true','id.token.claim'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','String','jsonType.label'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','zoneinfo','user.attribute'),('12ea7f21-0c4f-4825-ae44-03974e7bcb81','true','userinfo.token.claim'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','true','access.token.claim'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','phone_number_verified','claim.name'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','true','id.token.claim'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','boolean','jsonType.label'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','phoneNumberVerified','user.attribute'),('1c6a9088-080e-4204-9e05-5c0e12c267b9','true','userinfo.token.claim'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','true','access.token.claim'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','birthdate','claim.name'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','true','id.token.claim'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','String','jsonType.label'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','birthdate','user.attribute'),('1f58df75-e6ef-43fe-952c-3e9dffaa5d5a','true','userinfo.token.claim'),('21744f05-23ca-46c8-88b9-15c4ebddd839','true','access.token.claim'),('21744f05-23ca-46c8-88b9-15c4ebddd839','updated_at','claim.name'),('21744f05-23ca-46c8-88b9-15c4ebddd839','true','id.token.claim'),('21744f05-23ca-46c8-88b9-15c4ebddd839','String','jsonType.label'),('21744f05-23ca-46c8-88b9-15c4ebddd839','updatedAt','user.attribute'),('21744f05-23ca-46c8-88b9-15c4ebddd839','true','userinfo.token.claim'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','true','access.token.claim'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','updated_at','claim.name'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','true','id.token.claim'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','String','jsonType.label'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','updatedAt','user.attribute'),('2a05659b-19eb-4a61-bae7-cbf0a72ab620','true','userinfo.token.claim'),('2dd3a990-8c6e-4098-a847-41592bc84096','true','access.token.claim'),('2dd3a990-8c6e-4098-a847-41592bc84096','family_name','claim.name'),('2dd3a990-8c6e-4098-a847-41592bc84096','true','id.token.claim'),('2dd3a990-8c6e-4098-a847-41592bc84096','String','jsonType.label'),('2dd3a990-8c6e-4098-a847-41592bc84096','lastName','user.attribute'),('2dd3a990-8c6e-4098-a847-41592bc84096','true','userinfo.token.claim'),('3b9e387f-108f-4dc3-960d-efc6987157a5','true','access.token.claim'),('3b9e387f-108f-4dc3-960d-efc6987157a5','groups','claim.name'),('3b9e387f-108f-4dc3-960d-efc6987157a5','true','id.token.claim'),('3b9e387f-108f-4dc3-960d-efc6987157a5','String','jsonType.label'),('3b9e387f-108f-4dc3-960d-efc6987157a5','true','multivalued'),('3b9e387f-108f-4dc3-960d-efc6987157a5','foo','user.attribute'),('3f99f922-94a9-49fd-a362-4b3820127403','true','access.token.claim'),('3f99f922-94a9-49fd-a362-4b3820127403','given_name','claim.name'),('3f99f922-94a9-49fd-a362-4b3820127403','true','id.token.claim'),('3f99f922-94a9-49fd-a362-4b3820127403','String','jsonType.label'),('3f99f922-94a9-49fd-a362-4b3820127403','firstName','user.attribute'),('3f99f922-94a9-49fd-a362-4b3820127403','true','userinfo.token.claim'),('476e806c-da5c-4286-8ccb-a654491c57a7','true','access.token.claim'),('476e806c-da5c-4286-8ccb-a654491c57a7','resource_access.${client_id}.roles','claim.name'),('476e806c-da5c-4286-8ccb-a654491c57a7','String','jsonType.label'),('476e806c-da5c-4286-8ccb-a654491c57a7','true','multivalued'),('476e806c-da5c-4286-8ccb-a654491c57a7','foo','user.attribute'),('4b6957d8-c349-43ac-a163-41613f407475','true','access.token.claim'),('4b6957d8-c349-43ac-a163-41613f407475','locale','claim.name'),('4b6957d8-c349-43ac-a163-41613f407475','true','id.token.claim'),('4b6957d8-c349-43ac-a163-41613f407475','String','jsonType.label'),('4b6957d8-c349-43ac-a163-41613f407475','locale','user.attribute'),('4b6957d8-c349-43ac-a163-41613f407475','true','userinfo.token.claim'),('51019db7-295c-48d1-b77e-68a836252f4a','true','access.token.claim'),('51019db7-295c-48d1-b77e-68a836252f4a','true','id.token.claim'),('51019db7-295c-48d1-b77e-68a836252f4a','true','userinfo.token.claim'),('52870437-98f4-4be2-9901-dfb360da9ad6','true','access.token.claim'),('52870437-98f4-4be2-9901-dfb360da9ad6','phone_number_verified','claim.name'),('52870437-98f4-4be2-9901-dfb360da9ad6','true','id.token.claim'),('52870437-98f4-4be2-9901-dfb360da9ad6','boolean','jsonType.label'),('52870437-98f4-4be2-9901-dfb360da9ad6','phoneNumberVerified','user.attribute'),('52870437-98f4-4be2-9901-dfb360da9ad6','true','userinfo.token.claim'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','true','access.token.claim'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','realm_access.roles','claim.name'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','String','jsonType.label'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','true','multivalued'),('59d11eb0-c9a3-4b75-9154-2ef988188d2c','foo','user.attribute'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','true','access.token.claim'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','locale','claim.name'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','true','id.token.claim'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','String','jsonType.label'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','locale','user.attribute'),('5bc7d60c-4638-4b16-af6c-ac61ec1bb0af','true','userinfo.token.claim'),('61d24ccb-6fde-4128-bd88-555ed34542a6','true','access.token.claim'),('61d24ccb-6fde-4128-bd88-555ed34542a6','groups','claim.name'),('61d24ccb-6fde-4128-bd88-555ed34542a6','true','id.token.claim'),('61d24ccb-6fde-4128-bd88-555ed34542a6','String','jsonType.label'),('61d24ccb-6fde-4128-bd88-555ed34542a6','true','multivalued'),('61d24ccb-6fde-4128-bd88-555ed34542a6','foo','user.attribute'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','true','access.token.claim'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','picture','claim.name'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','true','id.token.claim'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','String','jsonType.label'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','picture','user.attribute'),('68deeee3-6445-495c-bf1e-7e2aed6ba112','true','userinfo.token.claim'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','true','access.token.claim'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','family_name','claim.name'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','true','id.token.claim'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','String','jsonType.label'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','lastName','user.attribute'),('69b83dd6-17e8-4423-8e0b-b2a87046d391','true','userinfo.token.claim'),('6e02c16f-046a-4a6c-af57-dafcb4057135','true','access.token.claim'),('6e02c16f-046a-4a6c-af57-dafcb4057135','zoneinfo','claim.name'),('6e02c16f-046a-4a6c-af57-dafcb4057135','true','id.token.claim'),('6e02c16f-046a-4a6c-af57-dafcb4057135','String','jsonType.label'),('6e02c16f-046a-4a6c-af57-dafcb4057135','zoneinfo','user.attribute'),('6e02c16f-046a-4a6c-af57-dafcb4057135','true','userinfo.token.claim'),('6f254500-4113-45d8-90f1-b93527d946e6','true','access.token.claim'),('6f254500-4113-45d8-90f1-b93527d946e6','phone_number','claim.name'),('6f254500-4113-45d8-90f1-b93527d946e6','true','id.token.claim'),('6f254500-4113-45d8-90f1-b93527d946e6','String','jsonType.label'),('6f254500-4113-45d8-90f1-b93527d946e6','phoneNumber','user.attribute'),('6f254500-4113-45d8-90f1-b93527d946e6','true','userinfo.token.claim'),('770bb0e9-cb6a-490c-9182-9264fdad2776','true','access.token.claim'),('770bb0e9-cb6a-490c-9182-9264fdad2776','website','claim.name'),('770bb0e9-cb6a-490c-9182-9264fdad2776','true','id.token.claim'),('770bb0e9-cb6a-490c-9182-9264fdad2776','String','jsonType.label'),('770bb0e9-cb6a-490c-9182-9264fdad2776','website','user.attribute'),('770bb0e9-cb6a-490c-9182-9264fdad2776','true','userinfo.token.claim'),('7774afdd-cfce-4f10-ba0e-70103b1cdba9','true','access.token.claim'),('7774afdd-cfce-4f10-ba0e-70103b1cdba9','true','id.token.claim'),('7774afdd-cfce-4f10-ba0e-70103b1cdba9','true','userinfo.token.claim'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','true','access.token.claim'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','phone_number','claim.name'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','true','id.token.claim'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','String','jsonType.label'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','phoneNumber','user.attribute'),('883ed3b0-8b1a-414c-86c4-08e2b5d08714','true','userinfo.token.claim'),('8a24cd18-82f7-4328-9c54-cdb84680b088','true','access.token.claim'),('8a24cd18-82f7-4328-9c54-cdb84680b088','realm_access.roles','claim.name'),('8a24cd18-82f7-4328-9c54-cdb84680b088','String','jsonType.label'),('8a24cd18-82f7-4328-9c54-cdb84680b088','true','multivalued'),('8a24cd18-82f7-4328-9c54-cdb84680b088','foo','user.attribute'),('8d76a385-d210-4674-97e0-6b4bc0543339','true','access.token.claim'),('8d76a385-d210-4674-97e0-6b4bc0543339','email_verified','claim.name'),('8d76a385-d210-4674-97e0-6b4bc0543339','true','id.token.claim'),('8d76a385-d210-4674-97e0-6b4bc0543339','boolean','jsonType.label'),('8d76a385-d210-4674-97e0-6b4bc0543339','emailVerified','user.attribute'),('8d76a385-d210-4674-97e0-6b4bc0543339','true','userinfo.token.claim'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','true','access.token.claim'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','birthdate','claim.name'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','true','id.token.claim'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','String','jsonType.label'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','birthdate','user.attribute'),('8fd6655c-9ff8-47ce-8335-767deb9d99fb','true','userinfo.token.claim'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','true','access.token.claim'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','preferred_username','claim.name'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','true','id.token.claim'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','String','jsonType.label'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','username','user.attribute'),('93b3314d-bc0f-462d-bd82-635d3bd18f46','true','userinfo.token.claim'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','true','access.token.claim'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','true','id.token.claim'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','country','user.attribute.country'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','formatted','user.attribute.formatted'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','locality','user.attribute.locality'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','postal_code','user.attribute.postal_code'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','region','user.attribute.region'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','street','user.attribute.street'),('9453e615-6896-4e47-b9a0-1ad6786bc53d','true','userinfo.token.claim'),('ad7e0577-e842-46f6-ba37-063c7326abf4','true','access.token.claim'),('ad7e0577-e842-46f6-ba37-063c7326abf4','gender','claim.name'),('ad7e0577-e842-46f6-ba37-063c7326abf4','true','id.token.claim'),('ad7e0577-e842-46f6-ba37-063c7326abf4','String','jsonType.label'),('ad7e0577-e842-46f6-ba37-063c7326abf4','gender','user.attribute'),('ad7e0577-e842-46f6-ba37-063c7326abf4','true','userinfo.token.claim'),('b1484c99-c19e-452d-9528-17c5b9342f9a','true','access.token.claim'),('b1484c99-c19e-452d-9528-17c5b9342f9a','preferred_username','claim.name'),('b1484c99-c19e-452d-9528-17c5b9342f9a','true','id.token.claim'),('b1484c99-c19e-452d-9528-17c5b9342f9a','String','jsonType.label'),('b1484c99-c19e-452d-9528-17c5b9342f9a','username','user.attribute'),('b1484c99-c19e-452d-9528-17c5b9342f9a','true','userinfo.token.claim'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','true','access.token.claim'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','picture','claim.name'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','true','id.token.claim'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','String','jsonType.label'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','picture','user.attribute'),('b5eac6f2-d121-4557-a1cf-bf694ed63fe1','true','userinfo.token.claim'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','true','access.token.claim'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','nickname','claim.name'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','true','id.token.claim'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','String','jsonType.label'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','nickname','user.attribute'),('bb942b84-3e32-46e3-9bb8-5dfde4e9c32d','true','userinfo.token.claim'),('bcddf94a-9cac-4897-b659-bb93417bb030','true','access.token.claim'),('bcddf94a-9cac-4897-b659-bb93417bb030','given_name','claim.name'),('bcddf94a-9cac-4897-b659-bb93417bb030','true','id.token.claim'),('bcddf94a-9cac-4897-b659-bb93417bb030','String','jsonType.label'),('bcddf94a-9cac-4897-b659-bb93417bb030','firstName','user.attribute'),('bcddf94a-9cac-4897-b659-bb93417bb030','true','userinfo.token.claim'),('bce0af2b-76c3-4f26-9983-00fac40306cf','true','access.token.claim'),('bce0af2b-76c3-4f26-9983-00fac40306cf','middle_name','claim.name'),('bce0af2b-76c3-4f26-9983-00fac40306cf','true','id.token.claim'),('bce0af2b-76c3-4f26-9983-00fac40306cf','String','jsonType.label'),('bce0af2b-76c3-4f26-9983-00fac40306cf','middleName','user.attribute'),('bce0af2b-76c3-4f26-9983-00fac40306cf','true','userinfo.token.claim'),('be367858-7d9c-40c7-a145-25b20ddefd2f','Role','attribute.name'),('be367858-7d9c-40c7-a145-25b20ddefd2f','Basic','attribute.nameformat'),('be367858-7d9c-40c7-a145-25b20ddefd2f','false','single'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','true','access.token.claim'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','gender','claim.name'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','true','id.token.claim'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','String','jsonType.label'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','gender','user.attribute'),('c06eefd6-b085-4e91-88ca-e3df9620efd3','true','userinfo.token.claim'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','true','access.token.claim'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','profile','claim.name'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','true','id.token.claim'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','String','jsonType.label'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','profile','user.attribute'),('c581170b-5944-4d33-b6b5-b0766bb5e5bf','true','userinfo.token.claim'),('c5ca3565-3b66-4e47-8b2f-862174513ead','true','access.token.claim'),('c5ca3565-3b66-4e47-8b2f-862174513ead','email_verified','claim.name'),('c5ca3565-3b66-4e47-8b2f-862174513ead','true','id.token.claim'),('c5ca3565-3b66-4e47-8b2f-862174513ead','boolean','jsonType.label'),('c5ca3565-3b66-4e47-8b2f-862174513ead','emailVerified','user.attribute'),('c5ca3565-3b66-4e47-8b2f-862174513ead','true','userinfo.token.claim'),('ca5aaee7-8d7a-4c55-a9d2-533838918e68','Role','attribute.name'),('ca5aaee7-8d7a-4c55-a9d2-533838918e68','Basic','attribute.nameformat'),('ca5aaee7-8d7a-4c55-a9d2-533838918e68','false','single'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','true','access.token.claim'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','locale','claim.name'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','true','id.token.claim'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','String','jsonType.label'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','locale','user.attribute'),('cb73da1f-5b96-4c5e-b4a3-b676fe38483d','true','userinfo.token.claim'),('d3986df1-b765-4c2e-9808-06353556936a','true','access.token.claim'),('d3986df1-b765-4c2e-9808-06353556936a','resource_access.${client_id}.roles','claim.name'),('d3986df1-b765-4c2e-9808-06353556936a','String','jsonType.label'),('d3986df1-b765-4c2e-9808-06353556936a','true','multivalued'),('d3986df1-b765-4c2e-9808-06353556936a','foo','user.attribute'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','true','access.token.claim'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','upn','claim.name'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','true','id.token.claim'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','String','jsonType.label'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','username','user.attribute'),('d73f7ebe-35af-423a-a4d9-eaf2d35b11c2','true','userinfo.token.claim'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','true','access.token.claim'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','website','claim.name'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','true','id.token.claim'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','String','jsonType.label'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','website','user.attribute'),('d7ca18cd-b7d0-4d98-9899-60ecabdf29a9','true','userinfo.token.claim'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','true','access.token.claim'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','nickname','claim.name'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','true','id.token.claim'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','String','jsonType.label'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','nickname','user.attribute'),('dccf84fa-a5e6-4c4b-ab47-66367ae7d14c','true','userinfo.token.claim'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','true','access.token.claim'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','profile','claim.name'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','true','id.token.claim'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','String','jsonType.label'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','profile','user.attribute'),('dee41b3e-e9ba-4c8e-80c0-826eaae5dae7','true','userinfo.token.claim'),('e4652a24-5160-4413-bca3-13d108095e35','true','access.token.claim'),('e4652a24-5160-4413-bca3-13d108095e35','middle_name','claim.name'),('e4652a24-5160-4413-bca3-13d108095e35','true','id.token.claim'),('e4652a24-5160-4413-bca3-13d108095e35','String','jsonType.label'),('e4652a24-5160-4413-bca3-13d108095e35','middleName','user.attribute'),('e4652a24-5160-4413-bca3-13d108095e35','true','userinfo.token.claim'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','true','access.token.claim'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','email','claim.name'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','true','id.token.claim'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','String','jsonType.label'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','email','user.attribute'),('f0ec345c-615b-4a3e-8972-ee69fffc414a','true','userinfo.token.claim'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','true','access.token.claim'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','email','claim.name'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','true','id.token.claim'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','String','jsonType.label'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','email','user.attribute'),('f285aabc-37d5-411e-acff-5fcabbdfa67f','true','userinfo.token.claim'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','true','access.token.claim'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','locale','claim.name'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','true','id.token.claim'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','String','jsonType.label'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','locale','user.attribute'),('f82a13d0-d2fb-4849-af9d-e28e79bbf626','true','userinfo.token.claim'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','true','access.token.claim'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','upn','claim.name'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','true','id.token.claim'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','String','jsonType.label'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','username','user.attribute'),('fa523a2f-c07d-4ada-9531-5230dd0b796e','true','userinfo.token.claim');
/*!40000 ALTER TABLE `PROTOCOL_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM`
--

DROP TABLE IF EXISTS `REALM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM` (
  `ID` varchar(36) NOT NULL,
  `ACCESS_CODE_LIFESPAN` int DEFAULT NULL,
  `USER_ACTION_LIFESPAN` int DEFAULT NULL,
  `ACCESS_TOKEN_LIFESPAN` int DEFAULT NULL,
  `ACCOUNT_THEME` varchar(255) DEFAULT NULL,
  `ADMIN_THEME` varchar(255) DEFAULT NULL,
  `EMAIL_THEME` varchar(255) DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EVENTS_EXPIRATION` bigint DEFAULT NULL,
  `LOGIN_THEME` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int DEFAULT NULL,
  `PASSWORD_POLICY` text,
  `REGISTRATION_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `RESET_PASSWORD_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `SOCIAL` bit(1) NOT NULL DEFAULT b'0',
  `SSL_REQUIRED` varchar(255) DEFAULT NULL,
  `SSO_IDLE_TIMEOUT` int DEFAULT NULL,
  `SSO_MAX_LIFESPAN` int DEFAULT NULL,
  `UPDATE_PROFILE_ON_SOC_LOGIN` bit(1) NOT NULL DEFAULT b'0',
  `VERIFY_EMAIL` bit(1) NOT NULL DEFAULT b'0',
  `MASTER_ADMIN_CLIENT` varchar(36) DEFAULT NULL,
  `LOGIN_LIFESPAN` int DEFAULT NULL,
  `INTERNATIONALIZATION_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_LOCALE` varchar(255) DEFAULT NULL,
  `REG_EMAIL_AS_USERNAME` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `ADMIN_EVENTS_DETAILS_ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `EDIT_USERNAME_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `OTP_POLICY_COUNTER` int DEFAULT '0',
  `OTP_POLICY_WINDOW` int DEFAULT '1',
  `OTP_POLICY_PERIOD` int DEFAULT '30',
  `OTP_POLICY_DIGITS` int DEFAULT '6',
  `OTP_POLICY_ALG` varchar(36) DEFAULT 'HmacSHA1',
  `OTP_POLICY_TYPE` varchar(36) DEFAULT 'totp',
  `BROWSER_FLOW` varchar(36) DEFAULT NULL,
  `REGISTRATION_FLOW` varchar(36) DEFAULT NULL,
  `DIRECT_GRANT_FLOW` varchar(36) DEFAULT NULL,
  `RESET_CREDENTIALS_FLOW` varchar(36) DEFAULT NULL,
  `CLIENT_AUTH_FLOW` varchar(36) DEFAULT NULL,
  `OFFLINE_SESSION_IDLE_TIMEOUT` int DEFAULT '0',
  `REVOKE_REFRESH_TOKEN` bit(1) NOT NULL DEFAULT b'0',
  `ACCESS_TOKEN_LIFE_IMPLICIT` int DEFAULT '0',
  `LOGIN_WITH_EMAIL_ALLOWED` bit(1) NOT NULL DEFAULT b'1',
  `DUPLICATE_EMAILS_ALLOWED` bit(1) NOT NULL DEFAULT b'0',
  `DOCKER_AUTH_FLOW` varchar(36) DEFAULT NULL,
  `REFRESH_TOKEN_MAX_REUSE` int DEFAULT '0',
  `ALLOW_USER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `SSO_MAX_LIFESPAN_REMEMBER_ME` int NOT NULL,
  `SSO_IDLE_TIMEOUT_REMEMBER_ME` int NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ORVSDMLA56612EAEFIQ6WL5OI` (`NAME`),
  KEY `IDX_REALM_MASTER_ADM_CLI` (`MASTER_ADMIN_CLIENT`),
  CONSTRAINT `FK_TRAF444KK6QRKMS7N56AIWQ5Y` FOREIGN KEY (`MASTER_ADMIN_CLIENT`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM`
--

LOCK TABLES `REALM` WRITE;
/*!40000 ALTER TABLE `REALM` DISABLE KEYS */;
INSERT INTO `REALM` VALUES ('master',60,300,60,NULL,NULL,NULL,_binary '',_binary '\0',0,NULL,'master',0,NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0','NONE',1800,36000,_binary '\0',_binary '\0','522586e3-ecfe-44af-8053-8859d3870549',1800,_binary '\0',NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0',0,1,30,6,'HmacSHA1','totp','b0cbca6d-372e-49dd-bdcf-14fce67d009f','fcd3f9ff-77ca-4e2d-9c0e-71d32053d75f','052a297e-6977-47ef-ac36-71502001a3b9','d51f8ea4-5687-49f7-811c-00887172a1b1','879ccd32-2d80-41b6-931d-3a5754924f53',2592000,_binary '\0',900,_binary '\0',_binary '\0','7639537e-660e-486e-84c4-e203fccd71c3',0,_binary '',0,0),('Sc-project',60,300,300,NULL,NULL,NULL,_binary '',_binary '\0',0,NULL,'Sc-project',0,NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0','NONE',1800,36000,_binary '\0',_binary '\0','3510d930-aa13-46d9-bfd7-730a324d04da',1800,_binary '\0',NULL,_binary '\0',_binary '\0',_binary '\0',_binary '\0',0,1,30,6,'HmacSHA1','totp','e01b385b-7ba6-4bb0-a1b9-c001baa111b2','e9e32b41-6d9f-4a76-ba7c-cf366b58d62f','2c6ccc59-5ec8-4356-b703-e72b9a3ab823','2d365691-f30e-47b6-bde7-930bb15a7d17','aeb079fd-6ff5-4144-863c-968adad83dad',2592000,_binary '\0',900,_binary '',_binary '\0','95ac08d4-1301-4743-9bff-60b1a0bbfac8',0,_binary '\0',0,0);
/*!40000 ALTER TABLE `REALM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_ATTRIBUTE`
--

DROP TABLE IF EXISTS `REALM_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_ATTRIBUTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`NAME`,`REALM_ID`),
  KEY `IDX_REALM_ATTR_REALM` (`REALM_ID`),
  CONSTRAINT `FK_8SHXD6L3E9ATQUKACXGPFFPTW` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_ATTRIBUTE`
--

LOCK TABLES `REALM_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `REALM_ATTRIBUTE` DISABLE KEYS */;
INSERT INTO `REALM_ATTRIBUTE` VALUES ('_browser_header.contentSecurityPolicy','frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';','master'),('_browser_header.contentSecurityPolicy','frame-src \'self\'; frame-ancestors \'self\'; object-src \'none\';','Sc-project'),('_browser_header.contentSecurityPolicyReportOnly','','master'),('_browser_header.contentSecurityPolicyReportOnly','','Sc-project'),('_browser_header.strictTransportSecurity','max-age=31536000; includeSubDomains','master'),('_browser_header.strictTransportSecurity','max-age=31536000; includeSubDomains','Sc-project'),('_browser_header.xContentTypeOptions','nosniff','master'),('_browser_header.xContentTypeOptions','nosniff','Sc-project'),('_browser_header.xFrameOptions','SAMEORIGIN','master'),('_browser_header.xFrameOptions','SAMEORIGIN','Sc-project'),('_browser_header.xRobotsTag','none','master'),('_browser_header.xRobotsTag','none','Sc-project'),('_browser_header.xXSSProtection','1; mode=block','master'),('_browser_header.xXSSProtection','1; mode=block','Sc-project'),('actionTokenGeneratedByAdminLifespan','43200','Sc-project'),('actionTokenGeneratedByUserLifespan','300','Sc-project'),('bruteForceProtected','false','master'),('bruteForceProtected','false','Sc-project'),('displayName','Keycloak','master'),('displayNameHtml','<div class=\"kc-logo-text\"><span>Keycloak</span></div>','master'),('failureFactor','30','master'),('failureFactor','30','Sc-project'),('maxDeltaTimeSeconds','43200','master'),('maxDeltaTimeSeconds','43200','Sc-project'),('maxFailureWaitSeconds','900','master'),('maxFailureWaitSeconds','900','Sc-project'),('minimumQuickLoginWaitSeconds','60','master'),('minimumQuickLoginWaitSeconds','60','Sc-project'),('offlineSessionMaxLifespan','5184000','master'),('offlineSessionMaxLifespan','5184000','Sc-project'),('offlineSessionMaxLifespanEnabled','false','master'),('offlineSessionMaxLifespanEnabled','false','Sc-project'),('permanentLockout','false','master'),('permanentLockout','false','Sc-project'),('quickLoginCheckMilliSeconds','1000','master'),('quickLoginCheckMilliSeconds','1000','Sc-project'),('waitIncrementSeconds','60','master'),('waitIncrementSeconds','60','Sc-project'),('webAuthnPolicyAttestationConveyancePreference','not specified','Sc-project'),('webAuthnPolicyAttestationConveyancePreferencePasswordless','not specified','Sc-project'),('webAuthnPolicyAuthenticatorAttachment','not specified','Sc-project'),('webAuthnPolicyAuthenticatorAttachmentPasswordless','not specified','Sc-project'),('webAuthnPolicyAvoidSameAuthenticatorRegister','false','Sc-project'),('webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless','false','Sc-project'),('webAuthnPolicyCreateTimeout','0','Sc-project'),('webAuthnPolicyCreateTimeoutPasswordless','0','Sc-project'),('webAuthnPolicyRequireResidentKey','not specified','Sc-project'),('webAuthnPolicyRequireResidentKeyPasswordless','not specified','Sc-project'),('webAuthnPolicyRpEntityName','keycloak','Sc-project'),('webAuthnPolicyRpEntityNamePasswordless','keycloak','Sc-project'),('webAuthnPolicyRpId','','Sc-project'),('webAuthnPolicyRpIdPasswordless','','Sc-project'),('webAuthnPolicySignatureAlgorithms','ES256','Sc-project'),('webAuthnPolicySignatureAlgorithmsPasswordless','ES256','Sc-project'),('webAuthnPolicyUserVerificationRequirement','not specified','Sc-project'),('webAuthnPolicyUserVerificationRequirementPasswordless','not specified','Sc-project');
/*!40000 ALTER TABLE `REALM_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_DEFAULT_GROUPS`
--

DROP TABLE IF EXISTS `REALM_DEFAULT_GROUPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_DEFAULT_GROUPS` (
  `REALM_ID` varchar(36) NOT NULL,
  `GROUP_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`GROUP_ID`),
  UNIQUE KEY `CON_GROUP_ID_DEF_GROUPS` (`GROUP_ID`),
  KEY `IDX_REALM_DEF_GRP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_DEF_GROUPS_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `KEYCLOAK_GROUP` (`ID`),
  CONSTRAINT `FK_DEF_GROUPS_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_DEFAULT_GROUPS`
--

LOCK TABLES `REALM_DEFAULT_GROUPS` WRITE;
/*!40000 ALTER TABLE `REALM_DEFAULT_GROUPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_DEFAULT_GROUPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_DEFAULT_ROLES`
--

DROP TABLE IF EXISTS `REALM_DEFAULT_ROLES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_DEFAULT_ROLES` (
  `REALM_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`ROLE_ID`),
  UNIQUE KEY `UK_H4WPD7W4HSOOLNI3H0SW7BTJE` (`ROLE_ID`),
  KEY `IDX_REALM_DEF_ROLES_REALM` (`REALM_ID`),
  CONSTRAINT `FK_EVUDB1PPW84OXFAX2DRS03ICC` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`),
  CONSTRAINT `FK_H4WPD7W4HSOOLNI3H0SW7BTJE` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_DEFAULT_ROLES`
--

LOCK TABLES `REALM_DEFAULT_ROLES` WRITE;
/*!40000 ALTER TABLE `REALM_DEFAULT_ROLES` DISABLE KEYS */;
INSERT INTO `REALM_DEFAULT_ROLES` VALUES ('Sc-project','031730c1-5ec1-4cde-845c-2d9001f1858d'),('master','20c62e21-0357-4254-990d-fd6dba5c33e9'),('Sc-project','265728e9-d9de-4edf-83e5-560c14ace534'),('master','3a2b0ebb-462a-45a0-8a95-b48f58148162');
/*!40000 ALTER TABLE `REALM_DEFAULT_ROLES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_ENABLED_EVENT_TYPES`
--

DROP TABLE IF EXISTS `REALM_ENABLED_EVENT_TYPES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_ENABLED_EVENT_TYPES` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_TYPES_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NWEDRF5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_ENABLED_EVENT_TYPES`
--

LOCK TABLES `REALM_ENABLED_EVENT_TYPES` WRITE;
/*!40000 ALTER TABLE `REALM_ENABLED_EVENT_TYPES` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_ENABLED_EVENT_TYPES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_EVENTS_LISTENERS`
--

DROP TABLE IF EXISTS `REALM_EVENTS_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_EVENTS_LISTENERS` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_EVT_LIST_REALM` (`REALM_ID`),
  CONSTRAINT `FK_H846O4H0W8EPX5NXEV9F5Y69J` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_EVENTS_LISTENERS`
--

LOCK TABLES `REALM_EVENTS_LISTENERS` WRITE;
/*!40000 ALTER TABLE `REALM_EVENTS_LISTENERS` DISABLE KEYS */;
INSERT INTO `REALM_EVENTS_LISTENERS` VALUES ('master','jboss-logging'),('Sc-project','jboss-logging');
/*!40000 ALTER TABLE `REALM_EVENTS_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_REQUIRED_CREDENTIAL`
--

DROP TABLE IF EXISTS `REALM_REQUIRED_CREDENTIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_REQUIRED_CREDENTIAL` (
  `TYPE` varchar(255) NOT NULL,
  `FORM_LABEL` varchar(255) DEFAULT NULL,
  `INPUT` bit(1) NOT NULL DEFAULT b'0',
  `SECRET` bit(1) NOT NULL DEFAULT b'0',
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`TYPE`),
  CONSTRAINT `FK_5HG65LYBEVAVKQFKI3KPONH9V` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_REQUIRED_CREDENTIAL`
--

LOCK TABLES `REALM_REQUIRED_CREDENTIAL` WRITE;
/*!40000 ALTER TABLE `REALM_REQUIRED_CREDENTIAL` DISABLE KEYS */;
INSERT INTO `REALM_REQUIRED_CREDENTIAL` VALUES ('password','password',_binary '',_binary '','master'),('password','password',_binary '',_binary '','Sc-project');
/*!40000 ALTER TABLE `REALM_REQUIRED_CREDENTIAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_SMTP_CONFIG`
--

DROP TABLE IF EXISTS `REALM_SMTP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_SMTP_CONFIG` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`NAME`),
  CONSTRAINT `FK_70EJ8XDXGXD0B9HH6180IRR0O` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_SMTP_CONFIG`
--

LOCK TABLES `REALM_SMTP_CONFIG` WRITE;
/*!40000 ALTER TABLE `REALM_SMTP_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_SMTP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REALM_SUPPORTED_LOCALES`
--

DROP TABLE IF EXISTS `REALM_SUPPORTED_LOCALES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REALM_SUPPORTED_LOCALES` (
  `REALM_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`REALM_ID`,`VALUE`),
  KEY `IDX_REALM_SUPP_LOCAL_REALM` (`REALM_ID`),
  CONSTRAINT `FK_SUPPORTED_LOCALES_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REALM_SUPPORTED_LOCALES`
--

LOCK TABLES `REALM_SUPPORTED_LOCALES` WRITE;
/*!40000 ALTER TABLE `REALM_SUPPORTED_LOCALES` DISABLE KEYS */;
/*!40000 ALTER TABLE `REALM_SUPPORTED_LOCALES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REDIRECT_URIS`
--

DROP TABLE IF EXISTS `REDIRECT_URIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REDIRECT_URIS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_REDIR_URI_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_1BURS8PB4OUJ97H5WUPPAHV9F` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REDIRECT_URIS`
--

LOCK TABLES `REDIRECT_URIS` WRITE;
/*!40000 ALTER TABLE `REDIRECT_URIS` DISABLE KEYS */;
INSERT INTO `REDIRECT_URIS` VALUES ('1b9f755b-72b9-4ee4-8fc6-132ab0556852','http://localhost:8081/*'),('207e7c98-d443-4ef9-8a8d-5bf4438b2656','/realms/master/account/*'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','/admin/master/console/*'),('9de038f3-8dcd-4169-8514-c3ae3561ea90','/realms/Sc-project/account/*'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','/realms/Sc-project/account/*'),('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','/realms/master/account/*'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','/admin/Sc-project/console/*'),('f092c270-84ce-44f5-8649-e1fd9714d24b','http://localhost:4200/*');
/*!40000 ALTER TABLE `REDIRECT_URIS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REQUIRED_ACTION_CONFIG`
--

DROP TABLE IF EXISTS `REQUIRED_ACTION_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REQUIRED_ACTION_CONFIG` (
  `REQUIRED_ACTION_ID` varchar(36) NOT NULL,
  `VALUE` longtext,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`REQUIRED_ACTION_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REQUIRED_ACTION_CONFIG`
--

LOCK TABLES `REQUIRED_ACTION_CONFIG` WRITE;
/*!40000 ALTER TABLE `REQUIRED_ACTION_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `REQUIRED_ACTION_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REQUIRED_ACTION_PROVIDER`
--

DROP TABLE IF EXISTS `REQUIRED_ACTION_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REQUIRED_ACTION_PROVIDER` (
  `ID` varchar(36) NOT NULL,
  `ALIAS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `DEFAULT_ACTION` bit(1) NOT NULL DEFAULT b'0',
  `PROVIDER_ID` varchar(255) DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_REQ_ACT_PROV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_REQ_ACT_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REQUIRED_ACTION_PROVIDER`
--

LOCK TABLES `REQUIRED_ACTION_PROVIDER` WRITE;
/*!40000 ALTER TABLE `REQUIRED_ACTION_PROVIDER` DISABLE KEYS */;
INSERT INTO `REQUIRED_ACTION_PROVIDER` VALUES ('0f40e40c-5daf-4c81-8bb9-2519f10db04d','CONFIGURE_TOTP','Configure OTP','master',_binary '',_binary '\0','CONFIGURE_TOTP',10),('3057b599-d265-4ec4-b43e-e606f98652a3','UPDATE_PROFILE','Update Profile','master',_binary '',_binary '\0','UPDATE_PROFILE',40),('6e2c99a4-43a2-40b4-98c5-86d9f7fd7ebf','UPDATE_PASSWORD','Update Password','master',_binary '',_binary '\0','UPDATE_PASSWORD',30),('7617aa40-1e43-40b3-9dfe-3f86c531982c','update_user_locale','Update User Locale','Sc-project',_binary '',_binary '\0','update_user_locale',1000),('7980ed7f-4428-4def-b11a-3d741e346782','update_user_locale','Update User Locale','master',_binary '',_binary '\0','update_user_locale',1000),('952f2c16-3bf8-4e64-bb79-5ff4acdd47d5','terms_and_conditions','Terms and Conditions','master',_binary '\0',_binary '\0','terms_and_conditions',20),('b19321df-8599-48d6-a077-bc53e407057c','VERIFY_EMAIL','Verify Email','Sc-project',_binary '',_binary '\0','VERIFY_EMAIL',50),('b701619b-7517-49cd-b2ed-e8d2ae572303','CONFIGURE_TOTP','Configure OTP','Sc-project',_binary '',_binary '\0','CONFIGURE_TOTP',10),('c1354bc2-5d24-435e-be4b-76f7fc32244e','UPDATE_PROFILE','Update Profile','Sc-project',_binary '',_binary '\0','UPDATE_PROFILE',40),('dc67d92b-4fa2-4a29-a977-f95366f39c8d','VERIFY_EMAIL','Verify Email','master',_binary '',_binary '\0','VERIFY_EMAIL',50),('f9703b6d-d117-4390-ad82-e7028ab8ff5e','UPDATE_PASSWORD','Update Password','Sc-project',_binary '',_binary '\0','UPDATE_PASSWORD',30),('fa55323a-5df7-4179-80de-dbe5ad83efb6','terms_and_conditions','Terms and Conditions','Sc-project',_binary '\0',_binary '\0','terms_and_conditions',20);
/*!40000 ALTER TABLE `REQUIRED_ACTION_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_ATTRIBUTE`
--

DROP TABLE IF EXISTS `RESOURCE_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `RESOURCE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_5HRM2VLF9QL5FU022KQEPOVBR` (`RESOURCE_ID`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU022KQEPOVBR` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_ATTRIBUTE`
--

LOCK TABLES `RESOURCE_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_POLICY`
--

DROP TABLE IF EXISTS `RESOURCE_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_POLICY` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`POLICY_ID`),
  KEY `IDX_RES_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRPOS53XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRPP213XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_POLICY`
--

LOCK TABLES `RESOURCE_POLICY` WRITE;
/*!40000 ALTER TABLE `RESOURCE_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SCOPE`
--

DROP TABLE IF EXISTS `RESOURCE_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SCOPE` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`SCOPE_ID`),
  KEY `IDX_RES_SCOPE_SCOPE` (`SCOPE_ID`),
  CONSTRAINT `FK_FRSRPOS13XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRPS213XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SCOPE`
--

LOCK TABLES `RESOURCE_SCOPE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER` (
  `ID` varchar(36) NOT NULL,
  `ALLOW_RS_REMOTE_MGMT` bit(1) NOT NULL DEFAULT b'0',
  `POLICY_ENFORCE_MODE` varchar(15) NOT NULL,
  `DECISION_STRATEGY` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER`
--

LOCK TABLES `RESOURCE_SERVER` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_PERM_TICKET`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_PERM_TICKET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_PERM_TICKET` (
  `ID` varchar(36) NOT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  `REQUESTER` varchar(255) DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint NOT NULL,
  `GRANTED_TIMESTAMP` bigint DEFAULT NULL,
  `RESOURCE_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5PMT` (`OWNER`,`REQUESTER`,`RESOURCE_SERVER_ID`,`RESOURCE_ID`,`SCOPE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG82SSPMT` (`RESOURCE_SERVER_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG83SSPMT` (`RESOURCE_ID`),
  KEY `FK_FRSRHO213XCX4WNKOG84SSPMT` (`SCOPE_ID`),
  KEY `FK_FRSRPO2128CX4WNKOG82SSRFY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSPMT` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG83SSPMT` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG84SSPMT` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`),
  CONSTRAINT `FK_FRSRPO2128CX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_PERM_TICKET`
--

LOCK TABLES `RESOURCE_SERVER_PERM_TICKET` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_PERM_TICKET` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_PERM_TICKET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_POLICY`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_POLICY` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `TYPE` varchar(255) NOT NULL,
  `DECISION_STRATEGY` varchar(20) DEFAULT NULL,
  `LOGIC` varchar(20) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRPT700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SERV_POL_RES_SERV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRPO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_POLICY`
--

LOCK TABLES `RESOURCE_SERVER_POLICY` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_RESOURCE`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_RESOURCE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_RESOURCE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `ICON_URI` varchar(255) DEFAULT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `OWNER_MANAGED_ACCESS` bit(1) NOT NULL DEFAULT b'0',
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSR6T700S9V50BU18WS5HA6` (`NAME`,`OWNER`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_RES_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_RESOURCE`
--

LOCK TABLES `RESOURCE_SERVER_RESOURCE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_RESOURCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_RESOURCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_SERVER_SCOPE`
--

DROP TABLE IF EXISTS `RESOURCE_SERVER_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_SERVER_SCOPE` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `ICON_URI` varchar(255) DEFAULT NULL,
  `RESOURCE_SERVER_ID` varchar(36) DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FRSRST700S9V50BU18WS5HA6` (`NAME`,`RESOURCE_SERVER_ID`),
  KEY `IDX_RES_SRV_SCOPE_RES_SRV` (`RESOURCE_SERVER_ID`),
  CONSTRAINT `FK_FRSRSO213XCX4WNKOG82SSRFY` FOREIGN KEY (`RESOURCE_SERVER_ID`) REFERENCES `RESOURCE_SERVER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_SERVER_SCOPE`
--

LOCK TABLES `RESOURCE_SERVER_SCOPE` WRITE;
/*!40000 ALTER TABLE `RESOURCE_SERVER_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_SERVER_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RESOURCE_URIS`
--

DROP TABLE IF EXISTS `RESOURCE_URIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RESOURCE_URIS` (
  `RESOURCE_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`,`VALUE`),
  CONSTRAINT `FK_RESOURCE_SERVER_URIS` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `RESOURCE_SERVER_RESOURCE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RESOURCE_URIS`
--

LOCK TABLES `RESOURCE_URIS` WRITE;
/*!40000 ALTER TABLE `RESOURCE_URIS` DISABLE KEYS */;
/*!40000 ALTER TABLE `RESOURCE_URIS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE_ATTRIBUTE`
--

DROP TABLE IF EXISTS `ROLE_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ROLE_ATTRIBUTE` (
  `ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_ROLE_ATTRIBUTE` (`ROLE_ID`),
  CONSTRAINT `FK_ROLE_ATTRIBUTE_ID` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE_ATTRIBUTE`
--

LOCK TABLES `ROLE_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `ROLE_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `ROLE_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCOPE_MAPPING`
--

DROP TABLE IF EXISTS `SCOPE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SCOPE_MAPPING` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`ROLE_ID`),
  KEY `IDX_SCOPE_MAPPING_ROLE` (`ROLE_ID`),
  CONSTRAINT `FK_OUSE064PLMLR732LXJCN1Q5F1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`),
  CONSTRAINT `FK_P3RH9GRKU11KQFRS4FLTT7RNQ` FOREIGN KEY (`ROLE_ID`) REFERENCES `KEYCLOAK_ROLE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCOPE_MAPPING`
--

LOCK TABLES `SCOPE_MAPPING` WRITE;
/*!40000 ALTER TABLE `SCOPE_MAPPING` DISABLE KEYS */;
INSERT INTO `SCOPE_MAPPING` VALUES ('cba24b0d-c4bc-4c7b-a4b4-4c11a5e249d5','704996b1-eb75-4c54-84c2-035e0fed2c5a'),('c16a2068-7765-47d5-b17e-c9079f96cbbd','a3620b79-c24b-4aed-86ed-016ce53e1190');
/*!40000 ALTER TABLE `SCOPE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCOPE_POLICY`
--

DROP TABLE IF EXISTS `SCOPE_POLICY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SCOPE_POLICY` (
  `SCOPE_ID` varchar(36) NOT NULL,
  `POLICY_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`SCOPE_ID`,`POLICY_ID`),
  KEY `IDX_SCOPE_POLICY_POLICY` (`POLICY_ID`),
  CONSTRAINT `FK_FRSRASP13XCX4WNKOG82SSRFY` FOREIGN KEY (`POLICY_ID`) REFERENCES `RESOURCE_SERVER_POLICY` (`ID`),
  CONSTRAINT `FK_FRSRPASS3XCX4WNKOG82SSRFY` FOREIGN KEY (`SCOPE_ID`) REFERENCES `RESOURCE_SERVER_SCOPE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCOPE_POLICY`
--

LOCK TABLES `SCOPE_POLICY` WRITE;
/*!40000 ALTER TABLE `SCOPE_POLICY` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCOPE_POLICY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERNAME_LOGIN_FAILURE`
--

DROP TABLE IF EXISTS `USERNAME_LOGIN_FAILURE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USERNAME_LOGIN_FAILURE` (
  `REALM_ID` varchar(36) NOT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FAILED_LOGIN_NOT_BEFORE` int DEFAULT NULL,
  `LAST_FAILURE` bigint DEFAULT NULL,
  `LAST_IP_FAILURE` varchar(255) DEFAULT NULL,
  `NUM_FAILURES` int DEFAULT NULL,
  PRIMARY KEY (`REALM_ID`,`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERNAME_LOGIN_FAILURE`
--

LOCK TABLES `USERNAME_LOGIN_FAILURE` WRITE;
/*!40000 ALTER TABLE `USERNAME_LOGIN_FAILURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERNAME_LOGIN_FAILURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ATTRIBUTE`
--

DROP TABLE IF EXISTS `USER_ATTRIBUTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ATTRIBUTE` (
  `NAME` varchar(255) NOT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `ID` varchar(36) NOT NULL DEFAULT 'sybase-needs-something-here',
  PRIMARY KEY (`ID`),
  KEY `IDX_USER_ATTRIBUTE` (`USER_ID`),
  CONSTRAINT `FK_5HRM2VLF9QL5FU043KQEPOVBR` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ATTRIBUTE`
--

LOCK TABLES `USER_ATTRIBUTE` WRITE;
/*!40000 ALTER TABLE `USER_ATTRIBUTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_ATTRIBUTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_CONSENT`
--

DROP TABLE IF EXISTS `USER_CONSENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_CONSENT` (
  `ID` varchar(36) NOT NULL,
  `CLIENT_ID` varchar(255) DEFAULT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `CREATED_DATE` bigint DEFAULT NULL,
  `LAST_UPDATED_DATE` bigint DEFAULT NULL,
  `CLIENT_STORAGE_PROVIDER` varchar(36) DEFAULT NULL,
  `EXTERNAL_CLIENT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_JKUWUVD56ONTGSUHOGM8UEWRT` (`CLIENT_ID`,`CLIENT_STORAGE_PROVIDER`,`EXTERNAL_CLIENT_ID`,`USER_ID`),
  KEY `IDX_USER_CONSENT` (`USER_ID`),
  CONSTRAINT `FK_GRNTCSNT_USER` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_CONSENT`
--

LOCK TABLES `USER_CONSENT` WRITE;
/*!40000 ALTER TABLE `USER_CONSENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_CONSENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_CONSENT_CLIENT_SCOPE`
--

DROP TABLE IF EXISTS `USER_CONSENT_CLIENT_SCOPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_CONSENT_CLIENT_SCOPE` (
  `USER_CONSENT_ID` varchar(36) NOT NULL,
  `SCOPE_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`USER_CONSENT_ID`,`SCOPE_ID`),
  KEY `IDX_USCONSENT_CLSCOPE` (`USER_CONSENT_ID`),
  CONSTRAINT `FK_GRNTCSNT_CLSC_USC` FOREIGN KEY (`USER_CONSENT_ID`) REFERENCES `USER_CONSENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_CONSENT_CLIENT_SCOPE`
--

LOCK TABLES `USER_CONSENT_CLIENT_SCOPE` WRITE;
/*!40000 ALTER TABLE `USER_CONSENT_CLIENT_SCOPE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_CONSENT_CLIENT_SCOPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ENTITY`
--

DROP TABLE IF EXISTS `USER_ENTITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ENTITY` (
  `ID` varchar(36) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `EMAIL_CONSTRAINT` varchar(255) DEFAULT NULL,
  `EMAIL_VERIFIED` bit(1) NOT NULL DEFAULT b'0',
  `ENABLED` bit(1) NOT NULL DEFAULT b'0',
  `FEDERATION_LINK` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `LAST_NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `CREATED_TIMESTAMP` bigint DEFAULT NULL,
  `SERVICE_ACCOUNT_CLIENT_LINK` varchar(255) DEFAULT NULL,
  `NOT_BEFORE` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_DYKN684SL8UP1CRFEI6ECKHD7` (`REALM_ID`,`EMAIL_CONSTRAINT`),
  UNIQUE KEY `UK_RU8TT6T700S9V50BU18WS5HA6` (`REALM_ID`,`USERNAME`),
  KEY `IDX_USER_EMAIL` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ENTITY`
--

LOCK TABLES `USER_ENTITY` WRITE;
/*!40000 ALTER TABLE `USER_ENTITY` DISABLE KEYS */;
INSERT INTO `USER_ENTITY` VALUES ('36044e7d-81de-4250-acee-f8962d9f857f','phou.jeannory@gmail.com','phou.jeannory@gmail.com',_binary '\0',_binary '',NULL,'jeannory','phou','Sc-project','phou.jeannory@gmail.com',1586128951116,NULL,0),('74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90','john.doe@gmail.com','john.doe@gmail.com',_binary '\0',_binary '',NULL,NULL,NULL,'Sc-project','john.doe@gmail.com',1586128896885,NULL,0),('78bf47b3-9d32-412d-8942-24e1def64a0b',NULL,'69cee763-c990-4d06-a329-42a22d841929',_binary '\0',_binary '',NULL,NULL,NULL,'master','admin',1586128479240,NULL,0),('87b0f71d-ee50-4d00-b433-0da7255245bb','test@gmail.com','test@gmail.com',_binary '\0',_binary '',NULL,'testy','ramy','Sc-project','test',1586129030417,NULL,0);
/*!40000 ALTER TABLE `USER_ENTITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_CONFIG`
--

DROP TABLE IF EXISTS `USER_FEDERATION_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_CONFIG` (
  `USER_FEDERATION_PROVIDER_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_PROVIDER_ID`,`NAME`),
  CONSTRAINT `FK_T13HPU1J94R2EBPEKR39X5EU5` FOREIGN KEY (`USER_FEDERATION_PROVIDER_ID`) REFERENCES `USER_FEDERATION_PROVIDER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_CONFIG`
--

LOCK TABLES `USER_FEDERATION_CONFIG` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_MAPPER`
--

DROP TABLE IF EXISTS `USER_FEDERATION_MAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_MAPPER` (
  `ID` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `FEDERATION_PROVIDER_ID` varchar(36) NOT NULL,
  `FEDERATION_MAPPER_TYPE` varchar(255) NOT NULL,
  `REALM_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_MAP_FED_PRV` (`FEDERATION_PROVIDER_ID`),
  KEY `IDX_USR_FED_MAP_REALM` (`REALM_ID`),
  CONSTRAINT `FK_FEDMAPPERPM_FEDPRV` FOREIGN KEY (`FEDERATION_PROVIDER_ID`) REFERENCES `USER_FEDERATION_PROVIDER` (`ID`),
  CONSTRAINT `FK_FEDMAPPERPM_REALM` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_MAPPER`
--

LOCK TABLES `USER_FEDERATION_MAPPER` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_MAPPER_CONFIG`
--

DROP TABLE IF EXISTS `USER_FEDERATION_MAPPER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_MAPPER_CONFIG` (
  `USER_FEDERATION_MAPPER_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_FEDERATION_MAPPER_ID`,`NAME`),
  CONSTRAINT `FK_FEDMAPPER_CFG` FOREIGN KEY (`USER_FEDERATION_MAPPER_ID`) REFERENCES `USER_FEDERATION_MAPPER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_MAPPER_CONFIG`
--

LOCK TABLES `USER_FEDERATION_MAPPER_CONFIG` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_MAPPER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_FEDERATION_PROVIDER`
--

DROP TABLE IF EXISTS `USER_FEDERATION_PROVIDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_FEDERATION_PROVIDER` (
  `ID` varchar(36) NOT NULL,
  `CHANGED_SYNC_PERIOD` int DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `FULL_SYNC_PERIOD` int DEFAULT NULL,
  `LAST_SYNC` int DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  `PROVIDER_NAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IDX_USR_FED_PRV_REALM` (`REALM_ID`),
  CONSTRAINT `FK_1FJ32F6PTOLW2QY60CD8N01E8` FOREIGN KEY (`REALM_ID`) REFERENCES `REALM` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_FEDERATION_PROVIDER`
--

LOCK TABLES `USER_FEDERATION_PROVIDER` WRITE;
/*!40000 ALTER TABLE `USER_FEDERATION_PROVIDER` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_FEDERATION_PROVIDER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_GROUP_MEMBERSHIP`
--

DROP TABLE IF EXISTS `USER_GROUP_MEMBERSHIP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_GROUP_MEMBERSHIP` (
  `GROUP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `IDX_USER_GROUP_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_USER_GROUP_USER` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_GROUP_MEMBERSHIP`
--

LOCK TABLES `USER_GROUP_MEMBERSHIP` WRITE;
/*!40000 ALTER TABLE `USER_GROUP_MEMBERSHIP` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_GROUP_MEMBERSHIP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_REQUIRED_ACTION`
--

DROP TABLE IF EXISTS `USER_REQUIRED_ACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_REQUIRED_ACTION` (
  `USER_ID` varchar(36) NOT NULL,
  `REQUIRED_ACTION` varchar(255) NOT NULL DEFAULT ' ',
  PRIMARY KEY (`REQUIRED_ACTION`,`USER_ID`),
  KEY `IDX_USER_REQACTIONS` (`USER_ID`),
  CONSTRAINT `FK_6QJ3W1JW9CVAFHE19BWSIUVMD` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_REQUIRED_ACTION`
--

LOCK TABLES `USER_REQUIRED_ACTION` WRITE;
/*!40000 ALTER TABLE `USER_REQUIRED_ACTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_REQUIRED_ACTION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_ROLE_MAPPING`
--

DROP TABLE IF EXISTS `USER_ROLE_MAPPING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_ROLE_MAPPING` (
  `ROLE_ID` varchar(255) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`USER_ID`),
  KEY `IDX_USER_ROLE_MAPPING` (`USER_ID`),
  CONSTRAINT `FK_C4FQV34P1MBYLLOXANG7B1Q3L` FOREIGN KEY (`USER_ID`) REFERENCES `USER_ENTITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_ROLE_MAPPING`
--

LOCK TABLES `USER_ROLE_MAPPING` WRITE;
/*!40000 ALTER TABLE `USER_ROLE_MAPPING` DISABLE KEYS */;
INSERT INTO `USER_ROLE_MAPPING` VALUES ('031730c1-5ec1-4cde-845c-2d9001f1858d','36044e7d-81de-4250-acee-f8962d9f857f'),('0b7a6b42-19fd-49b0-a3bd-b8e300f0b209','36044e7d-81de-4250-acee-f8962d9f857f'),('265728e9-d9de-4edf-83e5-560c14ace534','36044e7d-81de-4250-acee-f8962d9f857f'),('29256e83-7313-4cc7-85d8-b02a5263b3a6','36044e7d-81de-4250-acee-f8962d9f857f'),('51b7530b-928b-45e3-9770-f5a632303eb4','36044e7d-81de-4250-acee-f8962d9f857f'),('a3620b79-c24b-4aed-86ed-016ce53e1190','36044e7d-81de-4250-acee-f8962d9f857f'),('031730c1-5ec1-4cde-845c-2d9001f1858d','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90'),('0b7a6b42-19fd-49b0-a3bd-b8e300f0b209','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90'),('265728e9-d9de-4edf-83e5-560c14ace534','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90'),('29256e83-7313-4cc7-85d8-b02a5263b3a6','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90'),('a3620b79-c24b-4aed-86ed-016ce53e1190','74e2a72a-e7ae-4e7c-a9b4-75ef7ba34c90'),('20c62e21-0357-4254-990d-fd6dba5c33e9','78bf47b3-9d32-412d-8942-24e1def64a0b'),('3a2b0ebb-462a-45a0-8a95-b48f58148162','78bf47b3-9d32-412d-8942-24e1def64a0b'),('704996b1-eb75-4c54-84c2-035e0fed2c5a','78bf47b3-9d32-412d-8942-24e1def64a0b'),('b462d407-a386-4006-aec7-5b5c37223690','78bf47b3-9d32-412d-8942-24e1def64a0b'),('e5e27e00-90d6-464b-9560-d8af8b88272c','78bf47b3-9d32-412d-8942-24e1def64a0b'),('031730c1-5ec1-4cde-845c-2d9001f1858d','87b0f71d-ee50-4d00-b433-0da7255245bb'),('0b7a6b42-19fd-49b0-a3bd-b8e300f0b209','87b0f71d-ee50-4d00-b433-0da7255245bb'),('265728e9-d9de-4edf-83e5-560c14ace534','87b0f71d-ee50-4d00-b433-0da7255245bb'),('29256e83-7313-4cc7-85d8-b02a5263b3a6','87b0f71d-ee50-4d00-b433-0da7255245bb'),('a3620b79-c24b-4aed-86ed-016ce53e1190','87b0f71d-ee50-4d00-b433-0da7255245bb');
/*!40000 ALTER TABLE `USER_ROLE_MAPPING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_SESSION`
--

DROP TABLE IF EXISTS `USER_SESSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_SESSION` (
  `ID` varchar(36) NOT NULL,
  `AUTH_METHOD` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `LAST_SESSION_REFRESH` int DEFAULT NULL,
  `LOGIN_USERNAME` varchar(255) DEFAULT NULL,
  `REALM_ID` varchar(255) DEFAULT NULL,
  `REMEMBER_ME` bit(1) NOT NULL DEFAULT b'0',
  `STARTED` int DEFAULT NULL,
  `USER_ID` varchar(255) DEFAULT NULL,
  `USER_SESSION_STATE` int DEFAULT NULL,
  `BROKER_SESSION_ID` varchar(255) DEFAULT NULL,
  `BROKER_USER_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_SESSION`
--

LOCK TABLES `USER_SESSION` WRITE;
/*!40000 ALTER TABLE `USER_SESSION` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_SESSION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER_SESSION_NOTE`
--

DROP TABLE IF EXISTS `USER_SESSION_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_SESSION_NOTE` (
  `USER_SESSION` varchar(36) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `VALUE` text,
  PRIMARY KEY (`USER_SESSION`,`NAME`),
  CONSTRAINT `FK5EDFB00FF51D3472` FOREIGN KEY (`USER_SESSION`) REFERENCES `USER_SESSION` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_SESSION_NOTE`
--

LOCK TABLES `USER_SESSION_NOTE` WRITE;
/*!40000 ALTER TABLE `USER_SESSION_NOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `USER_SESSION_NOTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WEB_ORIGINS`
--

DROP TABLE IF EXISTS `WEB_ORIGINS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `WEB_ORIGINS` (
  `CLIENT_ID` varchar(36) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  PRIMARY KEY (`CLIENT_ID`,`VALUE`),
  KEY `IDX_WEB_ORIG_CLIENT` (`CLIENT_ID`),
  CONSTRAINT `FK_LOJPHO213XCX4WNKOG82SSRFY` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WEB_ORIGINS`
--

LOCK TABLES `WEB_ORIGINS` WRITE;
/*!40000 ALTER TABLE `WEB_ORIGINS` DISABLE KEYS */;
INSERT INTO `WEB_ORIGINS` VALUES ('1b9f755b-72b9-4ee4-8fc6-132ab0556852','http://192.168.1.35:8081'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','http://jeannory.dynamic-dns.net:8081'),('1b9f755b-72b9-4ee4-8fc6-132ab0556852','http://localhost:8081'),('6d408964-4ba2-4ce2-8c9e-d328bdb51c0e','+'),('e8c9f90c-1683-4037-8bb4-79c52969e5b9','+'),('f092c270-84ce-44f5-8649-e1fd9714d24b','http://jeannory.dynamic-dns.net:4200'),('f092c270-84ce-44f5-8649-e1fd9714d24b','http://localhost:4200');
/*!40000 ALTER TABLE `WEB_ORIGINS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-22 16:45:52
