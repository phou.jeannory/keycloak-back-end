package software.craftsmanship.scuser.utils;

import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.craftsmanship.scuser.configurations.ScProperties;
import software.craftsmanship.scuser.singleton.SingletonBean;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Objects;

@Service
public class BuilderUtilsKeycloak {

    @Autowired
    private SingletonBean singletonBean;

    private OkHttpClient httpClient;

    @Autowired
    private ScProperties scProperties;

    @PostConstruct
    private void setUp(){
        httpClient=new OkHttpClient();
    }

    public String getAccessToken() throws IOException {
        RequestBody formBody = new FormBody.Builder()
                .addEncoded("username", "test")
                .addEncoded("password", "1234")
                .addEncoded("client_id", "sc-user")
                .addEncoded("grant_type", "password")
                .build();

        Request request = new Request.Builder()
                .url(scProperties.getAuthServerUrl() + scProperties.getAuthServerParameters())
                .header("ContentType", "application/x-www-form-urlencoded")
                .post(formBody)
                .build();

        try (final Response response = httpClient.newCall(request).execute()) {
            final TokenStore tokenStore = singletonBean.getObjectMapper().readValue(Objects.requireNonNull(response.body()).bytes(), TokenStore.class);
            return tokenStore.getAccess_token();
        }catch(IOException ex){

        }
        return null;
    }
}
