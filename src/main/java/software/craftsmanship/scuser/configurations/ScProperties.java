package software.craftsmanship.scuser.configurations;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Component
@ConfigurationProperties("sc-properties")
public class ScProperties {

    private String authServerUrl;
    private String authServerParameters;

}
